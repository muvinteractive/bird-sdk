﻿using UnityEngine;
using System.Collections;

public class MuvInput : MonoBehaviour {

	private const float MAX_MOVE_ANGLES = 50f;
	private const float BORDER_ANGLES = 60f;
	private const float HOVER_SPEED = 0.8f;

	public Quadcopter quadcopter;

	private bool[] flag;

	public bool isPause;

	private bool canPlay = false;

	void OnEnable()
	{
		MuvSensor.OnCanPlay += HandleOnCanPlay;
	}

	void OnDisable()
	{
		MuvSensor.OnCanPlay -= HandleOnCanPlay;
	}

	void HandleOnCanPlay()
	{
		canPlay = true;
	}

	void Start ()
	{
		flag = new bool[7];
	}
	
	// Update is called once per frame
	void Update () {
		if (!canPlay)
		{
			return;
		}
		CheckButton(MuvSensor.ButtonSwitch, "ButtonPause", 0);
		CheckButton(Input.GetKey(KeyCode.Space), "ButtonA", 1); //Camera
		CheckButton(Input.GetKey(KeyCode.R), "ButtonB", 2); //ResetPos
		CheckButton(MuvSensor.ButtonHome, "ButtonX", 3); //Hover
		if (quadcopter.IsHover && quadcopter.HoverStartTime+1 >= Time.time)
		{
			CheckAxis(0, 0, "RightThumbstick", 4);
			CheckAxis(0, HOVER_SPEED+0.01f, "LeftThumbstick", 5);
			return;
		}
		if (isPause)
		{
			if (quadcopter.IsHover)
			{
				CheckAxis(0, 0, "RightThumbstick", 4);
				CheckAxis(0, HOVER_SPEED, "LeftThumbstick", 5);
			}
			else
			{
				isPause = false;
			}
			return;
		}
		//CheckButton(KeyCode.T, "ButtonY", 0); //stabilization
		//CheckButton(MuvSensor.ButtonSwitch, "ButtonStart", 4); //Pause

		float LeftRight = Mathf.Clamp(MuvSensor.FingerRotation.z / MAX_MOVE_ANGLES, -1f, 1f);
		float ForwardBackward = -Mathf.Clamp(MuvSensor.FingerRotation.x / MAX_MOVE_ANGLES, -1f, 1f);
		float UpDown = HOVER_SPEED;

		if (Mathf.Abs(MuvSensor.FingerRotation.x) > BORDER_ANGLES)
		{
			if(MuvSensor.FingerRotation.x >= 0)
			{
				UpDown = HOVER_SPEED + (Mathf.Clamp((MuvSensor.FingerRotation.x-BORDER_ANGLES) / (110f-BORDER_ANGLES), 0f, 1f) * 0.5f);
			}else{
				UpDown = 0.6f;
			}
			ForwardBackward = 0;
		}

		if (!quadcopter.IsHover)
		{
			LeftRight = 0;
			ForwardBackward = 0;
			UpDown = 0;
		}

		CheckAxis(LeftRight, ForwardBackward, "RightThumbstick", 4);

		//Debug.LogError(GetComponent<Rigidbody>().velocity +" "+ MuvSensor.FingerRotation + " horizontal" + horizontal + " vertical" + vertical + " UpDown" + UpDown);
		CheckAxis(0, UpDown, "LeftThumbstick", 5);
	}

	private void CheckButton(bool buttonPress, string buttonName, int flagIndex)
	{
		if(buttonPress && !flag[flagIndex])
		{
			SendMessage("On" + buttonName + "_Down", SendMessageOptions.DontRequireReceiver);
			flag[flagIndex] = true;
		}
		else if(!buttonPress && flag[flagIndex])
		{
			SendMessage("On" + buttonName + "_Up", SendMessageOptions.DontRequireReceiver);
			flag[flagIndex] = false;
		}
	}

	private void CheckAxis(float axis1, float axis2, string axisName, int flagIndex)
	{
		float axis1value = axis1;
		float axis2value = axis2;
		//if(axis1value != 0 || axis2value != 0)
		//{
		Vector2 currentPosition = new Vector2(axis1value, axis2value);
		SendMessage("On" + axisName + "_Move", currentPosition, SendMessageOptions.DontRequireReceiver);
		//Set a flag the thumbstick, dpad or trigger value has changed
		flag[flagIndex] = true;
		//}
		//If the thumbstick, dpad or trigger value has changed and is centered again send a message
		/*else if (flag[flagIndex])
		{
			SendMessage("On" + axisName + "_Centered", SendMessageOptions.DontRequireReceiver);
			//Reset the flag the thumbstick, dpad or trigger is now centered again
			flag[flagIndex] = false;
		}*/
	}

	public void OnButtonPause_Down()
	{
		isPause = !isPause;
	}
}
