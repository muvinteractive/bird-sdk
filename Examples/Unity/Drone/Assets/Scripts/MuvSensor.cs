﻿using UnityEngine;
using System;
using System.IO; 
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;

public class MuvSensor : MonoBehaviour {

	public static Action OnConnected;
	public static Action<string> OnRequestForPair;
	public static Action OnCanPlay;

	public delegate void DelegateOnOrientPRY(int tOrientPRY);
	public delegate void DelegateOnBtns(int num);
	public delegate void DelegateBirdPairing(int birdId);
	public delegate void DelegateOnBirdAdded(int birdId, int birdNum);
	private DelegateBirdPairing _dOnBirdPairing;
	private DelegateOnBirdAdded _dOnBirdAdded;

	[DllImport("kernel32.dll")]
	public static extern int QueryDosDevice(string lpDeviceName, IntPtr lpTargetPath, int ucchMax);
	
	[DllImport("MUVClientWin.dll")]
	public static extern int Cli_Init(IntPtr pVer);
	
	[DllImport("MUVClientWin.dll")]
	public static extern int Sens_Generate(string sensorId, string com);
	
	[DllImport("MUVClientWin.dll")]
	public static extern int Sens_Connect(string sensorId);
	
	[DllImport("MUVClientWin.dll")]
	public static extern int Sens_Disconnect(string sensorId);

	[DllImport("MUVClientWin.dll")]
	public static extern int Sens_RegisterOnThiPairingPktCB(string sensorId, DelegateBirdPairing pFunc);
	
	[DllImport("MUVClientWin.dll")]
	public static extern int Sens_PairThimble(string sensorId, string thimbleId);

	[DllImport("MUVClientWin.dll")]
	public static extern int Sens_UnpairThimble(string sensorId, string thimbleId);
	
	[DllImport("MUVClientWin.dll")]
	public static extern int Sens_UnpairAllThi(string sensorId);

	[DllImport("MUVClientWin.dll")]
	public static extern int Thi_RegisterOnBtnsCB(string thimbleId, DelegateOnBtns pFunc);

	[DllImport("MUVClientWin.dll")]
	public static extern int Thi_GetBtns(string thimbleId, IntPtr btns);

	[DllImport("MUVClientWin.dll")]
	public static extern int Thi_RegisterOnOrientPRYCB(string thimbleId, DelegateOnOrientPRY pFunc);

	[DllImport("MUVClientWin.dll")]
	public static extern int Thi_SetIMUData(string thimbleId, bool bOn);

	[DllImport("MUVClientWin.dll")]
	public static extern int Sens_Remove(string sensorId);
	
	[DllImport("MUVClientWin.dll")]
	public static extern int Sens_RegisterOnThiAddedCB(string sensorId, DelegateOnBirdAdded pFunc);

	[DllImport("MUVClientWin.dll")]
	public static extern int Thi_GetOrientPRY(string thimbleId, IntPtr orient);

	[DllImport("MUVClientWin.dll")]
	public static extern int Thi_GetThimbleInfo(string thimbleId, IntPtr status);

	public string SensorId;
	public string ThimbleId;

	public bool ConnectedToSecondThimble;

	private string logLine;

	private int currentStep = 0;

	public static Vector3 FingerRotation = Vector3.zero;
	public static bool ButtonPinch = false;
	public static bool ButtonHome = false;
	public static bool ButtonSwitch = false;
	public static bool FingerDetected = false;

	private IntPtr pBtns;
	private int btns;
	private bool bPinch;
	private bool bHome;
	private bool bSwitch;
	private IntPtr pOrient;
	private float pitch;
	private float roll;
	private float yaw;
	private IntPtr pStatus;
	private bool bFingerDetected;

	private float currentTime = 0;

	void DebugLog (object message)
	{
		UnityEngine.Debug.Log(message);
		logLine = message.ToString();
	}

	// Use this for initialization
	void Start () {
		GetSensorId ();
		int ret;
		IntPtr pVer = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(int)));
		ret = Cli_Init(pVer);
		int ver = Marshal.ReadInt32(pVer);

		int majorVer = 0xFF & ver >> 0;
		int minorVer = 0xFF & ver >> 8;
		int buildVer = 0xFF & ver >> 16;

		DebugLog("ret = "+ret +" clientVer = "+majorVer.ToString("X")+"."+minorVer.ToString("X")+"."+buildVer.ToString("X"));

		StartCoroutine(ConnectToSensorOnStart());
	}

	IEnumerator ConnectToSensorOnStart()
	{
		yield return null;
		BtnConnect_Click();
	}

	void GetSensorId()
	{
		string sense = "";
		string filename = "SensorID.txt";
		if (File.Exists (filename))
		{
			FileInfo theSourceFile = new FileInfo (filename);
			StreamReader reader = theSourceFile.OpenText ();
			sense = reader.ReadLine ();
			if (sense != "") SensorId = sense;
			reader.Close ();
		}
	}

	void OnDisable ()
	{
		Sens_Disconnect(SensorId);
	}

	void FixedUpdate()
	{
		currentTime = Time.time;
		if (currentStep == 1 || currentStep == 2)
		{
			GetBirdState();
		}
	}

	public void GetBirdState()
	{
		// get Bird Buttons state
		pBtns = Marshal.AllocHGlobal(4);
		Thi_GetBtns(ThimbleId, pBtns);
		btns = Marshal.ReadInt32(pBtns);
		
		bPinch = Convert.ToBoolean( btns & (1 << 3) );
		bHome = Convert.ToBoolean( btns & (1 << 6) );
		bSwitch = Convert.ToBoolean( btns & (1 << 7) );

		ButtonPinch = bPinch;
		ButtonHome = bHome;
		ButtonSwitch = bSwitch;
		
		// get Bird orientation
		pOrient = Marshal.AllocHGlobal(4 * 3);
		Thi_GetOrientPRY(ThimbleId, pOrient);
		pitch = GetSingle(pOrient);
		roll = GetSingle((IntPtr)(pOrient.ToInt32() + 4));
		yaw = GetSingle((IntPtr)(pOrient.ToInt32() + 8));

		FingerRotation.x = pitch;
		FingerRotation.y = yaw;
		FingerRotation.z = roll;
		
		pStatus = Marshal.AllocHGlobal(4*20);
		Thi_GetThimbleInfo(ThimbleId, pStatus);
		bFingerDetected = Marshal.ReadByte(pStatus) == 1;
		
		FingerDetected = bFingerDetected;
		
	}

	float GetSingle(IntPtr pInt) {
			byte[] bytes = {
				Marshal.ReadByte(pInt), 
				Marshal.ReadByte((IntPtr)(pInt.ToInt32() + 1)),
				Marshal.ReadByte((IntPtr)(pInt.ToInt32() + 2)),
				Marshal.ReadByte((IntPtr)(pInt.ToInt32() + 3))};
			return BitConverter.ToSingle(bytes, 0);
	}

	void BtnQuit_Click()
	{
		Sens_Disconnect(SensorId);
	}

	void BtnConnect_Click()
	{
		int ret;
		if (SensorId != "")
		{
			Sens_Remove(SensorId);
		}

		ret = Sens_Generate(SensorId, SensorId);

		if(ret == -1)
		{
			DebugLog("Sens_Generate fail");
			return;
		}

		ret = Sens_Connect(SensorId);
		
		if(ret == -1)
		{
			DebugLog("Sens_Connect fail");
			return;
		}

		_dOnBirdAdded = HandleOnBirdAdded;
		ret = Sens_RegisterOnThiAddedCB(SensorId, _dOnBirdAdded);

		DebugLog("Sens_RegisterOnThiAddedCB ret="+ret);

		_dOnBirdPairing = HandleBirdPairing;
		ret = Sens_RegisterOnThiPairingPktCB(SensorId, _dOnBirdPairing);
		if(ret == -1)
		{
			DebugLog("Sens_RegisterOnThiPairingPktCB fail");
		}
	}

	void HandleBirdPairing(int birdId)
	{
		if (ConnectedToSecondThimble) {
			return; 
		}
		currentStep = 0;
		ConnectedToSecondThimble = true;

		if (OnRequestForPair != null)
		{
			OnRequestForPair(birdId.ToString("X"));
		}

		/*DebugLog("HandleBirdPairing " + birdId);
		ThimbleId = birdId.ToString("X");
		DebugLog("ThimbleId " + ThimbleId);
		Thread thread = new Thread(new ThreadStart(PairThimble));
		thread.Start();*/
	}

	public void CallPairThimble(string thimbleID)
	{
		ThimbleId = thimbleID;
		DebugLog("ThimbleId " + ThimbleId);
		Thread thread = new Thread(new ThreadStart(PairThimble));
		thread.Start();
	}

	public void CallDontPairNewThimble(string thimbleID)
	{
		if (ThimbleId == thimbleID)
		{
			Thread thread = new Thread(new ThreadStart(PairThimble));
			thread.Start();
		}
		else
		{
			CallStart();
		}
	}

	public void CallStart()
	{
		currentStep = 1;
		ConnectedToSecondThimble = true;
		if (OnCanPlay != null)
		{
			OnCanPlay();
		}
	}

	void PairThimble() {
		Sens_UnpairThimble("", "");
		Sens_PairThimble(SensorId, ThimbleId);
	}

	void HandleOnBirdAdded(int birdId, int birdNum)
	{
		if (!ConnectedToSecondThimble)
		{
			if (OnConnected != null)
			{
				OnConnected();
			}
		} else {
			if (OnCanPlay != null)
			{
				OnCanPlay();
			}
		}
		DebugLog("HandleOnBirdAdded " + birdId + " " + birdNum);
		ThimbleId = birdId.ToString("X");
		DebugLog("HandleOnBirdAdded ThimbleId = " + ThimbleId);
		currentStep = 1;

		Thread thread = new Thread(new ThreadStart(ImuForm_OnLoad));
		thread.Start();
	}

	void ImuForm_OnLoad()
	{
		try {
			int ret = 0;
			
			// request Bird to send IMU data
			for (int i = 0; i < 10; i++) {
				ret = Thi_SetIMUData(ThimbleId, true);
				if (ret == 0) {
					break;
				}
				Thread.Sleep(100);
				
			}
			if (ret == -1) {
				throw new Exception("Thi_SetIMUData fail");
			}
			
			
		} catch (Exception ex) {
			DebugLog(ex.Message);
			return;
		}
	}
}
