﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;

public class MuvMenu : MonoBehaviour {

	public MuvSensor muvSensor;

	public GameObject StartMenu;
	public GameObject PairMenu;

	public TextMesh ThimbleID;

	public SelectionSlider StartSlider;
	public SelectionSlider PairSlider;
	public SelectionSlider DontPairSlider;

	public Reticle reticle;

	private string thimbleIDText;
	private bool callOnConnected;
	private bool callOnRequestForPair;

	void OnEnable()
	{
		MuvSensor.OnConnected += HandleOnConnected;
		MuvSensor.OnRequestForPair += HandleOnRequestForPair;

		StartSlider.OnBarFilled += HandleOnStartSlider;
		PairSlider.OnBarFilled += HandleOnPairSlider;
		DontPairSlider.OnBarFilled += HandleOnDontPairSlider;
	}
	
	void OnDisable()
	{
		MuvSensor.OnConnected -= HandleOnConnected;
		MuvSensor.OnRequestForPair -= HandleOnRequestForPair;
		
		StartSlider.OnBarFilled -= HandleOnStartSlider;
		PairSlider.OnBarFilled -= HandleOnPairSlider;
		DontPairSlider.OnBarFilled -= HandleOnDontPairSlider;
	}

	void HandleOnStartSlider()
	{
		// on start selected
		muvSensor.CallStart();
		CloseMenu();
	}

	void HandleOnPairSlider()
	{
		// on pair selected
		muvSensor.CallPairThimble(thimbleIDText);
		CloseMenu();
	}

	void HandleOnDontPairSlider()
	{
		// on don't pair selected
		muvSensor.CallDontPairNewThimble(thimbleIDText);
		CloseMenu();
	}

	void CloseMenu()
	{
		StartMenu.SetActive(false);
		PairMenu.SetActive(false);
		reticle.Hide();
	}
	
	void HandleOnConnected()
	{
		callOnConnected = true;
	}

	void HandleOnRequestForPair(string thimbleID)
	{
		thimbleIDText = thimbleID;
		callOnRequestForPair = true;
	}

	// Use this for initialization
	void Awake () {
		StartMenu.SetActive(false);
		PairMenu.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (callOnConnected)
		{
			callOnConnected = false;
			StartMenu.SetActive(true);
			PairMenu.SetActive(false);
		}
		if (callOnRequestForPair)
		{
			callOnRequestForPair = false;
			ThimbleID.text = thimbleIDText;
			StartMenu.SetActive(false);
			PairMenu.SetActive(true);
		}
	}
}
