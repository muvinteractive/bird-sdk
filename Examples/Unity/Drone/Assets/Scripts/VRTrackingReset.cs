using UnityEngine;
using UnityEngine.VR;

namespace VRStandardAssets.Utils
{
    // This class simply insures the head tracking behaves correctly when the application is paused.
    public class VRTrackingReset : MonoBehaviour
    {
		void Awake()
		{
			InputTracking.Recenter();
		}

		void Update()
		{
			if(Input.GetKeyDown(KeyCode.T))
			{
				InputTracking.Recenter();
			}
		}

        private void OnApplicationPause(bool pauseStatus)
        {
            InputTracking.Recenter();
        }
    }
}