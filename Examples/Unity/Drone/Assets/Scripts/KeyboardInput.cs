﻿using UnityEngine;
using System.Collections;

public class KeyboardInput : MonoBehaviour {

	private bool[] flag;
	
	void Start ()
	{
		flag = new bool[7];
	}
	
	// Update is called once per frame
	void Update () {
		CheckButton(KeyCode.T, "ButtonY", 0); //stabilization
		CheckButton(KeyCode.Y, "ButtonX", 1); //Hover
		CheckButton(KeyCode.U, "ButtonA", 2); //Camera
		CheckButton(KeyCode.I, "ButtonB", 3); //ResetPos
		CheckButton(KeyCode.O, "ButtonStart", 4); //Pause
		CheckAxis("Horizontal", "Vertical", "RightThumbstick", 5); //stabilization
		//CheckAxis("Horizontal", "Vertical", "RightThumbstick", 6);
	}

	private void CheckButton(KeyCode key, string buttonName, int flagIndex)
	{
		if(Input.GetKey(key) && !flag[flagIndex])
		{
			SendMessage("On" + buttonName + "_Down", SendMessageOptions.DontRequireReceiver);
			flag[flagIndex] = true;
		}
		else if(!Input.GetKey(key) && flag[flagIndex])
		{
			SendMessage("On" + buttonName + "_Up", SendMessageOptions.DontRequireReceiver);
			flag[flagIndex] = false;
		}
	}

	private void CheckAxis(string axis1, string axis2, string axisName, int flagIndex)
	{
		float axis1value = Input.GetAxis(axis1);
		float axis2value = Input.GetAxis(axis2);
		if(axis1value != 0 || axis2value != 0)
		{
			Vector2 currentPosition = new Vector2(axis1value, axis2value);
			SendMessage("On" + axisName + "_Move", currentPosition, SendMessageOptions.DontRequireReceiver);
			//Set a flag the thumbstick, dpad or trigger value has changed
			flag[flagIndex] = true;
		}
		//If the thumbstick, dpad or trigger value has changed and is centered again send a message
		else if (flag[flagIndex])
		{
			SendMessage("On" + axisName + "_Centered", SendMessageOptions.DontRequireReceiver);
			//Reset the flag the thumbstick, dpad or trigger is now centered again
			flag[flagIndex] = false;
		}
	}
}
