﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GUITexture))]
public class Joystick : MonoBehaviour 
{
	// A simple class for bounding how far the GUITexture will move
	public class Boundary 
	{
		public Vector2 min = Vector2.zero;
		public Vector2 max = Vector2.zero;
	}

	private static Joystick[] joysticks;					// A static collection of all joysticks
	private static bool enumeratedJoysticks = false;
	private static float tapTimeDelta = 0.3f;				// Time allowed between taps
	
	public bool TouchPad;							        // Is this a TouchPad?
	public Rect TouchZone;
	public Vector2 DeadZone = Vector2.zero;					// Control when Position is output
	public bool Normalize = false; 						    // Normalize output after the dead-zone?
	public Vector2 Position; 							    // [-1, 1] in x,y
	public int TapCount;									// Current tap count
	
	private int lastFingerId = -1;							// Finger last used for this joystick
	private float tapTimeWindow;							// How much time there is left for a tap to occur
	private Vector2 fingerDownPos;

	private GUITexture gui;									// Joystick graphic
	private Rect defaultRect;								// Default Position / extents of the joystick graphic
	private Boundary guiBoundary = new Boundary();				// Boundary for joystick graphic
	private Vector2 guiTouchOffset;							// Offset to apply to touch input
	private Vector2 guiCenter;								// Center of joystick

	void Start ()
	{
		// Cache this component at startup instead of looking up every frame	
		gui = GetComponent<GUITexture>();
		
		// Store the default rect for the gui, so we can snap back to it
		defaultRect = gui.pixelInset;	
		
		defaultRect.x += transform.position.x * Screen.width;
		defaultRect.y += transform.position.y * Screen.height;
		
		transform.position = new Vector3(0,0,transform.position.z);
		
		if ( TouchPad )
		{
			// If a texture has been assigned, then use the rect ferom the gui as our TouchZone
			if ( gui.texture )
				TouchZone = defaultRect;
		}
		else
		{				
			// This is an offset for touch input to match with the top left
			// corner of the GUI
			guiTouchOffset.x = defaultRect.width * 0.5f;
			guiTouchOffset.y = defaultRect.height * 0.5f;
			
			// Cache the center of the GUI, since it doesn't change
			guiCenter.x = defaultRect.x + guiTouchOffset.x;
			guiCenter.y = defaultRect.y + guiTouchOffset.y;
			
			// Let's build the GUI boundary, so we can clamp joystick movement
			guiBoundary.min = new Vector3(defaultRect.x - guiTouchOffset.x, defaultRect.y - guiTouchOffset.y);
			guiBoundary.max = new Vector3(defaultRect.x + guiTouchOffset.x, defaultRect.y + guiTouchOffset.y);
		}
	}

	public void Disable()
	{
		gameObject.SetActive(false);
		enumeratedJoysticks = false;
	}

	public void ResetJoystick()
	{
		// Release the finger control and set the joystick back to the default Position
		gui.pixelInset = defaultRect;
		lastFingerId = -1;
		Position = Vector2.zero;
		fingerDownPos = Vector2.zero;
		
		if ( TouchPad )
		{
			gui.color = new Color(gui.color.r, gui.color.g, gui.color.b, 0.025f);
		}
	}

	public bool IsFingerDown()
	{
		return (lastFingerId != -1);
	}

	void LatchedFinger(int fingerId)
	{
		// If another joystick has latched this finger, then we must release it
		if ( lastFingerId == fingerId )
		{
			ResetJoystick();
		}
	}

	void Update ()
	{
		if ( !enumeratedJoysticks )
		{
			// Collect all joysticks in the game, so we can relay finger latching messages
			joysticks = FindObjectsOfType<Joystick>();
			enumeratedJoysticks = true;
		}	
		
		int count = Input.touchCount;
		
		// Adjust the tap time window while it still available
		if ( tapTimeWindow > 0 )
			tapTimeWindow -= Time.deltaTime;
		else
			TapCount = 0;
		
		if ( count == 0 )
			ResetJoystick();
		else
		{
			for(int i = 0;i < count; i++)
			{
				Touch touch = Input.GetTouch(i);			
				Vector2 guiTouchPos = touch.position - guiTouchOffset;
				
				bool shouldLatchFinger = false;
				if ( TouchPad )
				{				
					if ( TouchZone.Contains( touch.position ) )
						shouldLatchFinger = true;
				}
				else if ( gui.HitTest( touch.position ) )
				{
					shouldLatchFinger = true;
				}		
				
				// Latch the finger if this is a new touch
				if ( shouldLatchFinger && ( lastFingerId == -1 || lastFingerId != touch.fingerId ) )
				{
					
					if ( TouchPad )
					{
						gui.color = new Color(gui.color.r, gui.color.g, gui.color.b, 0.15f);
						
						lastFingerId = touch.fingerId;
						fingerDownPos = touch.position;
					}
					
					lastFingerId = touch.fingerId;
					
					// Accumulate taps if it is within the time window
					if ( tapTimeWindow > 0 )
						TapCount++;
					else
					{
						TapCount = 1;
						tapTimeWindow = tapTimeDelta;
					}
					
					// Tell other joysticks we've latched this finger
					foreach (Joystick j in joysticks)
					{
						if ( j != this )
							j.LatchedFinger( touch.fingerId );
					}						
				}				
				
				if ( lastFingerId == touch.fingerId )
				{	
					// Override the tap count with what the iPhone SDK reports if it is greater
					// This is a workaround, since the iPhone SDK does not currently track taps
					// for multiple touches
					if ( touch.tapCount > TapCount )
						TapCount = touch.tapCount;
					
					if ( TouchPad )
					{	
						// For a touchpad, let's just set the Position directly based on distance from initial touchdown
						Position.x = Mathf.Clamp( ( touch.position.x - fingerDownPos.x ) / ( TouchZone.width / 2 ), -1, 1 );
						Position.y = Mathf.Clamp( ( touch.position.y - fingerDownPos.y ) / ( TouchZone.height / 2 ), -1, 1 );
					}
					else
					{					
						// Change the location of the joystick graphic to match where the touch is
						gui.pixelInset = new Rect(Mathf.Clamp( guiTouchPos.x, guiBoundary.min.x, guiBoundary.max.x ),
						                          Mathf.Clamp( guiTouchPos.y, guiBoundary.min.y, guiBoundary.max.y ),
						                          gui.pixelInset.width,
						                          gui.pixelInset.height);
					}
					
					if ( touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled )
						ResetJoystick();					
				}			
			}
		}
		
		if ( !TouchPad )
		{
			// Get a value between -1 and 1 based on the joystick graphic location
			Position.x = ( gui.pixelInset.x + guiTouchOffset.x - guiCenter.x ) / guiTouchOffset.x;
			Position.y = ( gui.pixelInset.y + guiTouchOffset.y - guiCenter.y ) / guiTouchOffset.y;
		}
		
		// Adjust for dead zone	
		var absoluteX = Mathf.Abs( Position.x );
		var absoluteY = Mathf.Abs( Position.y );
		
		if ( absoluteX < DeadZone.x )
		{
			// Report the joystick as being at the center if it is within the dead zone
			Position.x = 0;
		}
		else if ( Normalize )
		{
			// Rescale the output after taking the dead zone into account
			Position.x = Mathf.Sign( Position.x ) * ( absoluteX - DeadZone.x ) / ( 1 - DeadZone.x );
		}
		
		if ( absoluteY < DeadZone.y )
		{
			// Report the joystick as being at the center if it is within the dead zone
			Position.y = 0;
		}
		else if ( Normalize )
		{
			// Rescale the output after taking the dead zone into account
			Position.y = Mathf.Sign( Position.y ) * ( absoluteY - DeadZone.y ) / ( 1 - DeadZone.y );
		}
	}
}
