﻿using UnityEngine;
using System.Collections;

//This script is used to reset the obstacle when B is pressed on the xbox360 controller
public class Obstacle : MonoBehaviour
{
	private Vector3[] positions;

	void Start()
	{
		positions = new Vector3[transform.childCount];
		for(int i = 0; i < transform.childCount; i++)
		{
			positions[i] = transform.GetChild(i).position;
		}
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.R))
		{
			Reset();
		}
	}

	//Signal receiver from Xbox360ControllerInput script
	public void OnButtonB_Down()
	{
		//Reset();
	}

	private void Reset()
	{
		//Reset all child components to their default position, rotation and zero velocity
		for(int i = 0; i < transform.childCount; i++)
		{
			Transform childTransform = transform.GetChild(i);
			childTransform.position = positions[i];
			childTransform.eulerAngles = Vector3.zero;
			Rigidbody rigidbody = childTransform.GetComponent<Rigidbody>();
			if(rigidbody != null)
			{
				rigidbody.velocity = Vector3.zero;
			}
		}
	}
}
