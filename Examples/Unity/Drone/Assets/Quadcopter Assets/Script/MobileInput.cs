﻿using UnityEngine;
using System.Collections;

public class MobileInput : MonoBehaviour
{
	public Joystick LeftJoystick;
	public Joystick RightJoystick;
	public ButtonInput ButtonA;
	public ButtonInput ButtonB;
	public ButtonInput ButtonX;
	public ButtonInput ButtonY;
	public ButtonInput ButtonStart;

	private bool[] flag;

	void Start ()
	{
		flag = new bool[7];
	}

	void Update ()
	{
		if(LeftJoystick != null && RightJoystick != null)
		{
			CheckButton(ButtonA, "ButtonA", 0);
			CheckButton(ButtonB, "ButtonB", 1);
			CheckButton(ButtonX, "ButtonX", 2);
			CheckButton(ButtonY, "ButtonY", 3);
			CheckButton(ButtonStart, "ButtonStart", 4);
			CheckAxis(LeftJoystick.Position, "LeftThumbstick", 5);
			CheckAxis(RightJoystick.Position, "RightThumbstick", 6);
		}
	}

	private void CheckButton(ButtonInput button, string buttonName, int flagIndex)
	{
		if(button.Hit && !flag[flagIndex])
		{
			SendMessage("On" + buttonName + "_Down", SendMessageOptions.DontRequireReceiver);
			flag[flagIndex] = true;
		}
		else if(!button.Hit && flag[flagIndex])
		{
			SendMessage("On" + buttonName + "_Up", SendMessageOptions.DontRequireReceiver);
			flag[flagIndex] = false;
		}
	}

	private void CheckAxis(Vector2 position, string axisName, int flagIndex)
	{
		if(position != Vector2.zero)
		{
			SendMessage("On" + axisName + "_Move", position, SendMessageOptions.DontRequireReceiver);
			//Set a flag the thumbstick
			flag[flagIndex] = true;
		}
		//If the thumbstick, dpad or trigger value has changed and is centered again send a message
		else if (flag[flagIndex])
		{
			SendMessage("On" + axisName + "_Centered", SendMessageOptions.DontRequireReceiver);
			//Reset the flag the thumbstick, dpad or trigger is now centered again
			flag[flagIndex] = false;
		}
	}
}
