using UnityEngine;
using System.Collections;

public class FPSCounter : MonoBehaviour
{
	// A FPS counter.
	// It calculates frames/second over each updateInterval,
	// so the display does not keep changing wildly.

	private float updateInterval = 0.5f;
	private float lastInterval;
	// Last interval end time
	private float frames = 0;
	// Frames over current interval
	private float fps;
	// Current FPS
	
	void Start ()
	{
		lastInterval = Time.realtimeSinceStartup;
		frames = 0;
		Cursor.visible = false;
	}

	void OnGUI ()
	{
		GUI.Box(new Rect(Screen.width - 200,0, 200,25), fps.ToString ("f2") + " fps");
	}

	void Update ()
	{
		++frames;
		var timeNow = Time.realtimeSinceStartup;
		if (timeNow > lastInterval + updateInterval)
		{
			fps = frames / (timeNow - lastInterval);
			frames = 0;
			lastInterval = timeNow;
		}
	}
}