﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody))]
public class QuadcopterMobile : MonoBehaviour
{
	public Text StabilizationText;
	public Text StabilizationTextShadow1;
	public Text StabilizationTextShadow2;
	public Text HoverModeText;
	public Text HoverModeTextShadow1;
	public Text HoverModeTextShadow2;
	public Text CameraText;
	public Text CameraTextShadow1;
	public Text CameraTextShadow2;
	public Text PauseText;
	public Text PauseTextShadow1;
	public Text PauseTextShadow2;

	private const float LIFTMULTIPLIER = 724.34f;        //Used for the maximum lift
	private const float TORQUEMULTIPLIER = 0.6f;         //Used for tilting
	private const float TURNMULTIPLIER = 2f;             //Used for Y rotation
	private const int NUMBEROFPROPELLERS = 8;            //Used for turning the propellers (8 parts)
	private const float MAXPROPELLERROTATIONSPEED = 100; //Used for rotation speed of the propellers
	private const float STABILITYFACTOR = 0.3f;          //Used for stabilizing speed the quadcopter
	private const float HOVERSPEED = 0.8f;               //Used for the rotation speed of te propellers on hover mode (simulated)
	private const float HOVERFORCE = 11.84f;             //Used for the hovering force of the quadcopter
	private const float HOVERCORRECTION = 0.5f;          //Used for switching rising/falling speed to hover (zero Y speed)

	private AudioListener mainCameraAudioListener;       //Used for listening to the quadcopter sound from the ground
	private AudioListener quadcopterCameraAudioListener; //Used for listening to the quadcopter sound from the quadcopter
	private Camera quadcopterCamera;                     //Used for switching to view the quadcopter camera
	private AudioSource audioSource;                     //Used for playing the quadcopter sound
	private float leftX = 0;                             //Used for the horizontal movement of the left thumbstick
	private float leftY = 0;                             //Used for the vertical movement of the left thumbstick
	private float rightX = 0;                            //Used for the horizontal movement of the right thumbstick
	private float rightY = 0;                            //Used for the vertical movement of the left thumbstick
	private GameObject[] propellars;                     //Used for storing the propeller objects
	private bool stabilize = true;                       //Used for turning stabilizing on or off
	private bool hover = false;                          //Used for turning hovering on or off
	private float startTime = 0;                         //Used for switching rising/falling speed to hover in steps (zero Y speed)
	private float startVelocity = 0;                     //Used for switching rising/falling speed to hover in steps (zero Y speed)

	void Start ()
	{
		mainCameraAudioListener = Camera.main.gameObject.GetComponent<AudioListener>();
		quadcopterCamera = GetChildCamera("Unity3DCamera");
		quadcopterCameraAudioListener = quadcopterCamera.GetComponent<AudioListener>();
		audioSource = GetComponent<AudioSource>();
		propellars = new GameObject[NUMBEROFPROPELLERS];
		for(int i = 0; i < NUMBEROFPROPELLERS; i++)
		{
			propellars[i] = GetChildGameObject("Propeller" + (i + 1));
		}
		UpdateGUI();
	}

	void UpdateGUI()
	{
		StabilizationText.text = string.Format("Stabilization {0}", stabilize ? "active" : "inactive");
		StabilizationTextShadow1.text = StabilizationTextShadow2.text = StabilizationText.text;
		HoverModeText.text = string.Format("Hover mode {0}", hover ? "active" : "inactive");
		HoverModeTextShadow1.text = HoverModeTextShadow2.text = HoverModeText.text;
		CameraText.text = string.Format("{0} camera active", quadcopterCamera.enabled ? "Quadcopter" : "Main");
		CameraTextShadow1.text = CameraTextShadow2.text = CameraText.text;
		PauseText.text = Time.timeScale == 0 ? "Unpause" : "Pause";
		PauseTextShadow1.text = PauseTextShadow2.text = PauseText.text;
	}

	void Update()
	{
		//Quit the application on escape
		if(Input.GetKey(KeyCode.Escape))
		{
			Application.Quit();
		}
		Stabilize();
		RotateMotors();
		AddForces();
		CheckAudio();
	}

	//Signal receiver from Xbox360ControllerInput script
	public void OnButtonA_Down()
	{
		if(quadcopterCamera != null)
		{
			quadcopterCamera.enabled = !quadcopterCamera.enabled;
			mainCameraAudioListener.enabled = !quadcopterCamera.enabled;
			quadcopterCameraAudioListener.enabled = quadcopterCamera.enabled;
			UpdateGUI();
		}
	}

	//Signal receiver from Xbox360ControllerInput script
	public void OnButtonB_Down()
	{
		Reset();
	}

	//Signal receiver from Xbox360ControllerInput script
	public void OnButtonX_Down()
	{
		hover = !hover;
		//Then starting hover mode get the current time
		startTime = Time.time;
		//and the velocity to determine steps to zero Y speed
		startVelocity = Mathf.Abs(GetComponent<Rigidbody>().velocity.y);
		UpdateGUI();
	}

	//Signal receiver from Xbox360ControllerInput script
	public void OnButtonY_Down()
	{
		stabilize = !stabilize;
		UpdateGUI();
	}

	//Signal receiver from Xbox360ControllerInput script
	public void OnButtonStart_Down()
	{
		//Pause game
		if(Time.timeScale == 1)
		{
			if(audioSource.isPlaying)
			{
				audioSource.Pause();
			}
			Time.timeScale = 0;
		}
		else
		{
			//Unpause game
			Time.timeScale = 1;
		}
		UpdateGUI();
	}

	//Gets a child gameobject by name, return null if not found
	private GameObject GetChildGameObject(string name)
	{
		Transform childTransform = gameObject.transform.FindChild(name);
		if(childTransform != null)
		{
			return childTransform.gameObject;
		}
		return null;
	}

	//Gets a child camera by name, return null if not found
	private Camera GetChildCamera(string name)
	{
		Transform childTransform = gameObject.transform.FindChild(name);
		if(childTransform != null)
		{
			return childTransform.GetComponent<Camera>();
		}
		return null;
	}

	//Signal receiver from Xbox360ControllerInput script
	public void OnLeftThumbstick_Move(Vector2 position)
	{
		//Dont respond in the lower half of the left thumbstick
		if(position.y >= 0)
		{
			leftX = position.x;
			leftY = position.y;
		}
		else
		{
			//In the lower half of the left thumbstick reset all horizontal and vertical movement
			OnLeftThumbstick_Centered();
		}
	}
	
	//Signal receiver from Xbox360ControllerInput script
	public void OnLeftThumbstick_Centered()
	{
		leftX = 0;
		leftY = 0;
	}

	//Signal receiver from Xbox360ControllerInput script
	public void OnRightThumbstick_Move(Vector2 position)
	{
		rightX = position.x;
		rightY = position.y;
	}
	
	//Signal receiver from Xbox360ControllerInput script
	public void OnRightThumbstick_Centered()
	{
		rightX = 0;
		rightY = 0;
	}

	//This method is used to rotate the motors and play the audio (volume + pitch)
	//This method is purely visual, no forces are applied in this method
	private void RotateMotors()
	{
		//When not paused
		if(Time.timeScale != 0)
		{
			//leftY is never negative
			float motorSpeed = 0;
			if(hover)
			{
				//Use the hoverspeed or a throttle above hoverspeed
				motorSpeed = Mathf.Max(Mathf.Abs(leftX), Mathf.Max(leftY, HOVERSPEED), Mathf.Abs(rightX), Mathf.Abs(rightY));
			}
			else
			{
				//Use the motorspeed from all thumbsticks (max value determine)
				motorSpeed = Mathf.Max(Mathf.Abs(leftX), leftY, Mathf.Abs(rightX), Mathf.Abs(rightY));
			}
			if(motorSpeed > 0 && !audioSource.isPlaying)
			{
				//Play audio when the motors turn
				audioSource.Play();
			}
			//Adjust the audio pitch according to motorSpeed;
			audioSource.pitch = motorSpeed;
			//Adjust the audio volume according to motorSpeed;
			audioSource.volume = motorSpeed;
			for(int i = 0; i < NUMBEROFPROPELLERS; i++)
			{
				if(propellars[i] != null)
				{
					//Rotate each propeller at motorspeed
					propellars[i].transform.Rotate(0, 0, motorSpeed * MAXPROPELLERROTATIONSPEED);
				}
			}
		}
	}

	//This method is used for moving the quadcopter
	private void AddForces()
	{
		//When not paused
		if(Time.timeScale != 0)
		{
			//Turn around Y axis
			GetComponent<Rigidbody>().AddTorque(0, leftX * TURNMULTIPLIER, 0);
			//Tilt the quadcopter left/right
			float zAxisTorque = -rightX * TORQUEMULTIPLIER;
			//Tilt the quadcopter front/back
			float xAxisTorque = rightY * TORQUEMULTIPLIER;
			//When the hover speed is higher then the left thumbstick speed
			if(hover && leftY <= HOVERSPEED)
			{
				//Determine the percentage of the stopping 
				float fracComplete = (Time.time - startTime) / (startVelocity / HOVERCORRECTION);
				//Alter the vertical velocity to zero in steps
				GetComponent<Rigidbody>().velocity = Vector3.Slerp(GetComponent<Rigidbody>().velocity, new Vector3(GetComponent<Rigidbody>().velocity.x, 0, GetComponent<Rigidbody>().velocity.z), fracComplete);
				//Hover the quadcopter
				GetComponent<Rigidbody>().AddRelativeForce(0, HOVERFORCE * Time.deltaTime, 0, ForceMode.Impulse);
			}
			else
			{
				//Determine the vertical force
				float altitudeForce = leftY * LIFTMULTIPLIER * Time.deltaTime;
				//Add the vertical force with a torque correction (to prevent quick lif)
				GetComponent<Rigidbody>().AddRelativeForce(0, altitudeForce - Mathf.Abs(zAxisTorque) - Mathf.Abs(xAxisTorque), 0);
				//When flying without hovering reset the hover timer
				startTime = Time.time;
				//When flying without hovering reset start velocity
				startVelocity = Mathf.Abs(GetComponent<Rigidbody>().velocity.y);
			}
			//Turn around Z axis
			GetComponent<Rigidbody>().AddRelativeTorque(0, 0, zAxisTorque);
			//Turn around X axis
			GetComponent<Rigidbody>().AddRelativeTorque(xAxisTorque, 0, 0);
		}
	}

	//This method stabilizes the quadcopter to the horizontal position
	private void Stabilize()
	{
		//When not paused
		if(Time.timeScale != 0 && stabilize)
		{
			//Determine the upvector
			Vector3 predictedUp = Quaternion.AngleAxis(GetComponent<Rigidbody>().angularVelocity.magnitude * Mathf.Rad2Deg * STABILITYFACTOR,
			                                           GetComponent<Rigidbody>().angularVelocity) * transform.up;
			//Determine the torque needed to get to the upvector
			Vector3 torqueVector = Vector3.Cross(predictedUp, Vector3.up);
			//Apply the torque to get to the horzontal position
			GetComponent<Rigidbody>().AddTorque(torqueVector);
			//Apply drag on the vertical movement te be more stabile
			GetComponent<Rigidbody>().AddTorque(0, -GetComponent<Rigidbody>().angularVelocity.y, 0);
		}
	}

	//This method checks if the motor audio still needs to be playing
	private void CheckAudio()
	{
		if(leftX == 0 && leftY == 0 && rightX == 0 && rightY == 0 && !hover)
		{
			audioSource.Stop();
		}
	}

	//This method resets the quadcopter to its original position
	private void Reset()
	{
		//Default position
		transform.position = Vector3.zero;
		//Default rotation
		transform.eulerAngles = Vector3.zero;
		//Default no velocity
		GetComponent<Rigidbody>().velocity = Vector3.zero;
		//Disable hover on reset
		hover = false;
	}
}
