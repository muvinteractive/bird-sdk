﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class ButtonInput : MonoBehaviour
{
	public bool Hit { get; private set; }

    void Update ()
	{
		Hit = TestHit();
	}

	private bool TestHit()
	{
		int count = Input.touchCount;
		for(int i = 0;i < count; i++)
		{
			Touch touch = Input.GetTouch(i);
			if(GetComponent<Collider2D>() == Physics2D.OverlapPoint(touch.position))
			{
				return true;
			}
		}
		return false;
	}
}
