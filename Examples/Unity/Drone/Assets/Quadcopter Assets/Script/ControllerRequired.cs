﻿using UnityEngine;
using System.Collections;

//This script shows or hide a message when the xbox360 controller is connected/disconnected
[RequireComponent(typeof(Canvas))]
public class ControllerRequired : MonoBehaviour
{
	private Canvas canvas;

	void Start()
	{
		canvas = GetComponent<Canvas>();
	}

	public void OnJoystick_Connect()
	{
		canvas.enabled = false;
	}
	
	public void OnJoystick_Disconnect()
	{
		canvas.enabled = true;
	}
}