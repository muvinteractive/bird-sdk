﻿using System.Linq;
using UnityEngine;
using System.Collections;

public class Xbox360ControllerInput : MonoBehaviour
{
	//The name of the xbox 360 controller
	private const string XBOX360CONTROLLERNAME = "Controller (Xbox 360 Wireless Receiver for Windows)";
	//A flag if the controller is connected or not
	private bool connected;
	//An array to hold the last state of a button/dpad/thumbstick
	private bool[] flag;

	void Start () 
	{
		flag = new bool[14];
	}
	
	void Update () 
	{
		CheckConnected();
		CheckButton("Fire1", "ButtonA", 0);
		CheckButton("Fire2", "ButtonB", 1);
		CheckButton("Fire3", "ButtonX", 2);
		CheckButton("Fire4", "ButtonY", 3);
		CheckButton("Fire5", "ButtonLB", 4);
		CheckButton("Fire6", "ButtonRB", 5);
		CheckButton("Fire7", "ButtonBack", 6);
		CheckButton("Fire8", "ButtonStart", 7);
		CheckButton("Fire9", "ButtonLeftThumbstick", 8);
		CheckButton("Fire10", "ButtonRightThumbstick", 9);
		CheckAxis("Horizontal", "Vertical", "LeftThumbstick", 10);
		CheckAxis("LeftTrigger", "RightTrigger", "Triggers", 11);
		CheckAxis("HorizontalRight", "VerticalRight", "RightThumbstick", 12);
		CheckAxis("HorizontalDPad", "VerticalDPad", "DPad", 13);
	}

	//Check if the xbox360 controller is still connected (sends messages)
	private void CheckConnected()
	{
		string[] joystickNames = Input.GetJoystickNames();
		if(joystickNames.Length > 0 && joystickNames.Contains(XBOX360CONTROLLERNAME) && !connected)
		{
			SendMessage("OnJoystick_Connect", SendMessageOptions.DontRequireReceiver);
			connected = true;
		}
		if(joystickNames.Length == 0 && connected)
		{
			SendMessage("OnJoystick_Disconnect", SendMessageOptions.DontRequireReceiver);
			connected = false;
		}
	}

	//Check if a button is pressed or released (sends messages)
	private void CheckButton(string button, string buttonName, int flagIndex)
	{
		if(Input.GetButton(button) && !flag[flagIndex])
		{
			SendMessage("On" + buttonName + "_Down", SendMessageOptions.DontRequireReceiver);
			flag[flagIndex] = true;
		}
		else if(!Input.GetButton(button) && flag[flagIndex])
		{
			SendMessage("On" + buttonName + "_Up", SendMessageOptions.DontRequireReceiver);
			flag[flagIndex] = false;
		}
	}

	//Check if a thumbstick, dpad or trigger value has changed (sends messages)
	private void CheckAxis(string axis1, string axis2, string axisName, int flagIndex)
	{
		float axis1value = Input.GetAxis(axis1);
		float axis2value = Input.GetAxis(axis2);
		if(axis1value != 0 || axis2value != 0)
		{
			Vector2 currentPosition = new Vector2(axis1value, axis2value);
			SendMessage("On" + axisName + "_Move", currentPosition, SendMessageOptions.DontRequireReceiver);
			//Set a flag the thumbstick, dpad or trigger value has changed
			flag[flagIndex] = true;
		}
		//If the thumbstick, dpad or trigger value has changed and is centered again send a message
		else if (flag[flagIndex])
		{
			SendMessage("On" + axisName + "_Centered", SendMessageOptions.DontRequireReceiver);
			//Reset the flag the thumbstick, dpad or trigger is now centered again
			flag[flagIndex] = false;
		}
	}
}
