﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;

namespace BirdUI {
	public partial class screen_3ddemo : screen {
		public bool _bResetModelOrientation = true;
		public Quaternion _modelOrientationOffsetQ = new Quaternion(1, 0, 0, 0);
		public screen_3ddemo() {
			InitializeComponent();
		}

		private void Button_Click(object sender, RoutedEventArgs e) {
			// start a reset operation
			_bResetModelOrientation = true;
		}

		public void Rotate3DModel(double x, double y, double z, double w) {
			// rotate the model to the given Bird quaternion
			var birdOrientQ = new Quaternion(x, y, z, w);

			// do we need a reset?
			if (_bResetModelOrientation) {
				// sample the current Bird orientation and save it as offset
				_modelOrientationOffsetQ = new Quaternion(x, y, z, w);
				_modelOrientationOffsetQ.Conjugate();
				_bResetModelOrientation = false;
			}
			// compensate for the offset
			var modelOrientQ = Quaternion.Multiply(_modelOrientationOffsetQ, birdOrientQ);
			// do the rotation
			var myQuaternionRotation3D = new QuaternionRotation3D(modelOrientQ);
			RotateTransform3D myRotateTransform = new RotateTransform3D(myQuaternionRotation3D);
			model_cube_r.Transform = myRotateTransform;
			model_cube_g.Transform = myRotateTransform;
			model_cube_b.Transform = myRotateTransform;
		}
	}
}
