﻿/******************************************************************************
	Module:	SDK Examples
	File:	BirdConnect.cs
	Desc:	Bird connection example
	Author:	#gr
	Creatd:	3-2016
	Remarks:
	Changs:

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

public class BirdConnect {
	private bool bGetEventCallbacks = true;
	public string SensorId = "";
    public string SensorAddr = "";
    public List<String> BirdIds = new List<String>();
    public string ActiveBirdId { get { return BirdIds.Count == 1 ? BirdIds[0] : GetActiveBirdId(); } }
    public bool IsUsbSensor = false;
	public bool bBirdConfig;
	public const string BirdDLLPath = "./MUVClientWin.dll";
	public bool bBirdDuringPairing=false;
    public bool isMultiBirdMode = false;
    public const int PAKCETS_LIMIT = 100;
    public int packetsCount = 0;
    public bool menuActivated = false;

    public delegate void DelegateOnOrient(IntPtr BirdId, IntPtr pOrient);
	public delegate void DelegateOnBtns(IntPtr BirdId, int num);
	public delegate void DelegateBirdPairing(int birdId, int pairingFlags);
	public delegate void DelegateOnBirdAdded(int birdId, int birdNum);
    public delegate void DelegateOnOFN(IntPtr BirdId, int x, int y, byte flags);
	public delegate void DelegateVoid_Ptr(IntPtr pVoid);
	public delegate void DelegateVoid_IntInt(int i0, int i1);

	private DelegateBirdPairing _dOnBirdPairing;
	private DelegateOnBirdAdded _dOnBirdAdded;

    #region Bird DLL API
    // import the Bird API from the local DLL
	[DllImport("kernel32.dll")]
	public static extern int QueryDosDevice(string lpDeviceName, IntPtr lpTargetPath, int ucchMax);

	[DllImport(BirdDLLPath)]
	public static extern int Cli_Init(IntPtr pVer);

    [DllImport(BirdDLLPath)]
    public static extern int Cli_LOG(string msg);
	[DllImport(BirdDLLPath)]
	public static extern int Cli_Shutdown();

	[DllImport(BirdDLLPath)]
	public static extern int Sens_Generate(string sensorId, string com);

	[DllImport(BirdDLLPath)]
    public static extern int  Cli_DiscoverBirdUsbSensor(IntPtr pSensNamesBuff, int buffLen);

    [DllImport(BirdDLLPath)]
    public static extern int Cli_DiscoverBirdSensors(IntPtr pSensNamesBuff, int buffLenNames, IntPtr pSensAddrsBuff, int buffLenAddrs, String pSensNamesPrefix);
    
    [DllImport(BirdDLLPath)]
	public static extern int Sens_Connect(string sensorId);

    [DllImport(BirdDLLPath)]
    public static extern int Cli_HostPairBluetoothDevice(string sensorId, string sensorAddr);
    
    [DllImport(BirdDLLPath)]
	public static extern int Sens_Disconnect(string sensorId);

	[DllImport(BirdDLLPath)]
    public static extern int Sens_GetPairedThimblesC(string sensorId, int bufLen, IntPtr thimbles);

	[DllImport(BirdDLLPath)]
	public static extern int Sens_RegisterOnThiPairingPktCB(string sensorId, DelegateBirdPairing pFunc);

	[DllImport(BirdDLLPath)]
	public static extern int Sens_PairThimble(string sensorId, string thimbleId);

	[DllImport(BirdDLLPath)]
	public static extern int Sens_UnpairThimble(string sensorId, string thimbleId);

	[DllImport(BirdDLLPath)]
	public static extern int Sens_UnpairAllThi(string sensorId);

    [DllImport(BirdDLLPath)]
    public static extern int Sens_GetActiveThimbleId(String sensId, IntPtr activeThiId, IntPtr isMaster, IntPtr isLocked);
	[DllImport(BirdDLLPath)]
    public static extern int Thi_SetMultitouchMode(bool enable);

    [DllImport(BirdDLLPath)]
    public static extern int Sens_BlockThimbleMessages(string sensorId, bool block);

	[DllImport(BirdDLLPath)]
	public static extern int Thi_InitIMUCalib(string thimbleId, DelegateVoid_IntInt pFunc);
	
	[DllImport(BirdDLLPath)]
	public static extern int Thi_WakeUp(string thimbleId, int duration);

	[DllImport(BirdDLLPath)]
	public static extern int Thi_InitCompassCalib(string thimbleId, DelegateVoid_IntInt pFunc);

	[DllImport(BirdDLLPath)]
	public static extern int Thi_RetrieveCompassAdjustmentValues(string thimbleId);

	[DllImport(BirdDLLPath)]
	public static extern int Thi_RegisterOnBtnsCB(string thimbleId, DelegateOnBtns pFunc);

	[DllImport(BirdDLLPath)]
	public static extern int Thi_RegisterOnOFNCB(string thimbleId, DelegateOnOFN pFunc);
	[DllImport(BirdDLLPath)]
	public static extern int Thi_Start(string ThimbleId, bool start);

	[DllImport(BirdDLLPath)]
	public static extern int Thi_GetBtns(string thimbleId, IntPtr btns);

	[DllImport(BirdDLLPath)]
	public static extern int Thi_GetOFN(string thimbleId, IntPtr x, IntPtr y, IntPtr flags);

	[DllImport(BirdDLLPath)]
	public static extern int Thi_RegisterOnOrientPRYCB(string thimbleId, DelegateOnOrient pFunc);

	[DllImport(BirdDLLPath)]
	public static extern int Thi_RegisterOnOrientQCB(string thimbleId, DelegateOnOrient pFunc);

	[DllImport(BirdDLLPath)]
	public static extern int Thi_SetIMUData(string thimbleId, bool bOn);

	[DllImport(BirdDLLPath)]
	public static extern int Sens_Remove(string sensorId);

	[DllImport(BirdDLLPath)]
	public static extern int Sens_RegisterOnThiAddedCB(string sensorId, DelegateOnBirdAdded pFunc);

	[DllImport(BirdDLLPath)]
	public static extern int Thi_GetOrientPRY(string thimbleId, IntPtr orient);

	[DllImport(BirdDLLPath)]
	public static extern int Thi_GetOrientQ(string thimbleId, IntPtr orient);

	[DllImport(BirdDLLPath)]
	public static extern int Thi_GetThimbleInfo(string thimbleId, IntPtr status);

	[DllImport(BirdDLLPath)]
	public static extern int Thi_SetPostureHandshake(string thimbleId, Boolean bOn);
    #endregion
    public enum FoundSensors{
        SENSORS_NOT_FOUND,
        USB_SENSORS,
        BT_SENSORS
    };
    public enum PairingFlags
    {
        ALREADY_PAIRED = 1,
        ENFORCED_PARING = 2
    };
    #region Helper functions
    private int FailedI(int ret, string func_name, bool throwException = false, string optionalStr = "")
    {
        if (ret < 0)
        {
            String errStr = func_name + " fail " + optionalStr;
            WriteLine(errStr);
            if (throwException)
                throw new Exception(errStr);
        }
        return ret;
    }
    private bool Failed(int ret, string func_name, bool throwException = false, string optionalStr = "")
    {
        return FailedI(ret, func_name, throwException, optionalStr) < 0;
    }
    public void WriteLine(string msg)
    {
        Cli_LOG(msg);
        Console.WriteLine(msg);
        Debug.WriteLine(msg);
    }
    void PrintOptions(String[] options)
    {
        WriteLine("Please type item number or \"C\" for cancel or \"R\" for reprint: ");
        int count = 0;
        foreach (String opt in options)
        {
            WriteLine(count.ToString() + ") " + opt);
            count++;
        }
    }
    int SelectFromList(String[] options, out String selection)
    {
        selection = "";
        if (options == null)
            return -1;
        PrintOptions(options);
        ConsoleKeyInfo key;
        for (int tries = 0; tries < 3; tries++)
        {
            key = Console.ReadKey();
            WriteLine("");
            if (key.KeyChar == 'C' || key.KeyChar == 'c')
                return -1;
            if (key.KeyChar == 'R' || key.KeyChar == 'r')
            {
                PrintOptions(options);
                tries--;
                continue;
            }
            if (Char.IsDigit(key.KeyChar) && (key.KeyChar - '0') < options.Length)
            {
                int ind = key.KeyChar - '0';
                selection = options[ind];
                WriteLine("Selected: " + selection);
                return ind;
            }
            if (tries < 2)
                WriteLine("Invalid choice. Please try again");
        }
        WriteLine("No item selected");
        return -1;
    }

    #endregion
    #region Init and Connection issues

    //initializes client dll and recievs its version
    public void Init()
    {
        Console.WriteLine("Working directory: " + Directory.GetCurrentDirectory());
        IntPtr pVer = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(int)));
        if (Cli_Init(pVer) < 0)
        {
            throw new Exception("Cli_Init failed");
        }
        int ver = Marshal.ReadInt32(pVer);
        int majorVer = 0xFF & ver >> 0;
        int minorVer = 0xFF & ver >> 8;
        int buildVer = 0xFF & ver >> 16;
        WriteLine("clientVer = " + majorVer.ToString() + "." + minorVer.ToString() + "." + buildVer.ToString());
    }
    public void Shutdown()
    {
        Cli_Shutdown();
    }
    string SelectSensor()
    {
        String comPortString;
        String[] pSensNames;
        String[] pSensAddrs;
        FoundSensors foundSensors = DiscoverAvaliableSensors(out comPortString, out pSensNames, out pSensAddrs);
        string sensName = "";
        switch (foundSensors)
        {
            case FoundSensors.SENSORS_NOT_FOUND:
                return "";
            case FoundSensors.BT_SENSORS:
                int ind = SelectFromList(pSensNames, out sensName);
                if (ind >= 0)
                    SensorAddr = pSensAddrs[ind];
                break;
            case FoundSensors.USB_SENSORS:
                SelectFromList(new String[] { comPortString }, out sensName);
                break;
        }
        if (sensName != "")
            IsUsbSensor = (foundSensors == FoundSensors.USB_SENSORS);
        return sensName;
    }

    // connect to USB receiver or Base unit and registers callbacks for Birds pairing processing
    public void Connect()
    {
        String _sensorId = "";
        WriteLine("Looking for avaliable sensors...");
        for (int tries = 0; tries < 3 && String.IsNullOrEmpty(_sensorId); tries++)
        {
            _sensorId = SelectSensor();
        }
        if (String.IsNullOrEmpty(_sensorId))
        {
            WriteLine("No sensor selected");
            return;
        }
        int ret;
        // remove current sensor, if any
        if (SensorId != "")
        {
            WriteLine("Removing previosly connected sensor: " + SensorId);
            Sens_Remove(SensorId);
        }

        // connect to sensor
        ret = Sens_Generate(_sensorId, _sensorId);

        if (ret == -1)
        {
            throw new Exception("Sens_Generate fail, SensorId=" + _sensorId);
        }

        //Sens_BlockThimbleMessages(_sensorId, true);
        if (!IsUsbSensor)
        {
            WriteLine("Pairing with sensor: " + _sensorId);
            Failed(Cli_HostPairBluetoothDevice(_sensorId, SensorAddr), "Cli_HostPairBluetoothDevice", true, "SensorId=" + _sensorId);
        }
        _dOnBirdAdded = OnBirdAdded;
        Sens_RegisterOnThiAddedCB(SensorId, _dOnBirdAdded);

        // listen for a would-be-connected Bird 
        _dOnBirdPairing = OnBirdPairing;
        Sens_RegisterOnThiPairingPktCB(SensorId, _dOnBirdPairing);
        WriteLine("Connecting with sensor: " + _sensorId);
        for (int tries = 0; tries < 3; tries++)
        {
            Thread.Sleep(400);
            if (!Failed(Sens_Connect(_sensorId), "Sens_Connect", tries == 2, "SensorId=" + _sensorId))
                break;
        }
        WriteLine("Sensor connected!");
        SensorId = _sensorId;
    }


    // disconnect from Bird's base unit
    public void Disconnect()
    {
        Sens_Disconnect(SensorId);
        Sens_Remove(SensorId);
        SensorId = "";
    }

    void PairBird(String BirdId)
    {
        //Pairs last added Bird
        Failed(Sens_PairThimble(SensorId, BirdId), "Sens_PairThimble", false, "Bird Id=" + BirdId);
        bBirdDuringPairing = false;
    }

    // config the Bird
    void ConfigBird(String BirdId)
    {
        bBirdDuringPairing = false;
        WriteLine("try to ConfigBird " + BirdId);

        try
        {
            Thi_WakeUp(BirdId, 10);

            // request Bird to send IMU data
            //Thi_SetIMUData(BirdId, true);

            // register to event callbacks, if needed

            Thi_Start(BirdId, true);

        }
        catch (Exception ex)
        {
            WriteLine(ex.Message);
            return;
        }
        WriteLine("ConfigBird done");
        bBirdConfig = true;
    }

    //Discovering avaliable USB receiver or Bluetooth Base Units (if USB receiver is not detected).
    public FoundSensors DiscoverAvaliableSensors(out String comPortString, out String[] pSensNames, out String[] pSensAddrs)
    {
        FoundSensors foundSensors = FoundSensors.SENSORS_NOT_FOUND;

        int ret = 0;
        pSensNames = null;
        pSensAddrs = null;

        const int buffLenNames = 1024;
        const int buffLenAddrs = 1024;
        IntPtr pSensNamesBuff = Marshal.AllocHGlobal(buffLenNames);
        IntPtr pSensAddrsBuff = Marshal.AllocHGlobal(buffLenAddrs);
        char[] ncomPort = new char[9];
        comPortString = "";


        try
        {

            int maxDongleTries = 3;
            while (maxDongleTries > 0)
            {
                ret = FailedI(Cli_DiscoverBirdUsbSensor(pSensNamesBuff, buffLenNames), "Cli_DiscoverBirdUsbSensor");
                if (ret != 0)
                    break;
                maxDongleTries--;
            }

            if (ret == 1)
            {
                comPortString = Marshal.PtrToStringAnsi(pSensNamesBuff);
                foundSensors = FoundSensors.USB_SENSORS;
            }
            else
            {
                if (Failed (Cli_DiscoverBirdSensors(pSensNamesBuff, buffLenNames, pSensAddrsBuff, buffLenAddrs, "MUV"), "Cli_DiscoverBirdSensors"))
                    return FoundSensors.SENSORS_NOT_FOUND;

                String pSensNamesBuffStr = Marshal.PtrToStringAnsi(pSensNamesBuff);
                String pSensAddrsBuffStr = Marshal.PtrToStringAnsi(pSensAddrsBuff);
                pSensNames = pSensNamesBuffStr.Split(' ');
                pSensAddrs = pSensAddrsBuffStr.Split(' ');

                foundSensors = pSensNames.Length > 0 ? FoundSensors.BT_SENSORS : FoundSensors.SENSORS_NOT_FOUND;
            }
        }
        catch (Exception ex)
        {
            WriteLine("DiscoverAvaliableSensors exception : " + ex.Message);
        }
        return foundSensors;
    }
    #endregion


#region Examples
    public void MainMenu()
    {
        String[] options = new String[] { "Select mode", "Select Example", "Disconnect Bird", "Exit" };
        menuActivated = true;
        while (true)
        {
            String selectedStr = "";
            int choice = SelectFromList(options, out selectedStr);
            switch (choice)
            {
                case 0:
                    SelectMode();
                    break;
                case 1:
                    RunExample();
                    break;
                case 2:
                    DisconnectBird();
                    break;
                default:
                    return;
            }
        }
    }

    bool SelectMode()
    {
        String[] options = new String[] { "Single cursor", "Multi cursor" };
        String selectedStr = "";
        int choice = SelectFromList(options, out selectedStr);
        if (choice < 0)
            return false;
        isMultiBirdMode = choice == 1;
        Failed(Thi_SetMultitouchMode(isMultiBirdMode), "Thi_SetMultitouchMode");
        return true;
    }

    void RegisterOnOrientPRYCB()
    {
        Console.WriteLine ("Put Bird(s) on your finger(s) and press Pinch of/and Switch button");
        if (isMultiBirdMode)
        {
            foreach (String BirdId in BirdIds)
            {
                //IMU events will be activated by pressing pinch button
                Failed(Thi_RegisterOnOrientPRYCB(BirdId, HandleOnOrientPRY), "Thi_RegisterOnOrientPRYCB");
            }
        }
        else if (!String.IsNullOrEmpty(ActiveBirdId))
        {
            if (Failed(Thi_SetIMUData(ActiveBirdId, true), "Thi_SetIMUData", false, "IMU example failure."))
                return;
            Failed(Thi_RegisterOnOrientPRYCB(ActiveBirdId, HandleOnOrientPRY), "Thi_RegisterOnOrientPRYCB");
        }
    }
    void RegisterOnBtnsCB()
    {
        if (isMultiBirdMode)
        {
            foreach (String BirdId in BirdIds)
            {
                Failed(Thi_RegisterOnBtnsCB(BirdId, HandleOnBtns), "Thi_RegisterOnBtnsCB");
            }
        }
        else if (!String.IsNullOrEmpty(ActiveBirdId))
        {
            Failed(Thi_RegisterOnBtnsCB(ActiveBirdId, HandleOnBtns), "Thi_RegisterOnBtnsCB");
        }
    }
    void RegisterOnOFNCB()
    {
        if (isMultiBirdMode)
        {
            foreach (String BirdId in BirdIds)
            {
                Failed(Thi_RegisterOnOFNCB(BirdId, HandleOnOFN), "Thi_RegisterOnOFNCB");
            }
        }
        else if (!String.IsNullOrEmpty(ActiveBirdId))
        {
            Failed(Thi_RegisterOnOFNCB(ActiveBirdId, HandleOnOFN), "Thi_RegisterOnOFNCB");
        }
    }
    void CheckState()
    {
        for (packetsCount = 0; packetsCount < 20; packetsCount++)
        {
            if (isMultiBirdMode)
            {
                foreach (String BirdId in BirdIds)
                {
                    GetBirdState(BirdId, BirdId == ActiveBirdId);
                }
            }
            else
            {
                GetActiveBirdState();
            }
            Thread.Sleep(1000);
        }
    }

    void DisconnectBird()
    {
        WriteLine("Select Bird ");
        String BirdId = "";
        String [] birds = new String[BirdIds.Count];
        BirdIds.CopyTo(birds, 0);
        int choice = SelectFromList(birds, out BirdId);
        if (choice < 0)
            return;
        if (!Failed(Sens_UnpairThimble(SensorId, BirdId), "Sens_UnpairThimble"))
        {
            BirdIds.Remove(BirdId);
            WriteLine("Bird " + BirdId + " successfully disconected");
        }
    }

    void SyncBirdsList()
    {
        const int bufSize = 1024;
        IntPtr Birds = Marshal.AllocHGlobal(bufSize);
        if (Failed(Sens_GetPairedThimblesC(SensorId, bufSize, Birds), "Sens_GetPairedThimblesC", true))
            return;
        String BirdsStr = Marshal.PtrToStringAnsi(Birds);
        BirdIds = new List<string>(BirdsStr.Split(' '));
        WriteLine("Updated list of paired birds: ");
        foreach (String bird in BirdIds)
        {
            WriteLine("       " + bird);
        }
    }

    bool RunExample()
    {

        String[] options = new String[] { "IMU Callback", "OFN Callback", "Buttons Callback", "Check State", "Calibrate Bird", "Sync. paired Birds list" };
        packetsCount = 0;
        String selectedStr = "";
        int choice = SelectFromList(options, out selectedStr);
        if (choice < 0)
            return false;
        switch (choice)
        {
            case 0:
                RegisterOnOrientPRYCB();
                return true;
            case 1:
                RegisterOnOFNCB();
                return true;
            case 2:
                RegisterOnBtnsCB();
                return true;
            case 3:
                CheckState();
                return true;
            case 4:
                CalibrateBird(ActiveBirdId);
                return true;
            case 5:
                SyncBirdsList();
                return true;

        }
        return false;
    }
    string GetActiveBirdId()
    {
        string activeBird = "";
        IntPtr pActivBird = Marshal.AllocHGlobal(32);
        if (Failed(Sens_GetActiveThimbleId(SensorId, pActivBird, IntPtr.Zero, IntPtr.Zero), "Sens_GetActiveThimbleId"))
        {
            return "";
        }
        activeBird = Marshal.PtrToStringAnsi(pActivBird);
        return activeBird;
    }

	// get and print the Bird's state (orientation + buttons state)
	public void GetActiveBirdState()
    {
        GetBirdState(ActiveBirdId, true);
    }
	
    public void GetBirdState(String BirdId, bool isActive)
	{
        WriteLine("*********** " + packetsCount + ")  State of Bird: " + BirdId + " (" + (isActive ? "active)" : "not active)") + "  *************");
		// get Bird Buttons state
		IntPtr pBtns = Marshal.AllocHGlobal(4);
		Thi_GetBtns(BirdId, pBtns);
		int btns = Marshal.ReadInt32(pBtns);

		bool bPinch = Convert.ToBoolean( btns & (1 << 3) );
		bool bHome = Convert.ToBoolean( btns & (1 << 6) );
		bool bSwitch = Convert.ToBoolean( btns & (1 << 7) );

		WriteLine("bPinch = " + bPinch + " bSwitch = " + bSwitch + " bHome = " + bHome);

		// get Bird orientation
		IntPtr pOrient = Marshal.AllocHGlobal(4 * 3);
		Thi_GetOrientPRY(BirdId, pOrient);
		float pitch = GetFloat(pOrient);
		float roll = GetFloat(pOrient + 4);
		float yaw = GetFloat(pOrient + 8);

		WriteLine("pitch=" + pitch + ", roll=" + roll + ", yaw=" + yaw);

		// get Bird orientation quaternion
		IntPtr pOrientQ = Marshal.AllocHGlobal(4 * 4);
		Thi_GetOrientQ(BirdId, pOrientQ);
		float q0 = GetFloat(pOrientQ);
		float q1 = GetFloat(pOrientQ + 4);
		float q2 = GetFloat(pOrientQ + 8);
		float q3 = GetFloat(pOrientQ + 12);

		WriteLine("q0(w)=" + q0 + ", q1(x)=" + q1 + ", q2(y)=" + q2 + ", q3(z)=" + q3);

		IntPtr pStatus = Marshal.AllocHGlobal(4*20);
		Thi_GetThimbleInfo(BirdId, pStatus);
		bool bFingerDetected = Marshal.ReadByte(pStatus) == 1;

		WriteLine("bFingerDetected = " + bFingerDetected);

		// get Bird OFN state
		IntPtr pX = Marshal.AllocHGlobal(4);
		IntPtr pY = Marshal.AllocHGlobal(4);
		IntPtr pFlags = Marshal.AllocHGlobal(1);
		Thi_GetOFN(BirdId, pX, pY, pFlags);
		int OFN_X = Marshal.ReadInt32(pX);
		int OFN_Y = Marshal.ReadInt32(pY);
		byte OFNflags = Marshal.ReadByte(pFlags);

		bool OFN_fingerPresent = Convert.ToBoolean(OFNflags & (1 << 7));
		bool OFN_tap_hold_bit = Convert.ToBoolean(OFNflags & (1 << 6));
		bool OFN_dblclk_bit = Convert.ToBoolean(OFNflags & (1 << 5));
		bool OFN_click_bit = Convert.ToBoolean(OFNflags & (1 << 4));

		WriteLine("OFN X= " + OFN_X + ", Y= " + OFN_Y);
		WriteLine(
			"OFN fingerPresent=" + OFN_fingerPresent+
			", tap_hold_bit=" + OFN_tap_hold_bit +
			", dblclk_bit=" + OFN_dblclk_bit +
			", click_bit=" + OFN_click_bit );
        packetsCount++;
	}

	float GetFloat(IntPtr pInt) {
		byte[] bytes = {
				Marshal.ReadByte(pInt), 
				Marshal.ReadByte((IntPtr)(pInt.ToInt32() + 1)),
				Marshal.ReadByte((IntPtr)(pInt.ToInt32() + 2)),
				Marshal.ReadByte((IntPtr)(pInt.ToInt32() + 3))};
		return BitConverter.ToSingle(bytes, 0);
	}

	// orientation change callback
	void HandleOnOrientPRY(IntPtr pBirdId, IntPtr pOrient)
	{
        String BirdId = Marshal.PtrToStringAnsi(pBirdId);
        float pitch = GetFloat(pOrient);
		float roll = GetFloat(pOrient + 4);
		float yaw = GetFloat(pOrient + 8);

        WriteLine(packetsCount + ") OrientPRY BirdId=" + BirdId + "; pitch=" + pitch + ", roll=" + roll + ", yaw=" + yaw);
        packetsCount++;
        if (packetsCount > PAKCETS_LIMIT)
        {
            Failed(Thi_RegisterOnBtnsCB(BirdId, null), "Thi_RegisterOnBtnsCB");
        }
    }


	// buttons event callback
    void HandleOnBtns(IntPtr pBirdId, int num)
    {
        String BirdId = Marshal.PtrToStringAnsi(pBirdId);
        WriteLine(packetsCount + ") HandleOnBtns  BirdId=" + BirdId + ";  flags=" + num);
        packetsCount++;
        if (packetsCount > PAKCETS_LIMIT)
        {
            Failed(Thi_RegisterOnBtnsCB(BirdId, null), "Thi_RegisterOnBtnsCB");
        }
    }

    void HandleOnOFN(IntPtr pBirdId, int x, int y, byte flags)
    {
        String BirdId = Marshal.PtrToStringAnsi(pBirdId);
        WriteLine(packetsCount + ") OFN BirdId=" + BirdId + ":  x=" + x + ", y=" + y + ", flags=" + flags);
        packetsCount++;
        if (packetsCount > PAKCETS_LIMIT)
        {
            Failed(Thi_RegisterOnOFNCB(BirdId, null), "Thi_RegisterOnOFNCB");
        }
	}

	// call upon detection of Bird in the area
	void OnBirdAdded(int birdId, int birdNum)
	{
        Cli_LOG("OnBirdAdded: " + birdId);
		WriteLine("OnBirdAdded " + birdId + " " + birdNum);
		string BirdId = birdId.ToString("x");
        WriteLine("BirdId=" + BirdId);
        BirdIds.Add(BirdId);
		// call to config the Bird
		Thread thread = new Thread(() => ConfigBird(BirdId));
		thread.Start();
	}

	// call upon detection of a Bird trying to pair
    void OnBirdPairing(int birdId, int pairingFlags)
    {
        if (bBirdDuringPairing)
        {//otherwhise we will enter to this function on each pairing packet
            return;
        }
        bool alreadyPaired = (pairingFlags & (int) PairingFlags.ALREADY_PAIRED) > 0;
        bool enforcedPairing = (pairingFlags & (int) PairingFlags.ENFORCED_PARING) > 0; //means that Bird was connected in the past to this sensor

        var _BirdId = birdId.ToString("X");
        bool pairThi = false;
        if (BirdIds.IndexOf(_BirdId) < 0 && !alreadyPaired)
        {
            WriteLine("got OnBirdPairing from device " + _BirdId);
            if (!menuActivated) 
            { 
                WriteLine("Do you want to connect (Y/N)?");
                var res = Console.ReadLine();
                if (res.ToUpper() == "Y")
                {
                    pairThi = true;
                }
            }
            else
            {
                WriteLine("Silent paring of Bird: " + _BirdId);
                pairThi = true;
            }
        }
        else
            pairThi = true;
        if (pairThi)
        {
            while (bBirdDuringPairing)
                Thread.Sleep(100); //whating when pairing of previose Bird will be finished
            // call to pair the Bird
            Thread thread = new Thread(() => PairBird(_BirdId));
            thread.Start();
        }
        else
        {
            bBirdDuringPairing = false;
        }
    }

 
    //Activates calibration of IMU unit in specified Bird
    private void CalibrateBird(String BirdId)
    {
        //WriteLine("Run IMU calibration now? (y/n)");
        WriteLine("(hold the Bird steady before clicking Enter; calib. takes several seconds)");
        Console.ReadLine();
        WriteLine("start IMU calibration...");
        Failed(Thi_InitIMUCalib(BirdId, delegate(int state, int progress)
        {
            if (state == 0)
            {
                WriteLine("IMU calibrated!");
            }
            else
                if (state == 2)
                {
                    WriteLine("\r" + progress + " % done");
                    return;
                }
                else
                {
                    WriteLine("ERROR: IMU not calibrated");
                }
            if (0 == Thi_RetrieveCompassAdjustmentValues(BirdId))
            {
                WriteLine("Compass found, press any key to start calibration...");
                Console.ReadLine();
                WriteLine("start Compass calibration...");
                Thi_InitCompassCalib(BirdId, delegate(int _state, int _progress)
                {
                    if (_state == 0)
                    {
                        WriteLine("Compass calibrated!");
                    }
                    else
                    {
                        WriteLine("ERROR: Compass not calibrated");
                    }
                    bBirdConfig = true;
                });
            }
            else
            {
                WriteLine("no compass found");
                bBirdConfig = true;
            }
        }), "Thi_InitIMUCalib", true);
    }
}
#endregion 