﻿/******************************************************************************
	Module:	SDK Examples
	File:	Program.cs
	Desc:	Bird connection example
	Author:	#gr
	Creatd:	3-2016
	Remarks:
	Changs:

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace BirdIMUData
{
	class Program
	{
		static void Main(string[] args)
		{
			try {
				// create a BirdConnect instance
				_conn = new BirdConnect();

				// init the Bird connection
				// - call Cli_Init 
				_conn.Init();

				// connect to a Base Unit
				// - we assume that the Base Unit is already paired to the PC
				// and its com port number is known
				//Console.WriteLine("Enter Base Unit's com port name:");
				// allow also for emulator connection
				//Console.WriteLine("(for emulator, start the emulator and enter 'e')");
				//var comName = Console.ReadLine();

                //if (comName == "e") {
                //    comName = "127.0.0.1:2727";
                //}
				// do the connection
				// - here we call Sens_Generate and Sens_Connect
				_conn.Connect();

				Console.WriteLine("wait for thimble connection or pairing request...");
				while (!_conn.bBirdConfig) {
					Thread.Sleep(100);
				}


				// Runs examples
                _conn.MainMenu();
				// shut down
				_conn.Shutdown();
			}
			catch (Exception ex) {
				Console.WriteLine(ex.Message);
				Console.Read();
			}
		}

		static BirdConnect _conn;

	}
}
