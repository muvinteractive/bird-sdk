﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmImuTest
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TxtBtnHome = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BtnBack = New System.Windows.Forms.Button()
        Me.BtnQuit = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.TxtP = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TxtR = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TxtBtnPinch = New System.Windows.Forms.TextBox()
        Me.TxtBtnSwitch = New System.Windows.Forms.TextBox()
        Me.TxtOnFinger = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.ButtonReadVals = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TxtBtnHome
        '
        Me.TxtBtnHome.Enabled = False
        Me.TxtBtnHome.Location = New System.Drawing.Point(25, 50)
        Me.TxtBtnHome.Name = "TxtBtnHome"
        Me.TxtBtnHome.Size = New System.Drawing.Size(39, 20)
        Me.TxtBtnHome.TabIndex = 47
        Me.TxtBtnHome.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(26, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 13)
        Me.Label1.TabIndex = 46
        Me.Label1.Text = "Home"
        '
        'BtnBack
        '
        Me.BtnBack.Location = New System.Drawing.Point(107, 214)
        Me.BtnBack.Name = "BtnBack"
        Me.BtnBack.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BtnBack.Size = New System.Drawing.Size(77, 23)
        Me.BtnBack.TabIndex = 45
        Me.BtnBack.Text = "Back"
        Me.BtnBack.UseVisualStyleBackColor = True
        '
        'BtnQuit
        '
        Me.BtnQuit.Location = New System.Drawing.Point(21, 214)
        Me.BtnQuit.Name = "BtnQuit"
        Me.BtnQuit.Size = New System.Drawing.Size(77, 23)
        Me.BtnQuit.TabIndex = 43
        Me.BtnQuit.Text = "Quit"
        Me.BtnQuit.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'TxtP
        '
        Me.TxtP.Enabled = False
        Me.TxtP.Location = New System.Drawing.Point(127, 101)
        Me.TxtP.Name = "TxtP"
        Me.TxtP.Size = New System.Drawing.Size(140, 20)
        Me.TxtP.TabIndex = 53
        Me.TxtP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(18, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 13)
        Me.Label4.TabIndex = 52
        Me.Label4.Text = "Pitch (deg):"
        '
        'TxtR
        '
        Me.TxtR.Enabled = False
        Me.TxtR.Location = New System.Drawing.Point(127, 135)
        Me.TxtR.Name = "TxtR"
        Me.TxtR.Size = New System.Drawing.Size(140, 20)
        Me.TxtR.TabIndex = 55
        Me.TxtR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(18, 138)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 54
        Me.Label2.Text = "Roll (deg):"
        '
        'TxtBtnPinch
        '
        Me.TxtBtnPinch.Enabled = False
        Me.TxtBtnPinch.Location = New System.Drawing.Point(87, 51)
        Me.TxtBtnPinch.Name = "TxtBtnPinch"
        Me.TxtBtnPinch.Size = New System.Drawing.Size(39, 20)
        Me.TxtBtnPinch.TabIndex = 56
        Me.TxtBtnPinch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TxtBtnSwitch
        '
        Me.TxtBtnSwitch.Enabled = False
        Me.TxtBtnSwitch.Location = New System.Drawing.Point(151, 52)
        Me.TxtBtnSwitch.Name = "TxtBtnSwitch"
        Me.TxtBtnSwitch.Size = New System.Drawing.Size(39, 20)
        Me.TxtBtnSwitch.TabIndex = 57
        Me.TxtBtnSwitch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TxtOnFinger
        '
        Me.TxtOnFinger.Enabled = False
        Me.TxtOnFinger.Location = New System.Drawing.Point(215, 52)
        Me.TxtOnFinger.Name = "TxtOnFinger"
        Me.TxtOnFinger.Size = New System.Drawing.Size(39, 20)
        Me.TxtOnFinger.TabIndex = 58
        Me.TxtOnFinger.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(88, 29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 13)
        Me.Label3.TabIndex = 59
        Me.Label3.Text = "Pinch"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(151, 28)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(38, 13)
        Me.Label5.TabIndex = 60
        Me.Label5.Text = "Touch"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(215, 29)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 13)
        Me.Label6.TabIndex = 61
        Me.Label6.Text = "On Finger"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(260, 66)
        Me.GroupBox1.TabIndex = 63
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Bird Buttons"
        '
        'GroupBox2
        '
        Me.GroupBox2.Location = New System.Drawing.Point(1, 70)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(260, 84)
        Me.GroupBox2.TabIndex = 64
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Bird Orientation"
        '
        'GroupBox3
        '
        Me.GroupBox3.Location = New System.Drawing.Point(11, 83)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(262, 93)
        Me.GroupBox3.TabIndex = 64
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Bird Orientation"
        '
        'ButtonReadVals
        '
        Me.ButtonReadVals.Location = New System.Drawing.Point(22, 181)
        Me.ButtonReadVals.Name = "ButtonReadVals"
        Me.ButtonReadVals.Size = New System.Drawing.Size(123, 23)
        Me.ButtonReadVals.TabIndex = 65
        Me.ButtonReadVals.Text = "Read values direct"
        Me.ButtonReadVals.UseVisualStyleBackColor = True
        '
        'FrmImuTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.ButtonReadVals)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TxtOnFinger)
        Me.Controls.Add(Me.TxtBtnSwitch)
        Me.Controls.Add(Me.TxtBtnPinch)
        Me.Controls.Add(Me.TxtR)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TxtP)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TxtBtnHome)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BtnBack)
        Me.Controls.Add(Me.BtnQuit)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Name = "FrmImuTest"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "IMU Data  / Buttons"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TxtBtnHome As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BtnBack As System.Windows.Forms.Button
    Friend WithEvents BtnQuit As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents TxtP As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TxtR As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TxtBtnPinch As System.Windows.Forms.TextBox
    Friend WithEvents TxtBtnSwitch As System.Windows.Forms.TextBox
    Friend WithEvents TxtOnFinger As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonReadVals As System.Windows.Forms.Button
End Class
