﻿Imports System.Threading
Imports System.Runtime.InteropServices

Public Class FrmSensorConnect
    Private BirdAddedDelegate As DelegateOnBirdAdded = New DelegateOnBirdAdded(AddressOf OnBirdAddedCallback)
	Private fwUpgradeDelegate As DelegateFwUpgrade = New DelegateFwUpgrade(AddressOf FwUpgradeCB)
	Dim fileName As String = "D:\Box Sync\R&D, Technology\Software\Versions\LatestVersion\SensorApp_4100.s19"

    Private Sub FrmSensorConnect_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		Dim i As Integer
		Dim pStruct As IntPtr = Marshal.AllocHGlobal(1024)
		' show existing COM ports
        For i = 0 To 255
            If (QueryDosDevice("COM" & i, pStruct, 1024) <> 0) Then
                CmbComPort.Items.Add("COM" & i)
            End If
        Next
        ' add also the Bird emulator's IP (this host)
		CmbComPort.Items.Add("127.0.0.1:2727")

				If (sensorId = "") Then
						BtnNext.Enabled = False
						BtnConnect.Enabled = False
						BtnDisconnect.Enabled = False
				Else
						CmbComPort.Text = sensorId
						BtnNext.Enabled = True
						BtnConnect.Enabled = False
						BtnDisconnect.Enabled = True
				End If

        If Not bInit Then
            Dim ret As Integer
            Dim pVer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(GetType(Integer)))
            ' Initialize the Bird DLL
            ret = Cli_Init(pVer)

            Dim ver As Integer = Marshal.ReadInt32(pVer)
            ' show the DLL's version
            Dim majorVer As Integer = &HFF And ver >> 0
            Dim minorVer As Integer = &HFF And ver >> 8
            Dim buildVer As Integer = &HFF And ver >> 16

            If (ret = -1) Then
                MsgBox("Cli_Init fail")
            Else
                bInit = True
                MsgBox("Client ver: " & Hex(majorVer) & "." & Hex(minorVer) & "." & Hex(buildVer))
            End If
        End If
	End Sub


    Private Sub BtnQuit_Click(sender As Object, e As EventArgs) Handles BtnQuit.Click
        If (sensorId <> "") Then
            Sens_Disconnect(sensorId)
        End If
        End
    End Sub

    Private Sub BtnNext_Click(sender As Object, e As EventArgs) Handles BtnNext.Click
        _frmThimbleConnect.Show()
        Me.Close()
    End Sub

    Private Sub BtnConnect_Click(sender As Object, e As EventArgs) Handles BtnConnect.Click
        Dim ret As Integer

        If (sensorId <> "") Then
            ' remove any previous sensor objects
            Sens_Remove("")
        End If

        sensorId = CmbComPort.Text

        ' create a sensor object
        ret = Sens_Generate(sensorId, sensorId)
        If (ret = -1) Then
            MsgBox("Sens_Generate fail", MsgBoxStyle.Critical)
            Return
        End If
        ' connect to a physical sensor 
        ret = Sens_Connect(sensorId)
        If (ret = -1) Then
            MsgBox("Sens_Connect fail", MsgBoxStyle.Critical)
            Return
        End If

        Sens_RegisterOnThiAddedCB(sensorId, BirdAddedDelegate)

        BtnNext.Enabled = True
        BtnNext.Focus()
        BtnConnect.Enabled = False
        BtnDisconnect.Enabled = True
    End Sub

    Private Sub BtnDisconnect_Click(sender As Object, e As EventArgs) Handles BtnDisconnect.Click
        Dim ret As Integer
        If (sensorId <> "") Then
            ret = Sens_Disconnect(sensorId)
            If (ret = -1) Then
                MsgBox("Sens_Disconnect fail")
                Return
            End If
            BtnNext.Enabled = False
            BtnConnect.Enabled = True
            BtnDisconnect.Enabled = False
        End If
    End Sub

    Private Sub CmbComPort_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CmbComPort.SelectedIndexChanged
        BtnConnect.Enabled = True
    End Sub

    Private Sub CmbComPort_KeyPress(sender As Object, e As KeyPressEventArgs) Handles CmbComPort.KeyPress

        If Asc(e.KeyChar) = Keys.Enter Then

            BtnConnect.Enabled = True
            BtnConnect.PerformClick()

        End If

    End Sub

    Public Sub OnBirdAddedCallback(ByVal birdId As Integer, ByVal birdNum As Integer)
        ' here all callback logic
        Console.WriteLine("OnBirdAddedCallback: birdId=" & Hex(birdId))
        thimbleId = Hex(birdId)
        _frmThimbleConnect.ThimbleIdUpdated()
    End Sub

	Public Sub FwUpgradeCB(ByVal msg As IntPtr, ByVal msgType As Integer, ByVal progress As Integer)
		If TxtFWUProgress.InvokeRequired Then
			TxtFWUProgress.Invoke(fwUpgradeDelegate, msg, msgType, progress)
		Else
			If msg <> 0 Then
				Console.WriteLine(Marshal.PtrToStringAnsi(msg))
			Else
				TxtFWUProgress.Text = progress
			End If
		End If
	End Sub

	Private Sub Btn_LoadFW_Click(sender As Object, e As EventArgs) Handles Btn_LoadFW.Click
		If sensorId = "" Then
			Console.WriteLine("No sensor connected")
		End If
		Dim openFileDialog1 As New OpenFileDialog()
		openFileDialog1.Filter = "S19 files (*.S19)|*.S19"
		openFileDialog1.FilterIndex = 1
		openFileDialog1.RestoreDirectory = True

		If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
			fileName = openFileDialog1.FileName()
		Else
			Return
		End If

		Dim th As New Thread(AddressOf SensUpdateFirmware)
		th.Start()

	End Sub
	Private Sub SensUpdateFirmware(obj As Object)
		Dim ret As Integer = Sens_UpdateFirmware(sensorId, fileName, fwUpgradeDelegate)
		If (ret = 0) Then
		End If
	End Sub

End Class
