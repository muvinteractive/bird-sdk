﻿Imports System.Runtime.InteropServices
Module Module2
	Public Delegate Sub DelegateOnOrientPRY(ByVal tOrientPRY As Integer)
	Public Delegate Sub DelegateOnBtns(ByVal num As Integer)
	Public Delegate Sub DelegateBirdPairing(ByVal birdId As Integer)
	Public Delegate Sub DelegateOnBirdAdded(ByVal birdId As Integer, ByVal birdNum As Integer)
	Public Delegate Sub DelegateFwUpgrade(ByVal msg As IntPtr, ByVal msgType As Integer, ByVal progress As Integer)
	Public thimbleId As String, sensorId As String
	Public bInit As Boolean
	Public _frmThimbleConnect As FrmThimbleConnect = New FrmThimbleConnect
	Public Const BirdDLLPath As String = "./MUVClientWin.dll"

	' import all required Bird API from the local DLL
	<DllImport(BirdDLLPath)> Public Function Cli_Init(ByVal pVer As IntPtr) As Integer
	End Function

	<DllImport(BirdDLLPath)> Public Function Sens_Generate(ByVal sensorId As String, ByVal com As String) As Integer
	End Function

	<DllImport(BirdDLLPath)> Public Function Sens_Connect(ByVal sensorId As String) As Integer
	End Function

	<DllImport(BirdDLLPath)> Public Function Sens_Disconnect(ByVal sensorId As String) As Integer
	End Function

	<DllImport(BirdDLLPath)> Public Function Sens_UpdateFirmware(ByVal sensorId As String, ByVal filePath As String, ByVal callbacks As DelegateFwUpgrade) As Integer
	End Function

	<DllImport(BirdDLLPath)> Public Function Sens_UnpairAllThi(ByVal sensorId As String) As Integer
	End Function

	<DllImport(BirdDLLPath)> Public Function Sens_RegisterOnThiPairingPktCB(ByVal sensorId As String, ByVal pFunc As DelegateBirdPairing) As Integer
	End Function

	<DllImport(BirdDLLPath)> Public Function Sens_PairThimble(ByVal sensorId As String, ByVal thimbleId As String) As Integer
	End Function

	<DllImport(BirdDLLPath)> Public Function Thi_RegisterOnBtnsCB(ByVal thimbleId As String, ByVal pFunc As DelegateOnBtns) As Integer
	End Function

	<DllImport(BirdDLLPath)> Public Function Thi_RegisterOnOrientPRYCB(ByVal thimbleId As String, ByVal pFunc As DelegateOnOrientPRY) As Integer
	End Function

	<DllImport(BirdDLLPath)> Public Function Thi_SetIMUData(ByVal thimbleId As String, ByVal bOn As Boolean) As Integer
	End Function

	<DllImport(BirdDLLPath)> Public Function Sens_Remove(ByVal sensorId As String) As Integer
	End Function

	<DllImport(BirdDLLPath)> Public Function Sens_RegisterOnThiAddedCB(ByVal sensorId As String, ByVal pFunc As DelegateOnBirdAdded) As Integer
	End Function

	' import this one for convenience
	<DllImport("kernel32.dll")> Public Function QueryDosDevice(ByVal lpDeviceName As String, ByVal lpTargetPath As IntPtr, ByVal ucchMax As Integer) As Integer
	End Function

	<DllImport(BirdDLLPath)> Public Function Thi_GetBtns(ByVal thimbleId As String, ByVal btns As IntPtr) As Integer
	End Function

	<DllImport(BirdDLLPath)> Public Function Thi_GetOrientPRY(ByVal thimbleId As String, ByVal orient As IntPtr) As Integer
	End Function
End Module

