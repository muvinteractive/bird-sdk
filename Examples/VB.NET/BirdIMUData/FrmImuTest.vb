﻿Imports System.Runtime.InteropServices


Public Class FrmImuTest
    Private OrientPRYDelegate As DelegateOnOrientPRY = New DelegateOnOrientPRY(AddressOf OrientPRYCallback)
    Private ButtonsDelegate As DelegateOnBtns = New DelegateOnBtns(AddressOf OnButtonsCallback)
    Public Delegate Sub ShowOrientPRYDelegate()
		Private DelegateShowOrientPRY As ShowOrientPRYDelegate = New ShowOrientPRYDelegate(AddressOf ShowOrientPRY)

		Dim pitch As Double
		Dim roll As Double
		Dim yaw As Double

    Private Sub Form_OnLoad(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim ret As Integer
        ret = Thi_SetIMUData(thimbleId, True)
        If (ret = -1) Then
            MsgBox("Thi_SetGestures fail")
        End If
				ret = Thi_RegisterOnOrientPRYCB(thimbleId, OrientPRYDelegate)
				If (ret = -1) Then
						MsgBox("Thi_RegisterOnOrientPRYCB fail")
				End If
				ret = Thi_RegisterOnBtnsCB(thimbleId, ButtonsDelegate)
				If (ret = -1) Then
						MsgBox("Thi_RegisterOnBtnsCB fail")
				End If
        BtnBack.Enabled = True
        BtnBack.Focus()

    End Sub

		Private Function GetSingle(pInt As IntPtr) As Single
				Dim bytes(4) As Byte
				bytes(0) = Marshal.ReadByte(pInt)
				bytes(1) = Marshal.ReadByte(pInt + 1)
				bytes(2) = Marshal.ReadByte(pInt + 2)
				bytes(3) = Marshal.ReadByte(pInt + 3)
				Return BitConverter.ToSingle(bytes, 0)
		End Function

		Private Sub ReadRPY(pOrient As IntPtr)
				pitch = GetSingle(pOrient)
				roll = GetSingle(pOrient + 4)
				yaw = GetSingle(pOrient + 8)
		End Sub

		Private Sub BtnQuit_OnClick(sender As Object, e As EventArgs) Handles BtnQuit.Click
				Sens_Disconnect(sensorId)
				End
		End Sub

		Private Sub BtnBack_OnClick(sender As Object, e As EventArgs) Handles BtnBack.Click
				_frmThimbleConnect.Show()
				Me.Close()
		End Sub

		Private Sub ButtonReadVals_OnClick(sender As Object, e As EventArgs) Handles ButtonReadVals.Click
				' read the values directly (not through the callback mech.)

				Dim pBtns As IntPtr = Marshal.AllocHGlobal(4)
				Thi_GetBtns(thimbleId, pBtns)
				Dim btns As Integer = Marshal.ReadInt32(pBtns)
				Console.WriteLine(Hex(btns))


				Dim pOrient As IntPtr = Marshal.AllocHGlobal(4 * 3)
				Thi_GetOrientPRY(thimbleId, pOrient)
				ReadRPY(pOrient)
				Console.WriteLine("pitch=" & pitch & ", roll=" & roll & ", yaw" & yaw)
				ShowOrientPRY()

		End Sub

		Public Sub OnButtonsCallback(ByVal btns As Integer)
				If BtnQuit.InvokeRequired Then
						' if we didn't create this form, call invoke
						BtnQuit.BeginInvoke(ButtonsDelegate, btns)
				Else
						Dim bHome As Boolean = btns And (1 << 6)
						Dim bFingerDetected As Boolean = btns And (1 << 2)
						Dim bPinch As Boolean = btns And (1 << 3)
						Dim bSwitch As Boolean = btns And (1 << 7)
						If bFingerDetected Then
								TxtOnFinger.Text = "On"
								TxtOnFinger.BackColor = Color.Red
						Else
								TxtOnFinger.Text = "Off"
								TxtOnFinger.BackColor = Color.White
						End If
						If bPinch Then
								TxtBtnPinch.Text = "On"
								TxtBtnPinch.BackColor = Color.Red
						Else
								TxtBtnPinch.Text = "Off"
								TxtBtnPinch.BackColor = Color.White
						End If
						If bSwitch Then
								TxtBtnSwitch.Text = "On"
								TxtBtnSwitch.BackColor = Color.Red
						Else
								TxtBtnSwitch.Text = "Off"
								TxtBtnSwitch.BackColor = Color.White
						End If
						If bHome Then
								TxtBtnHome.Text = "On"
								TxtBtnHome.BackColor = Color.Red
						Else
								TxtBtnHome.Text = "Off"
								TxtBtnHome.BackColor = Color.White
						End If
				End If
		End Sub

		Public Sub OrientPRYCallback(ByVal orientPRY As Integer)
				If BtnQuit.InvokeRequired Then
						ReadRPY(orientPRY)
						' if we didn't create this form, call invoke
						BtnQuit.BeginInvoke(DelegateShowOrientPRY)
				Else
				End If
		End Sub

		Private Sub ShowOrientPRY()
				TxtP.Text = Math.Round(pitch, 2)
				TxtR.Text = Math.Round(roll, 2)
		End Sub
End Class