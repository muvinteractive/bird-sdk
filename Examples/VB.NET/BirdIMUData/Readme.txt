/******************************************************************************
	Module:	MUV Bird SDK - VB.NET Bird Examples
	File:	Readme.txt
	Desc:	IMU Data + Buttons VB Example
	Author:	#gr
	Creatd:	2-2016
	Remarks: 

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

Running the Example:
- Connect the Base Unit to power and get the Bird ready
- Discover and add the Base Unit as a Bluetooth device to your PC; find its COM port 
number (by opening the Bluetooth Settings).
- Open the project in VS and run it.
- Select the COM port of your Unit and connect; click Next
- Connect to the Bird (if was not previously connected); click Next
- the last screen shows the Bird orientation + buttons state.
