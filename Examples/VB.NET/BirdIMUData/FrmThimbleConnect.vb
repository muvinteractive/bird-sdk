﻿Imports System.Runtime.InteropServices
Public Class FrmThimbleConnect
    Private BirdPairingDelegate As DelegateBirdPairing = New DelegateBirdPairing(AddressOf OnThimblePairingPktCallback)
    Public Delegate Sub ThimbleIdUpdatedDelegate()
    Dim _thimbleIdUpdatedDelegate As ThimbleIdUpdatedDelegate = New ThimbleIdUpdatedDelegate(AddressOf ThimbleIdUpdated)

    Public _sensor As FrmSensorConnect


    Private Sub FormOnLoad(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim ret As Integer
        ret = Sens_RegisterOnThiPairingPktCB(sensorId, BirdPairingDelegate)
        If (ret = -1) Then
            MsgBox("Sens_RegisterOnThiPairingPktCB fail")
        End If

        If (thimbleId = "") Then
            BtnNext.Enabled = False
            BtnConnect.Enabled = False
            BtnDisconnect.Enabled = False
            TxtInstructions.Text = "Put the Bird on and press a long HOME"
        Else
            BtnConnect.Enabled = False
            BtnDisconnect.Enabled = True
            TxtThimbleId.Text = thimbleId
            BtnNext.Enabled = True
            TxtInstructions.Text = "Put on the Bird and click Next to continue"
        End If
    End Sub

    Private Sub BtnQuit_Click(sender As Object, e As EventArgs) Handles BtnQuit.Click
        Sens_Disconnect(sensorId)
        End
    End Sub

    Private Sub BtnBack_Click(sender As Object, e As EventArgs) Handles BtnBack.Click
        FrmSensorConnect.Show()
        Me.Close()
    End Sub

    Private Sub BtnNext_Click(sender As Object, e As EventArgs) Handles BtnNext.Click
        FrmImuTest.Show()
        Me.Hide()
    End Sub


    Private Sub BtnConnect_Click(sender As Object, e As EventArgs) Handles BtnConnect.Click
        Dim ret As Integer
        ret = Sens_PairThimble(sensorId, thimbleId)
        If (ret = -1) Then
            MsgBox("Sens_PairThimble fail")
            Return
        End If
        BtnNext.Enabled = True
        BtnConnect.Enabled = False
        BtnDisconnect.Enabled = True
    End Sub

    Private Sub BtnDisconnect_Click(sender As Object, e As EventArgs) Handles BtnDisconnect.Click
        Dim ret As Integer
        ret = Sens_UnpairAllThi(sensorId)
        If (ret = -1) Then
            MsgBox("Thi_UnpairThimble fail")
            Return
        End If
        thimbleId = ""
        TxtThimbleId.Text = ""

        BtnNext.Enabled = False
        BtnConnect.Enabled = False
        BtnDisconnect.Enabled = False
    End Sub


    Public Sub OnThimblePairingPktCallback(ByVal birdId As Integer)
        If thimbleId <> "" Then
            Return
        End If
        If BtnQuit.InvokeRequired Then
            ' if we didn't create this form, call invoke
            BtnQuit.BeginInvoke(BirdPairingDelegate, birdId)
        Else
            ' here all callback logic
            Console.WriteLine("OnThimblePairingPktCallback " & Hex(birdId))
            thimbleId = Hex(birdId)
            TxtThimbleId.Text = Hex(birdId)
            BtnConnect.Enabled = True
            BtnConnect.Focus()
        End If

    End Sub

    Public Sub ThimbleIdUpdated()
        Console.WriteLine("ThimbleIdUpdated")
        If TxtThimbleId.InvokeRequired Then
						TxtThimbleId.BeginInvoke(_thimbleIdUpdatedDelegate)
        Else
            TxtThimbleId.Text = thimbleId
            TxtInstructions.Text = "Put on the Bird and click Next to continue"
            BtnNext.Enabled = True
            BtnNext.Focus()
        End If
    End Sub

End Class