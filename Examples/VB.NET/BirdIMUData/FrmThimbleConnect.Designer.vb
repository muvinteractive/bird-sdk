﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmThimbleConnect
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.TxtThimbleId = New System.Windows.Forms.TextBox()
		Me.BtnDisconnect = New System.Windows.Forms.Button()
		Me.BtnConnect = New System.Windows.Forms.Button()
		Me.BtnQuit = New System.Windows.Forms.Button()
		Me.BtnNext = New System.Windows.Forms.Button()
		Me.BtnBack = New System.Windows.Forms.Button()
		Me.TxtInstructions = New System.Windows.Forms.Label()
		Me.SuspendLayout()
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(26, 70)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(40, 13)
		Me.Label1.TabIndex = 2
		Me.Label1.Text = "Bird Id:"
		'
		'TxtThimbleId
		'
		Me.TxtThimbleId.Enabled = False
		Me.TxtThimbleId.Location = New System.Drawing.Point(140, 70)
		Me.TxtThimbleId.Name = "TxtThimbleId"
		Me.TxtThimbleId.Size = New System.Drawing.Size(121, 20)
		Me.TxtThimbleId.TabIndex = 6
		Me.TxtThimbleId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
		'
		'BtnDisconnect
		'
		Me.BtnDisconnect.Location = New System.Drawing.Point(153, 108)
		Me.BtnDisconnect.Name = "BtnDisconnect"
		Me.BtnDisconnect.Size = New System.Drawing.Size(108, 23)
		Me.BtnDisconnect.TabIndex = 9
		Me.BtnDisconnect.TabStop = False
		Me.BtnDisconnect.Text = "Disconnect"
		Me.BtnDisconnect.UseVisualStyleBackColor = True
		'
		'BtnConnect
		'
		Me.BtnConnect.Location = New System.Drawing.Point(22, 108)
		Me.BtnConnect.Name = "BtnConnect"
		Me.BtnConnect.Size = New System.Drawing.Size(108, 23)
		Me.BtnConnect.TabIndex = 10
		Me.BtnConnect.Text = "Connect"
		Me.BtnConnect.UseVisualStyleBackColor = True
		'
		'BtnQuit
		'
		Me.BtnQuit.Location = New System.Drawing.Point(15, 211)
		Me.BtnQuit.Name = "BtnQuit"
		Me.BtnQuit.Size = New System.Drawing.Size(77, 23)
		Me.BtnQuit.TabIndex = 11
		Me.BtnQuit.Text = "Quit"
		Me.BtnQuit.UseVisualStyleBackColor = True
		'
		'BtnNext
		'
		Me.BtnNext.Location = New System.Drawing.Point(184, 211)
		Me.BtnNext.Name = "BtnNext"
		Me.BtnNext.Size = New System.Drawing.Size(77, 23)
		Me.BtnNext.TabIndex = 12
		Me.BtnNext.Text = "Next"
		Me.BtnNext.UseVisualStyleBackColor = True
		'
		'BtnBack
		'
		Me.BtnBack.Location = New System.Drawing.Point(101, 211)
		Me.BtnBack.Name = "BtnBack"
		Me.BtnBack.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.BtnBack.Size = New System.Drawing.Size(77, 23)
		Me.BtnBack.TabIndex = 13
		Me.BtnBack.Text = "Back"
		Me.BtnBack.UseVisualStyleBackColor = True
		'
		'TxtInstructions
		'
		Me.TxtInstructions.AutoSize = True
		Me.TxtInstructions.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TxtInstructions.Location = New System.Drawing.Point(29, 19)
		Me.TxtInstructions.Name = "TxtInstructions"
		Me.TxtInstructions.Size = New System.Drawing.Size(223, 13)
		Me.TxtInstructions.TabIndex = 41
		Me.TxtInstructions.Text = "Wear the Bird and press a long HOME"
		'
		'FrmThimbleConnect
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(284, 262)
		Me.Controls.Add(Me.TxtInstructions)
		Me.Controls.Add(Me.BtnBack)
		Me.Controls.Add(Me.BtnNext)
		Me.Controls.Add(Me.BtnQuit)
		Me.Controls.Add(Me.BtnConnect)
		Me.Controls.Add(Me.BtnDisconnect)
		Me.Controls.Add(Me.TxtThimbleId)
		Me.Controls.Add(Me.Label1)
		Me.Name = "FrmThimbleConnect"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Thimble connection"
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtThimbleId As System.Windows.Forms.TextBox
    Friend WithEvents BtnDisconnect As System.Windows.Forms.Button
    Friend WithEvents BtnConnect As System.Windows.Forms.Button
    Friend WithEvents BtnQuit As System.Windows.Forms.Button
    Friend WithEvents BtnNext As System.Windows.Forms.Button
    Friend WithEvents BtnBack As System.Windows.Forms.Button
    Friend WithEvents TxtInstructions As System.Windows.Forms.Label
End Class
