﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSensorConnect
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.CmbComPort = New System.Windows.Forms.ComboBox()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.BtnConnect = New System.Windows.Forms.Button()
		Me.BtnDisconnect = New System.Windows.Forms.Button()
		Me.BtnQuit = New System.Windows.Forms.Button()
		Me.BtnNext = New System.Windows.Forms.Button()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Btn_LoadFW = New System.Windows.Forms.Button()
		Me.TxtFWUProgress = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.SuspendLayout()
		'
		'CmbComPort
		'
		Me.CmbComPort.FormattingEnabled = True
		Me.CmbComPort.Location = New System.Drawing.Point(138, 59)
		Me.CmbComPort.Name = "CmbComPort"
		Me.CmbComPort.Size = New System.Drawing.Size(121, 21)
		Me.CmbComPort.TabIndex = 0
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(28, 59)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(77, 13)
		Me.Label1.TabIndex = 1
		Me.Label1.Text = "Base Unit port:"
		'
		'BtnConnect
		'
		Me.BtnConnect.Location = New System.Drawing.Point(15, 100)
		Me.BtnConnect.Name = "BtnConnect"
		Me.BtnConnect.Size = New System.Drawing.Size(108, 23)
		Me.BtnConnect.TabIndex = 2
		Me.BtnConnect.Text = "Connect"
		Me.BtnConnect.UseVisualStyleBackColor = True
		'
		'BtnDisconnect
		'
		Me.BtnDisconnect.Location = New System.Drawing.Point(151, 100)
		Me.BtnDisconnect.Name = "BtnDisconnect"
		Me.BtnDisconnect.Size = New System.Drawing.Size(108, 23)
		Me.BtnDisconnect.TabIndex = 3
		Me.BtnDisconnect.Text = "Disconnect"
		Me.BtnDisconnect.UseVisualStyleBackColor = True
		'
		'BtnQuit
		'
		Me.BtnQuit.Location = New System.Drawing.Point(24, 309)
		Me.BtnQuit.Name = "BtnQuit"
		Me.BtnQuit.Size = New System.Drawing.Size(108, 23)
		Me.BtnQuit.TabIndex = 6
		Me.BtnQuit.Text = "Quit"
		Me.BtnQuit.UseVisualStyleBackColor = True
		'
		'BtnNext
		'
		Me.BtnNext.Location = New System.Drawing.Point(160, 309)
		Me.BtnNext.Name = "BtnNext"
		Me.BtnNext.Size = New System.Drawing.Size(108, 23)
		Me.BtnNext.TabIndex = 7
		Me.BtnNext.Text = "Next"
		Me.BtnNext.UseVisualStyleBackColor = True
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.Location = New System.Drawing.Point(67, 20)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(150, 13)
		Me.Label3.TabIndex = 42
		Me.Label3.Text = "Connect to the Base Unit"
		'
		'Btn_LoadFW
		'
		Me.Btn_LoadFW.Location = New System.Drawing.Point(15, 161)
		Me.Btn_LoadFW.Name = "Btn_LoadFW"
		Me.Btn_LoadFW.Size = New System.Drawing.Size(108, 23)
		Me.Btn_LoadFW.TabIndex = 43
		Me.Btn_LoadFW.Text = "Load Firmware"
		Me.Btn_LoadFW.UseVisualStyleBackColor = True
		'
		'TxtFWUProgress
		'
		Me.TxtFWUProgress.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
		Me.TxtFWUProgress.Location = New System.Drawing.Point(157, 162)
		Me.TxtFWUProgress.Name = "TxtFWUProgress"
		Me.TxtFWUProgress.Size = New System.Drawing.Size(50, 20)
		Me.TxtFWUProgress.TabIndex = 44
		Me.TxtFWUProgress.Text = "-"
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(202, 166)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(15, 13)
		Me.Label2.TabIndex = 45
		Me.Label2.Text = "%"
		'
		'FrmSensorConnect
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(296, 354)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.TxtFWUProgress)
		Me.Controls.Add(Me.Btn_LoadFW)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.BtnNext)
		Me.Controls.Add(Me.BtnQuit)
		Me.Controls.Add(Me.BtnDisconnect)
		Me.Controls.Add(Me.BtnConnect)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.CmbComPort)
		Me.Name = "FrmSensorConnect"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Sensor Connection"
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
    Friend WithEvents CmbComPort As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BtnConnect As System.Windows.Forms.Button
    Friend WithEvents BtnDisconnect As System.Windows.Forms.Button
    Friend WithEvents BtnQuit As System.Windows.Forms.Button
    Friend WithEvents BtnNext As System.Windows.Forms.Button
	Friend WithEvents Label3 As System.Windows.Forms.Label
	Friend WithEvents Btn_LoadFW As System.Windows.Forms.Button
	Friend WithEvents TxtFWUProgress As System.Windows.Forms.Label
	Friend WithEvents Label2 As System.Windows.Forms.Label

End Class
