//////////////////////////////////////////////////////////////////////////////

	Bird SDK 7.2 - README.txt

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

	Last update: Jul-2017	

//////////////////////////////////////////////////////////////////////////////

1. SDK Directories
- Include: C/C++ API include files
- Bin: API libraries and DLLs (32/64 bit)
- Examples: Usage examples of the API
- Tools/Emulator: Bird emulator program

2. Bin Directory:
- contains Bird Core DLL MUVClientWin.dll (32/64 bit). Current version: 5.4.x

3. Include Directory:
- API Directory: contains all needed .h files to interact with the API. All
API functions are declared in C style in BirdAPI.h.

4. Examples Directory:
  - CS.NET: C# examples
    - BirdIMUData: C# console project is showing several examples of SDK usage
    - WPF 3D model: C#/WPF code for contorling 3D object via the Bird
  - Unity: Unity example projects
    - Drone: a Unity Drone-flying project using the Bird

5. API documentation: all API documentation is currently embedded in the .h
files, especially in BirdAPI.h.
