/******************************************************************************
	Module:	MUV CLient API
	File:	VersionInfo.h
	Desc:	Thimble/Sensor version structs
	Author:	#gr
	Creatd:	7-2015
	Remarks:

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

#ifndef __VERSIONINFO_H__
#define __VERSIONINFO_H__

/******************************************************************************
	DEFINES
*******************************************************************************/

#include <limits.h>

/******************************************************************************
	STRUCT DECL
*******************************************************************************/

struct stCoreVersion
{
	stCoreVersion(unsigned char _major = 0, unsigned char _minor = 0, unsigned char _build = 0) : major(_major), minor(_minor), build(_build) {}
	operator unsigned int() const {return (major << (2*CHAR_BIT)) | (minor << (1*CHAR_BIT)) | (build << (0*CHAR_BIT));}
	unsigned char major;
	unsigned char minor;
	unsigned char build;
};

struct stSenVersion
{
	stCoreVersion sw{};
	stSenVersion(stCoreVersion ver = {}) : sw(ver) {}
};

struct stThiVersion
{
	stCoreVersion sw{};
	stCoreVersion ld{};
	stCoreVersion hw{};
};

#endif // __VERSIONINFO_H__
