/******************************************************************************
	Module:	SDK
	File:	defines.h
	Desc:	defines for MUV Bird API
	Author:	#gr
	Creatd:	9-2014
	Changes:
	-----------------------------
	Author		Date	Desc
	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

#ifndef __DEFINES_H__
#define __DEFINES_H__

#ifdef WIN32
#ifdef DLLEXPORT
#define API_DECL	__declspec(dllexport) __stdcall	
#else
#define API_DECL	__stdcall	
#endif
#define CB_CALL		__stdcall	
#else
#define API_DECL	
#define CB_CALL		
#endif

#define API_EXTERN_C

#ifndef _out_ 
#define	_out_ 
#endif
#ifndef _in_
#define	_in_
#endif

#if defined ANDROID || defined IOS	// or other tables
#define TABLET
#endif

// Bird Server
#define API_SERVER_PORT		"7712"

#define API_SERVER_BTNS_ID		0x112
#define API_SERVER_ORIENT_ID	0x113
#define API_SERVER_OFN_ID		0x114

#endif //	__DEFINES_H__
