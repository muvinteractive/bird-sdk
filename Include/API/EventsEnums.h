#ifndef __EVENTS_ENUMS_H__
#define __EVENTS_ENUMS_H__

struct MUVEventType {
	enum Enum {
		GEST_LEFT,
		GEST_RIGHT,
		GEST_UP,
		GEST_DOWN,
		OFN_LEFT,
		OFN_RIGHT,
		OFN_UP,
		OFN_DOWN,
		OFN_UP_START,
		OFN_DOWN_START,
		HOME_BTN_CLICK,
		HOME_BTN_DBLCLICK,
		HOME_BTN_CLICK_NOT_DBL,
		OFN_TAP,
		OFN_DOUBLE_TAP,
		OFN_BTN_CHANGED_FINGER_PRES_ON,
		OFN_BTN_CHANGED_FINGER_PRES_OFF,
		OFN_BTN_CHANGED_OTHER,
		OFN_DND_ACTION_START,
		USWITCH_DOWN_START,
		USWITCH_UP_START,
		IMU,		// only no-mouse-move IMU events
		IMU_ANY,	// any IMU events
		GENERIC, //can support any action
		NUM_OF_TYPES,
		UNDEFINED = NUM_OF_TYPES
	};
};
struct AppEventDevice {
	enum Enum {
		KEYBOARD,
		MOUSE,
		TOUCH,
		GENERIC
	};
};

struct SupportedMouseEvent {
	enum Enum {
		WHEEL_UP,
		WHEEL_DOWN,
		WHEEL_LEFT,
		WHEEL_RIGHT,
		ZOOM_IN, //Ctrl+Wheel down
		ZOOM_OUT, //Ctrl+Wheel up
		RIGHT_BUTTON,
		NUM_OF_EVENTS,
		UNDEFINED = NUM_OF_EVENTS
	};
};

#endif