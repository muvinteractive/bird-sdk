/******************************************************************************
	Module:	MUV CLient API
	File:	btns.h
	Desc:	Thimble buttons params struct
	Author:	#gr
	Creatd:	11-2015
	Remarks:

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

#ifndef __BTNS_H__
#define __BTNS_H__

/******************************************************************************
	DEFINES
*******************************************************************************/

/******************************************************************************
	STRUCT DECL
*******************************************************************************/

namespace API {

	enum tBtnsStateFlags {
		btnf_idle = 0,
		btnf_finger_detected = 1 << 2,
		btnf_pinch_detected = 1 << 3,
		btnf_ofn_dome_pressed_detected = 1 << 4,
		btnf_left_mouse_down = 1 << 5,
		btnf_home_down = 1 << 6,
		btnf_tip_switch = 1 << 7,
	};
	/*
		Describes the the current state of the Thimble�s buttons.
		#follow CliPacket::cvals::btn_status
		*/
}

#endif // __BTNS_H__

