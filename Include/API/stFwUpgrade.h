/******************************************************************************
	Module:	MUV CLient API
	File:	stFwUpgrade.h
	Desc:	FW-Upgrade data structures and common definitions
	Author:	#bm
	Creatd:	06-2015
	Remarks:

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

#ifndef __ST_FW_UPGRADE_H__
#define __ST_FW_UPGRADE_H__

#include "defines.h"

/******************************************************************************
	DEFINES
*******************************************************************************/

#define	NO_PROGRESS	-1

#ifdef __cplusplus
#define ENUM_CLASS class
#define ENUM_SCOPE 
#else
#define ENUM_CLASS 
#define ENUM_SCOPE enum
#endif

/******************************************************************************
	STRUCT DECL
*******************************************************************************/

#ifdef __cplusplus
namespace API {
#endif
	
	enum ENUM_CLASS FwUMessageType {
		Progress, Problem, Error, Complete
	};

	typedef void(CB_CALL *tFwUpgradeCallback)(const char* szText,
		ENUM_SCOPE FwUMessageType t, int progress);

#ifdef __cplusplus
}
#endif

#endif // __ST_FW_UPGRADE_H__
