/******************************************************************************
	Module:	MUV CLient API
	File:	stThiInternParams.h
	Desc:	Thimble internal params struct
	Author:	#gr
	Creatd:	8-2015
	Remarks:

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

#ifndef __STTHIINTERNPARAMS_H__
#define __STTHIINTERNPARAMS_H__

/******************************************************************************
	DEFINES
*******************************************************************************/

/******************************************************************************
	STRUCT DECL
*******************************************************************************/

struct stThiInternParams {
	int				m_finPrThr{};
	int				m_finPrCurVal{};
	int				m_pinchThr{};
	int				m_pinchCurVal{};
};

enum class ThimbleChipType {
	FREESCALE = 0,
	MPR031 = 1,
	OFN = 2,
	INVENSENSE = 3
};

enum class CapacitanceType {
	FINGER_PINCH,
	FINGER_PINCH_THRESHOLD,
	FINGER_PRESENT_THRESHOLD,
	AIR_PINCH,
	AIR_PINCH_THRESHOLD //AIR_PINCH - constant_per_HW_version
};
#endif // __STTHIINTERNPARAMS_H__

