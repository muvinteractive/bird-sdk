/******************************************************************************
	Module:	MUV CLient API
	File:	stPktStat.h
	Desc:	Thimble packet statistics data struct
	Author:	#gr
	Creatd:	11-2014
	Remarks:

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

#ifndef __STPKTSTAT_H__
#define __STPKTSTAT_H__

/******************************************************************************
	DEFINES
*******************************************************************************/

/******************************************************************************
	STRUCT DECL
*******************************************************************************/

namespace API
{

	typedef struct {	// packet statistics

		// define indices to arrays below
		enum {
			inx_arriv_pkts_num,			// num of arrived pkts
			inx_lost_pkts_num,			// num of lost pkts
			inx_pkt_loss_perc_interval,	// pkt loss percentage (since start of last time interval (~ several seconds); for this THI)
			inx_pkt_loss_perc_total,	// pkt total loss percentage (since start of running/reset; for this THI)
			inx_pps,					//	packets per second
			inx_pkt_loss_perc_long_interval,	// pkt loss percentage (since start of last time interval (~ several minutes); for this THI)
			inx__num,
		};

		static const		int array_size = inx__num;

		int				m_allPkts[array_size];			// pkts from all types
		int				m_OFN_Pkts[array_size];			// pkts from OFN actions
		int				m_IMU_Pkts[array_size];			// pkts from IMU module
		int				m_buttonsPkts[array_size];		// pkts from buttons actions
		
		int				m_inPktSeqNum;	//	last packets's seq num
		bool			m_bRecalc;		//	re-calc was done

		void			InitCalc() {
			memset(m_allPkts, 0, sizeof(m_allPkts));
			memset(m_OFN_Pkts, 0, sizeof(m_OFN_Pkts));
			memset(m_IMU_Pkts, 0, sizeof(m_IMU_Pkts));
			memset(m_buttonsPkts, 0, sizeof(m_buttonsPkts));
			m_inPktSeqNum = -1;
		}
	} stPktStat;
}

#endif // __STPKTSTAT_H__

