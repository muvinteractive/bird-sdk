/******************************************************************************
	Module:	MUV CLient API
	File:	OFN.h
	Desc:	Thimble OFN declerations
	Author:	#gr
	Creatd:	11-2015
	Remarks:

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

#ifndef __OFN_H__
#define __OFN_H__

/******************************************************************************
	DEFINES
*******************************************************************************/

/******************************************************************************
	STRUCT DECL
*******************************************************************************/

namespace API {

	enum tOFNFlags {
		fing_present_bit = 1 << 7,
		tap_hold_bit = 1 << 6,
		dblclk_bit = 1 << 5,
		click_bit = 1 << 4,
	};

	struct tOFNData {
		int x;
		int y;
		tOFNFlags flags;
	};
}

#endif // __OFN_H__

