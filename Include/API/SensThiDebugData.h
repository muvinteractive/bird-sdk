/******************************************************************************
Module:	MUV CLient API
File:	SensThiDebugData.h
Desc:	Thimble and Sensor debug data struct and callback prototype
Author:	#hk
Creatd:	01-2016
Remarks:

-----------------------------
Copyright (C) MUV Interactive
All rights reserved.

*******************************************************************************/

#ifndef __SENS_THI_DEBUG_DATA_H__
#define __SENS_THI_DEBUG_DATA_H__

/******************************************************************************
DEFINES
*******************************************************************************/

/******************************************************************************
STRUCT DECL
*******************************************************************************/

namespace API
{
	enum TypeOfChangedDebugData { SENSOR_RECIVER_ERRORS, SENSOR_SOFT_RESETS, THI_RECIVER_ERRORS, THI_SOFT_RESETS };

	struct ReciverErrorsStat
	{
		unsigned int m_invalid_crc_or_status{};
		unsigned int m_wrong_thimble_address{};
		unsigned int m_link_quality{};
		void Set(unsigned int invalid_crc_or_status, unsigned int wrong_thimble_address, unsigned int link_quality)
		{
			m_invalid_crc_or_status = invalid_crc_or_status; 
			m_wrong_thimble_address = wrong_thimble_address;
			m_link_quality = link_quality;
		}
	};

	struct SensThiDebugData {
		TypeOfChangedDebugData typeOfChangedData{};
		ReciverErrorsStat recivErrs{};
		unsigned int softResets{};

		SensThiDebugData(){}
		SensThiDebugData(unsigned int resets, TypeOfChangedDebugData t)
		{
			typeOfChangedData = t;
			softResets = resets;
		}
		SensThiDebugData (unsigned int invalid_crc_or_status, unsigned int wrong_thimble_address, unsigned int link_quality, TypeOfChangedDebugData t)
		{
			typeOfChangedData = t;
			recivErrs.Set (invalid_crc_or_status, wrong_thimble_address, link_quality);
		}
	};
	typedef void(*OnSensThiDebugDataCB)(SensThiDebugData);
}

#endif