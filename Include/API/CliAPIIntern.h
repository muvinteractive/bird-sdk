/******************************************************************************
	Module:	SDK
	File:	CliAPIIntern.h
	Desc:	MUV Client Internal API, to be used inside MUV only
	Author:	#gr
	Creatd:	11-2015
	Changes:
	-----------------------------
	Author		Date	Desc
	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

#ifndef __CLIAPIINTERN_H__
#define __CLIAPIINTERN_H__

/******************************************************************************
	INCLUDES
*******************************************************************************/

#include "../Project.h"
#include "../CMUVClient.h"
#include "BirdAPI.h"

/******************************************************************************
	MACROS
*******************************************************************************/


/******************************************************************************
	API DECL
*******************************************************************************/


namespace API {
	namespace Intern {
		CMUVClient *Cli_GetMUVClientObject();
		void		Cli_ControlDrone(bool bOn);
		void		Cli_ControlLights(bool bOn);
		void		Cli_ControlDroneEmerg();
		void		Cli_ControlDroneLand();
	}
}

#endif	//	__CLIAPIINTERN_H__



