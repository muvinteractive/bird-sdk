/******************************************************************************
	Module: MUV CLient API
	File:   stLightSrcInfo.h
	Desc:   sensor-detected light sources data struct
	Author: #bm
	Creatd: 11-2015
	Remarks:

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

#ifndef __STLIGHTSRCINFO_H__
#define __STLIGHTSRCINFO_H__

/******************************************************************************
	DEFINES
*******************************************************************************/

#define STLIGHTSRCINFO_MAX_STR_LEN   32
#define STLIGHTSRCINFO_MAX_POINT_NUM 16
#define STLIGHTSRCINFO_MAX_WH_VALUE  98
#define STLIGHTSRCINFO_MAX_XY_VALUE  2940

#define STLIGHTSRCINFO_VAL_NOT_AVAIL  -1

/******************************************************************************
	STRUCT DECL
*******************************************************************************/

namespace API
{
	typedef struct
	{
		int scx; // geometric x coordinate of spot center
		int scy; // geometric y coordinate of spot center
	}
	stLightSrc;

	typedef struct
	{
		char         m_sensId[STLIGHTSRCINFO_MAX_STR_LEN];             // Unique identifier of sensor
		unsigned int m_thiId;                                          // id of source thimble
		int          m_lightSrcCount;                                  // sensor number of srcs
		stLightSrc   m_lightSrc[STLIGHTSRCINFO_MAX_POINT_NUM];         // sensor coordinates srcs
		int          m_lightSrcOffs_Pts[STLIGHTSRCINFO_MAX_POINT_NUM]; // number of srcs off events per point
		int			 m_arrived;											// number of packets arrived
	}
	stLightSrcInfo;

	typedef struct
	{
		int area  ; // size of spot in pixels
		int width ; // width  of bounding rectangle in pixels
		int height; // height of bounding rectangle in pixels
		int avgBrt; // average brightness
		int maxBrt; // maximum brightness
		int scx   ; // geometric x coordinate of spot center
		int scy   ; // geometric y coordinate of spot center
		int tlx   ; // geometric x coordinate of top    left  corner
		int tly   ; // geometric y coordinate of top    left  corner
		int brx   ; // geometric x coordinate of bottom right corner
		int bry   ; // geometric y coordinate of bottom right corner
	}
	stLightSrcEx;

	typedef struct
	{
		char         m_sensId[STLIGHTSRCINFO_MAX_STR_LEN];     // Unique identifier of sensor
		unsigned int m_thiId;                                  // id of source thimble
		int          m_lightSrcCount;                          // sensor number of srcs
		stLightSrcEx m_lightSrc[STLIGHTSRCINFO_MAX_POINT_NUM]; // sensor coordinates srcs
	}
	stLightSrcInfoEx;

	typedef enum
	{
		stLightModeNone,
		stLightModeOper,
		stLightModeFull,
		stLightModeArea,
	}
	stLightMode;

	/*
		Optical Calibration Support
	*/
	typedef enum
	{
		SENSOR_CALIBRATION_TOGGLE_ON,
		SENSOR_CALIBRATION_TOGGLE_OFF,
	}
	SensorCalibrationToggle;

	typedef enum
	{
		SENSOR_CALIBRATION_STATUS_PHASE_1_STARTED,
		SENSOR_CALIBRATION_STATUS_PHASE_2_STARTED,
		SENSOR_CALIBRATION_STATUS_COMPLETE_SUCCESS,
		SENSOR_CALIBRATION_STATUS_COMPLETE_FAILURE,
	}
	SensorCalibrationStatus;

	typedef enum
	{
		SENSOR_CALIBRATION_REPORT_COMPLETE_SUCCESS,
		SENSOR_CALIBRATION_REPORT_COMPLETE_FAILURE,
		SENSOR_CALIBRATION_REPORT_ABORT_ON_TIMEOUT,
		SENSOR_CALIBRATION_REPORT_ABORT_ON_REQUEST,
	}
	SensorCalibrationReport;

	typedef enum
	{
		SENSOR_CALIBRATION_ACTION_POINT_TO_SCREEN,
		SENSOR_CALIBRATION_ACTION_SCAN_THE_SCREEN,
	}
	SensorCalibrationAction;

	typedef void (*SensorCalibrationToggleFunc)(SensorCalibrationToggle toggle);
	typedef void (*SensorCalibrationStatusFunc)(SensorCalibrationStatus status);
	typedef void (*SensorCalibrationReportFunc)(SensorCalibrationReport report);
	typedef void (*SensorCalibrationRedrawFunc)(const stLightSrcInfoEx& lightSrcInfoEx);
	typedef void (*SensorCalibrationActionFunc)(SensorCalibrationAction action,int numOfMilliSecondsLeft);
	typedef void (*SensorCalibrationRecordFunc)(int gain,int threshold,int falsePositives,int falseNegatives,int rectangleGrade,double grade,bool done,double completed);

	/*
		Geometrical Calibration Support
	*/

	typedef enum {
		idle,
		init,
		point_1,
		point_1_ended,
		point_2,
		point_2_ended,
		point_3,
		point_3_ended,
		point_4,
		point_4_ended,
		done,
		aborted,
	} eSensorGeomCalibStage;

	typedef void(*SensorGeomCalibStageCB)(eSensorGeomCalibStage stageId);

};

#endif // __STLIGHTSRCINFO_H__
