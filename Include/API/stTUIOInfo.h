/******************************************************************************
	Module:	MUV CLient API
	File:	stTUIOInfo.h
	Desc:	TUIO data struct
	Author:	#gr
	Creatd:	12-2014
	Remarks:

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

#ifndef __STTUIOINFO_H__
#define __STTUIOINFO_H__

/******************************************************************************
	DEFINES
*******************************************************************************/

/******************************************************************************
	STRUCT DECL
*******************************************************************************/

struct stTUIOInfo {	
	int			m_sentPktCount{};	//	count of sent packets
};

using TUIOInfoCB = void(*)(stTUIOInfo&);

#endif // __STTUIOINFO_H__

