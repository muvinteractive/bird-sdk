/******************************************************************************
	Module:	MUV CLient API
	File:	stOrient.h
	Desc:	Thimble orientation params struct
	Author:	#gr
	Creatd:	11-2015
	Remarks:

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

#ifndef __STORIENT_H__
#define __STORIENT_H__

/******************************************************************************
	DEFINES
*******************************************************************************/

/******************************************************************************
	STRUCT DECL
*******************************************************************************/

namespace API {

	/*
		tOrientPRY  - Describes the orientation of the BIRD, in pitch, roll 
		and yaw coordinates.

		Members
		pitchAngle [out] � 	the BIRD�s pitch angle relative to horizon; range: -90 deg (hand fully pointing down) to + 90 deg (hand fully pointing up)
		rollAngle [out] � 	the BIRD�s roll angle relative to horizon; range: -90 deg (hand fully tilted left) to + 90 deg (hand fully tilted right)
		yawAngle [out] � 	the BIRD�s yaw angle relative to fixed starting point; range: 0-360 deg
	*/
	struct tOrientPRY {
		float	pitchAngle;
		float	rollAngle;
		float	yawAngle;
	};

	/*
		tOrientQ  - Describes the orientation of the BIRD as a quaternion.

		Members
			w x y z  [out] � 	the BIRD�s quaternion axes
			
			For the axis (x, y, z) and angle of rotation theta, the w value for the quaternion is:
			w = cos(theta/2)
	*/
	struct tOrientQ {
		float	w;
		float	x;
		float	y;
		float	z;
	};

}

#endif // __STORIENT_H__

