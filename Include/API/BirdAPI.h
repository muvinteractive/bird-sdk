/******************************************************************************
	Module:	SDK
	File:	BirdAPI.h
	Desc:	MUV Bird API 
	Author:	#gr
	Creatd:	9-2014
	Changes:
	-----------------------------
	Author		Date	Desc
	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

#ifndef __BIRDAPI_H__
#define __BIRDAPI_H__

/******************************************************************************
	MACROS
*******************************************************************************/


/******************************************************************************
	INCLUDES
*******************************************************************************/

#include "defines.h"
#include <vector>
#include <string>
#include "stSensorConfig.h"
#include "stThimbleConfig.h"
#include "stPktStat.h"
#include "stLightSrcInfo.h"
#include "stTUIOInfo.h"
#include "stFwUpgrade.h"
#include "stThiInternParams.h"
#include "stOrient.h"
#include "VersionInfo.h"
#include "EventsEnums.h"
#include "btns.h"
#include "OFN.h"
#include "CUIInfoExchange.h"


/******************************************************************************
	API Return codes
*******************************************************************************/

#define	API_PROGRESS		(2)
#define	API_OK				(0)
#define	API_ERR				(-1)
#define	API_CANCELED		(-2)
#define	API_INVALID			(-3)
#define	API_MISMATCHEDVER	(-4)
#define	API_BOOTLOADER		(-5)
#define	API_NAK				(-6)

/******************************************************************************
	API DECL
*******************************************************************************/


namespace API {
	enum PairingFlags{ ALREADY_PAIRED = 0x1, ENFORCED_PARING = 0x2 };
	using pCBFunc_t = void(CB_CALL *)(void *pParam);
	
	/*
		ErrCB: error message button events callback
		params:
	*/
	using ErrCB = void(CB_CALL *)(const char *pMsg);

	using tOnThiPktCB = void(CB_CALL *)(const unsigned int thiId);
	using tOnThiPairCB = void(CB_CALL *)(const unsigned int thiId, int pairingFlags);
	using tOnSensPktCB = void(CB_CALL *)(const char *sensId);
	using tSensOptUserMsgCB = void(CB_CALL *)(const char *pMsg);
	using tOnGestureCB = void(CB_CALL *)(const char *thiId, const GestureId gest);
	using tOnOrientPRYCB = void(CB_CALL *)(const char *thiId, _in_ const API::tOrientPRY& orient);
	using tOnOrientQCB = void(CB_CALL *)(const char *thiId, _in_ const API::tOrientQ& orient);
	using LightSrcInfoCB = void(CB_CALL *)(const stLightSrcInfo& lightSrcInfo);
	using LightSrcInfoExCB = void(CB_CALL *)(const stLightSrcInfoEx& lightSrcInfoEx);
	using pktStatCB = void(CB_CALL *)(const char *thiId, const stPktStat&);
	using tOnSwitchActiveThimbleCB = void(CB_CALL *)(const char* thiId);
	using tOnUIInfoCB = void(CB_CALL *)(API::CUIInfoExchange& uiInfo);
	using tActivityCB = void(CB_CALL *)(const int state, const int progress);
	using tSetChannelCB = void(CB_CALL *)(const char* actName, int intParam, const char* strParam);


	/*
		tOnBtnsCB: button events callback
		params:
		btns - bitmap of the relvant buttons:
	*/ 
	using tOnBtnsCB = void(CB_CALL *)(const char* thiId, const tBtnsStateFlags btns);
	using tOnBtnsExtCB = void(CB_CALL *)(const char* thiId, const tBtnsStateFlags btns, uint pinchCapacity, uint fingerCapacity);
	using tOnThiAddedCB = void(CB_CALL *)(const unsigned int thiId, const unsigned int pairedThiNum);
	using tOnOFNCB = void(CB_CALL *)(const char *thiId, const API::tOFNData OFNData);
	using tOnOFNSwipeCB = bool(CB_CALL *)(const char *thiId, bool bRight_not_left);
	using tOnOFNClicksCB = bool(CB_CALL *)(const char *thiId, bool bClick, bool bDblClick);
	using tOnOFNScrollCB = bool(CB_CALL *)(const char *thiId, bool bUp_not_down);

	// mode - special applcation modes
	struct ApplMode {
		STATCONS uint invalid = -1;
		STATCONS uint none = 0;
		STATCONS uint presenter = 0x1;
		STATCONS uint player = 0x2;

		uint 		ids;
		bool		bZoom;
	};

#ifdef API_EXTERN_C
	extern "C" {
#endif

		/******************************************************************************
			API Module:	Cli_*
			Desc:		Client (system) related actions and data
		*******************************************************************************/

		/*
			Cli_Init: initialize the Client core
			out: 
			version - software version (Major, minor, build)
			returns: 
			0 - OK; -1 - an error has occured
		*/
		int API_DECL Cli_Init(stCoreVersion &version);

#ifdef TABLET
		/*
			Cli_InitEx: initialize the Client core, embedded / Android
			in:
			scrDim_x - screen width
			scrDim_y - screen height
			scrRot90 - screen rotation position
			env	- internal environment implementation

			out:
			version - software version (Major, minor, build)

			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_InitEx(stCoreVersion &version, const int scrDim_x, const int scrDim_y, const int scrRot90, const void *env);

		/*
			Cli_GetEnvContextPerThread: obtain env context for tables env (e.g. Android)
			- needed to perform calls to non-native env (JVM)
			- must be called from the thread about to perform the calls to non-native env (JVM)

			out:
			env - env context for calling thread

			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_GetEnvContextPerThread(void **env);

#endif // TABLET

		/*
			Cli_Shutdown: shutdown the Client core
			- save repository

			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_Shutdown();
		
		/*
			Cli_ExitAllInstances: Exit all instances of Bird app on 
			this host gracefully.

			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_ExitAllInstances();

		/*
			Cli_SaveRepository: save Client repository
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_SaveRepository();
		
		/*
			Cli_LoadRepository: load Client repository
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_LoadRepository();

		/*
			Cli_GetVersion: Get Client version
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_GetVersion(stCoreVersion &version);

#ifdef WIN32
		
		/*
			Cli_TUIOSetParameters: set TUIO parameters
			in:
			ip - TUIO server IP address
			port - TUIO server port number

			returns:
			0 - OK; -1 - an error has occured

		*/
		int	API_DECL Cli_TUIOSetParameters(const char *ip, const char *port);
		
		/*
			Cli_TUIOGetParameters: get TUIO parameters
			out:
			ip - TUIO server IP address buffer
			ip_buff_size - buffer size
			port - TUIO server port number buffer
			port_buff_size - buffer size
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_TUIOGetParameters(char *ip, int ip_buff_size, char *port, int port_buff_size);
		
		/*
			Cli_TUIOSession: start/stop a TUIO session
			in:
			bStart - start/stop value
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_TUIOSession(const bool bStart);
		
		/*
			Cli_SetTUIOInfoCB: set callback for TUIO information
			in:
			_TUIOInfoCB - callback function address
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_SetTUIOInfoCB(TUIOInfoCB _TUIOInfoCB);
#endif
		
		/*
			Cli_SetErrCB: set error handler
			in:
			_errMsg - error handler function address
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_SetErrCB(ErrCB _errMsg);
		int	API_DECL Cli_demo_RUN_FLASH(const char *filepath); 
		
		/*
			Cli_EnableCalibWarp: enable/disable geometrical calibration
			in:
			bEn - enable/disable value
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_EnableCalibWarp(bool bEn);
		
		/*
			Cli_IsCalibWarpEnabled: is geometrical calibration enabled
			returns:
			enable/disable value
		*/
		bool API_DECL Cli_IsCalibWarpEnabled();

		/*
			Cli_DiscoverBirdSensors: discover and report available sensors
			in:
			buffLenNames - length of names buffer
			buffLenAddrs - length of addresses buffer
			pSensNamesPrefix - prefix of all sensor names
			out:
			pSensNamesBuff - list available sensors. format is space separated "sens1 sens2 ...sensN"
			pSensAddrsBuff - addresses of available sensors. format is space separated "addr1 addr2 ...addrN"
			returns:
			0 - OK; -1 - an error has occured
		*/
		int API_DECL Cli_DiscoverBirdSensors(char *pSensNamesBuff, const unsigned int buffLenNames, char *pSensAddrsBuff, const unsigned int buffLenAddrs, const char *pSensNamesPrefix);

		/*
			Cli_DiscoverBirdUsbSensor
			return the first USB sensors (dongle) that is found and responsive.
			in:
				const unsigned int buffLen - the length of the given buffer, to make sure we don't have a memory leak
			out:
				char *pSensNamesBuff - the buffer to be filled with the comm port label
			return:
				0 = error
				1 = OK
		*/
		int API_DECL Cli_DiscoverBirdUsbSensor(char *pSensNamesBuff, const unsigned int buffLen);

		/*
		Cli_DiscoverBirdUsbSensorsList
			return a list of all USB sensors (dongle) that were found and responsive.
		in:
			none
		out:
			vector of strings to be filed with the comm port labels
		return:
			none
		*/

		void API_DECL Cli_DiscoverBirdUsbSensorsList(vector<string> &discoverdSensors);
		/*
			Cli_CancelBluetoothAction: cancel discovery of available sensors etc.
			in:
			bBlock - wait for last action to end
			returns:
			0 - OK
		*/
		int	API_DECL Cli_CancelBluetoothAction(bool bBlock=false);

		/*
			Cli_InitBluetoothAction: init bluetooth activities. Must
			be called before any cancellable action
			returns:
			0 - OK
		*/
		int	API_DECL Cli_InitBluetoothAction();

		/*
			Cli_HostHasBluetoothRadio: does this host even have a BT radio (auxilliary)?
			out:
			bHasBluetoothRadio - does this host even have a BT radio 
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_HostHasBluetoothRadio(bool &bHasBluetoothRadio);

		/*
			Cli_HostPairBluetoothDevice: pair BT device to this device
			pDevName - BT device name
			pDevAddr - BT device address
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_HostPairBluetoothDevice(const char *pDevName, const char *pDevAddr);
		/*
			Cli_HostUnpairAllBluetoothDevices: Unpairs all sensor bluetooth devices
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_HostUnpairAllBluetoothDevices();
			
		/*************************************************************************
		Support of customizable mapping between burd events and application events
		**************************************************************************/
		/*
		Cli_AddCustomApp: adds custom application with full path and description
		in:
		fullPath	- full path to exe file, including file name
		displayName - user friendly application name
		description - application description 
		returns:
		0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_AddCustomApp(const char* fullPath, const char* displayName, const char* description);
		/*
		Cli_MapMuvToAppEvent: maps between a bird event and specific application action defined by keyboard or mouse event[s]
		in:
		muvEvent	 - bird event described by MUVEventType enum
		appName		 - application executable name
		appEventDev	 - type of device which event will be simulated (mouse or keyboard)
		instructions - sequence of actions (keys or mouse events)
		instructionsSize - instructions number
		returns:
		0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_MapMuvToAppEvent(MUVEventType::Enum muvEvent, const char* appName, AppEventDevice::Enum appEventDev, int* instructions, int instructionsSize);

		/*
		Cli_GetMapedMuvToAppEvent: maps between a bird event and specific application action defined by keyboard or mouse event[s]
		in:
		muvEvent	 - bird event described by MUVEventType enum
		appName		 - application executable name
		appEventDev	 - type of device which event will be simulated (mouse or keyboard)
		out:
		instructions - sequence of actions (keys or mouse events)
		instructionsSize - instructions number
		returns:
		instructions number or -1 if no events defined for appName application
		*/
		int	API_DECL Cli_GetMapedMuvToAppEvent(MUVEventType::Enum muvEvent, const char* appName, AppEventDevice::Enum appEventDev, int* instructions, int instructionsSize);

		int API_DECL Cli_GetMappedAppList(char delimeter, char* appList);
		int API_DECL Cli_GetMappedAppProperties(const char* appName, char* path, char* descr);
		/*
		Cli_DeleteMuvToAppEventMapping: Delete mapping between specified bird event and application 
		in:
		muvEvent	- bird event described by MUVEventType enum
		appName		- application executable name
		*/
		void API_DECL Cli_DeleteMuvToAppEventMapping(MUVEventType::Enum muvEvent, const char* appName);

		/*
		Cli_GetSupportedKeysList: Returns list of supported virtual keys values
		out:
		keys	- array of keys values
		returns:
			keys count in array
		*/
		int API_DECL Cli_GetSupportedKeysList(int* keys);
		/*
		Cli_GetKeyDescriptor: Returns virtual keys description
		out:
		key			- virutal key value
		tag			- virtual key mnemonic name
		displayName - key name for display
		isExtended  - flag says is the key is a part of standard keyboard
		isControl   - flag says is the key is one of SHIFT, CONTROL or ALT keys that has to be followed by non-control key
		returns:
		keys count in array
		*/
		int API_DECL Cli_GetKeyDescriptor(int key, char* tag, char* displayName, int* isExtended, int* isControl);
		/*
		Cli_GetKeyDescriptor: Returns virtual keys description
		out:
		eventId		- virutal key value
		tag			- event mnemonic name
		displayName - event name for display
		returns:
		keys count in array
		*/
		int API_DECL Cli_GetMouseEventDescriptor(int eventId, char* tag, char* displayName);
		/*
		Cli_AdjustLightSrcExPoints: Adjusts light-source coordinates to virtual screen
		int:
		numOfPoints		- number of points
		inputPoints		- input points
		out:
		outputPoints	- output points
		returns:
		0 - OK; -1 - an error has occured
		*/
		int API_DECL Cli_AdjustLightSrcExPoints(unsigned int numOfPoints, const stLightSrcEx inputPoints[STLIGHTSRCINFO_MAX_POINT_NUM], stLightSrcEx outputPoints[STLIGHTSRCINFO_MAX_POINT_NUM]);

		/******************************************************************************
			API Module:	Sens_*
			Desc:		Sensor related actions and data
		*******************************************************************************/

		/*
			Sens_Generate: create a sensor object
			in:
			sensId - an ID chosen for this sensor
			comId -  id of communication channel (OS-dependent value)
			returns:
			0 - OK; -1 - an error has occured
		*/

		int	API_DECL Sens_Generate(const char *sensId, const char *comId);
		
		/*
			Sens_Connect: connect to a sensor 
			in:
			sensId - ID of sensor
			returns:
			API_OK - OK
			API_ERR - an error has occured
			API_CANCELED - operation was canceled by user
		*/
		int	API_DECL Sens_Connect(const char *sensId);
		
		/*
			Sens_Connect: Cancel connection operation to a sensor
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/

		int	API_DECL Sens_CancelConnection(const char *sensId);

		/*
			Sens_Disconnect: disconnect from a sensor
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_Disconnect(const char *sensId);
		
		/*
			Sens_Remove: remove a sensor object
			in:
			sensId - ID of sensor; empty string means "all sensors"
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_Remove(const char *sensId);

		/*
			Sens_GetConnStatus: get sensor connection status
			in:
			sensId - ID of sensor
			out:
			bConnected - is sensor connected
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_GetConnStatus(const char *sensId, bool &bConnected);
		
		/*
			Sens_GetAlias: get sensor configuration
			in:
			sensId - ID of sensor
			out:
			alias - sensor alias buffer
			alias_buff_size - buffer size
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_GetAlias(const char *sensId, char *alias, int alias_buff_size);
		
		/*
			Sens_SetAlias: set sensor alias 
			in:
			sensId - ID of sensor
			alias - sensor alias
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_SetAlias(const char *sensId, const char *alias);
		
		/*
			Sens_GetActive: get sensor activation status
			in:
			sensId - ID of sensor
			out:
			bActive - sensor activation status
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_GetActive(const char *sensId, bool &bActive);
				
		/*
			Sens_GetMaster: get sensor master status
			in:
			sensId - ID of sensor
			out:
			bMaster - sensor master status
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_GetMaster(const char *sensId, bool &bMaster);
		
		/*
			Sens_SetMaster: set sensor master status
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_SetMaster(const char *sensId);
		
		/*
			Sens_GetCalibProf: get sensor calibration profile id
			in:
			sensId - ID of sensor
			out:
			prof - sensor calibration profile id
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_GetCalibProf(const char *sensId, int &prof);
		
		/*
			Sens_SetCalibProf: set sensor calibration profile id
			in:
			sensId - ID of sensor
			prof - sensor calibration profile id
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_SetCalibProf(const char *sensId, const int prof);
		
		/*
			Sens_IsCurCalibValid: is sensor's calibration valid?
			in:
			sensId - ID of sensor
			returns: true/false of calibration validity
		*/
		bool	API_DECL Sens_IsCurCalibValid(const char *sensId);
		
		/*
			Sens_GetDisplayExtension: get sensor's Display Extension id
			in:
			sensId - ID of sensor
			out:
			displayExtension - Display Extension id
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_GetDisplayExtension(const char *sensId, int &displayExtension);
		
		/*
			Sens_SetDisplayExtension: set sensor's Display Extension id
			in:
			sensId - ID of sensor
			displayExtension - Display Extension id
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_SetDisplayExtension(const char *sensId, const int &displayExtension);
		
		/*
			Sens_GeomCalibrationStart: start geometrical calibration process (on currentlly-set profile)
			in:
			sensId - ID of sensor
			bRemote - remote calibratin on/off
			bPartial - partial screen calibration
			CBFunc - callback to be called on calibration's stage changes; may be nullptr
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_GeomCalibrationStart(const char *sensId, bool bRemote, bool bPartial, SensorGeomCalibStageCB CBFunc);
		
		/*
			Sens_UpdateFirmware: start sensor FW upgrade
			in:
			sensId - ID of sensor
			fileName - file to update
			FwUpgradeCallback - callback	
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_UpdateFirmware(const char *sensId, const char *fileName, tFwUpgradeCallback callback);
		
		/*
			Sens_RequestVersion: Request sensor's version
			in:
			sensId - Sensor ID
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_RequestVersion(const char *sensId);
		
		/*
			Sens_RefreshVersion: Refresh sensor's version
			in:
			sensId - Sensor ID
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_RefreshVersion(const char *sensId);
		
		/*
			Sens_RetrieveVersion: Retrieve sensor's version
			in:
			sensId - Sensor ID
			out:
			version - Sensor's version
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_RetrieveVersion(const char *sensId, stSenVersion &version);
		
		/*
			Sens_MirrorX: mirror sensor X axis
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_MirrorX(const char *sensId);
		
		/*
			Sens_MirrorY: mirror sensor Y axis
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_MirrorY(const char *sensId);
		
		/*
			Sens_Rotate90: rotate sensor axes 90 deg
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_Rotate90(const char *sensId);
		
		/*
			Sens_IsMirrorX: is sensor X axis mirrored 
			in:
			sensId - ID of sensor
			returns: true/false
		*/
		bool	API_DECL Sens_IsMirrorX(const char *sensId);
		
		/*
			Sens_IsMirrorY: is sensor Y axis mirrored
			in:
			sensId - ID of sensor
			returns: true/false
		*/
		bool	API_DECL Sens_IsMirrorY(const char *sensId);
		
		/*
			Sens_GetRotation90: Get sensor's axes rotation
			in:
			sensId - ID of sensor
			returns: 0: 0 deg; 1: 90 deg;	2: 180 deg; 3: 277 deg; -1: error
		*/
		int	API_DECL Sens_GetRotation90(const char *sensId); 
		

		/*
			Sens_GetPairedThimbles: retrieves the list of currently paired thimbles
			in:
			sensId - ID of sensor
			out:
			thimbles - the list of currently paired thimbles
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_GetPairedThimbles(const char *sensId, std::vector<std::string>& thimbles);
		/*
			Sens_GetPairedThimblesC: retrieves the list of currently paired thimbles (C stile)
			in:
			sensId - ID of sensor
			length - length of allocated bytes for thimbles buffer
			out:
			thimbles - buffer allocated by coller that will contain the list of currently paired thimble Ids separated by space character
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_GetPairedThimblesC(const char *sensId, int length, char* thimbles);
			/*
			Sens_GetActiveThimbles: retrieves the list of currently paired thimbles with finger present flag is set to true
			in:
			sensId - ID of sensor
			out:
			thimbles - the list of active thimbles
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_GetActiveThimbles(const char *sensId, std::vector<std::string>& thimbles);
		/*
			Sens_IsPairedThimble: checks whether the thimble is in paired thimbles list
			in:
			sensId - ID of sensor
			thiId  - ID of thimble for check
			out:
			returns:
			thure - thimble is paired; false - thimble is not paired
		*/
			bool API_DECL Sens_IsPairedThimble(const char *sensId, const char* thiId);
		/*
			Sens_GetActiveThimbleId: retrieves the multiactive thimble ID
			in:
			sensId - ID of sensor
			out:
			activeThiId - ID of active thimble or empty string
			isMaster - returns flag is the specified thimble is master
			isLocked - if thimble is master returns the flag is the switch activity action locked now
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_GetActiveThimbleId(const char *sensId, char* activeThiId, bool* isMaster, bool* isLocked);
		/*
			Sens_SwitchMaltiactivityToThimble: switches multiactive state from currently active to specified thimble ID
			in:
			targetThi - ID of the thimble that requested to be multiactive
			out:
			activeThiId - ID of the thimble that was active before switch or empty string
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_SwitchMaltiactivityToThimble(const char* targetThi, char* prevActiveThiId);		
		/*
			Sens_GetPairedThimbleId: Get thimble id with given index
			in:
			sensId - ID of sensor
			index - index of thimble
			out:
			thiId - thimble id buffer
			thiId_buff_len - thimble id buffer size
			returns: 
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_GetPairedThimbleId(const char *sensId, unsigned int index, char *thiId, unsigned int thiId_buff_len);
		
		/*
			Sens_PairThimble: pair thimble with given id 
			in:
			sensId - ID of sensor
			out:
			thiId - thimble id buffer
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_PairThimble(const char *sensId, const char *thiId);

		/*
			Sens_RegisterOnStatusCB: register a callback to recieve sensor status events
			in:
			sensId - ID of sensor
			CBFunc - callback to events
			returns:
			0 - OK; -1 - an error has occured
		*/
		int API_DECL Sens_RegisterOnStatusCB(const char *sensId, tOnSensPktCB CB);

		/*
			Sens_RegisterOnThiPairingPktCB: register a callback to recieve thimbles'
			pairing requests
			in:
			sensId - ID of sensor
			CBFunc - callback to recieve thimbles' pairing requests
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_RegisterOnThiPairingPktCB(const char *sensId, tOnThiPairCB CBFunc);
		
		/*
			Sens_IncSensorGain: increase sensor' gain value by a predefined constant
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_IncSensorGain(const char *sensId);
		
		/*
			Sens_DecSensorGain: decrease sensor' gain value by a predefined constant
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_DecSensorGain(const char *sensId);
		
		/*
			Sens_IncSensorThreshold: increase sensor's threshold value by a predefined constant
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_IncSensorThreshold(const char *sensId);
		
		/*
			Sens_DecSensorThreshold: decrease sensor's threshold value by a predefined constant
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_DecSensorThreshold(const char *sensId);
		
		/*
			Sens_IncSensorMinSpotSize: increase sensor's min-spot-size value by a predefined constant
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_IncSensorMinSpotSize(const char *sensId);
		
		/*
			Sens_DecSensorMinSpotSize: decrease sensor's min-spot-size value by a predefined constant
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_DecSensorMinSpotSize(const char *sensId);
		
		/*
			Sens_IncSensorMaxSpotSize: increase sensor's max-spot-size value by a predefined constant
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_IncSensorMaxSpotSize(const char *sensId);
		
		/*
			Sens_DecSensorMaxSpotSize: decrease sensor's max-spot-size value by a predefined constant
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_DecSensorMaxSpotSize(const char *sensId);
		
		/*
			Sens_SetCmosParams: set sensor's parameters
			in:
			gain          - magnitude of image amplification (see table below)
			threshold     - light spot below this threshold is filtered out (not considered by the sensor as a light source)
			min_spot_size - light spot below this size is filtered out (not considered by the sensor as a light source)
			max_spot_size - light spot above this size is filtered out (not considered by the sensor as a light source)
			Gain conversion table:
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			| Gain Value | Amplification Magnitude | Gain Value | Amplification Magnitude | Gain Value | Amplification Magnitude |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|          0 |                  1.0000 |         16 |                  2.0000 |         32 |                  4.0000 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|          1 |                  1.0625 |         17 |                  2.1250 |         33 |                  4.2500 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|          2 |                  1.1250 |         18 |                  2.2500 |         34 |                  4.5000 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|          3 |                  1.1875 |         19 |                  2.3750 |         35 |                  4.7500 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|          4 |                  1.2500 |         20 |                  2.5000 |         36 |                  5.0000 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|          5 |                  1.3125 |         21 |                  2.6250 |         37 |                  5.2500 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|          6 |                  1.3750 |         22 |                  2.7500 |         38 |                  5.5000 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|          7 |                  1.4375 |         23 |                  2.8750 |         39 |                  5.7500 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|          8 |                  1.5000 |         24 |                  3.0000 |         40 |                  6.0000 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|          9 |                  1.5625 |         25 |                  3.1250 |         41 |                  6.2500 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|         10 |                  1.6250 |         26 |                  3.2500 |         42 |                  6.5000 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|         11 |                  1.6875 |         27 |                  3.3750 |         43 |                  6.7500 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|         12 |                  1.7500 |         28 |                  3.5000 |         44 |                  7.0000 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|         13 |                  1.8125 |         29 |                  3.6250 |         45 |                  7.2500 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|         14 |                  1.8750 |         30 |                  3.7500 |         46 |                  7.5000 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			|         15 |                  1.9375 |         31 |                  3.8750 |         47 |                  7.7500 |
			|------------|-------------------------|------------|-------------------------|------------|-------------------------|
			Gain Value = 48 --> Amplification Magnitude = 8.0000
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_SetCmosParams(const char *sensId, int gain, int threshold, int min_spot_size, int max_spot_size);
		
		/*
			Sens_GetCmosParams: get sensor's parameters
			in:
			sensId - ID of sensor
			out:
			see input arguments of function Sens_SetCmosParams
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_GetCmosParams(const char *sensId, int& gain, int& threshold, int& min_spot_size, int& max_spot_size);
		
		/*
			Sens_LoadCmosParams: load sensor's parameters from 
			non-volatilve mem.
			in:
			sensId - ID of sensor
			+ see input arguments of function Sens_SetCmosParams
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_LoadCmosParams(const char *sensId, int &gain, int &threshold, int &min_spot_size, int &max_spot_size);

		/*
			Sens_SaveCmosParams: save sensor's parameters to 
			non-volatilve mem.
			in:
			sensId - ID of sensor
			+ see input arguments of function Sens_SetCmosParams
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_SaveCmosParams(const char *sensId, int gain, int threshold, int min_spot_size, int max_spot_size);

		/*
			Sens_InitPktStat: initialize packet statistics counters
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_InitPktStat(const char *sensId);
		
		/*
			Sens_GetLightSrcsInfo: get light sources packet info
			in:
			sensId - ID of sensor
			lightSrcInfoCB - a callback that is called when a light sources packet arrives
			out:
			pLightSrcInfo - struct ptr to be filled synch'ly, if not null
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_GetLightSrcsInfo(const char *sensId, stLightSrcInfo *pLightSrcInfo, LightSrcInfoCB lightSrcInfoCB);
		
		/*
			Sens_RegisterLightSrcsInfoExCB: register a callback that is called when an extended light sources packet arrives
			in:
			sensId - ID of sensor
			lightSrcInfoExCB - a callback that is called when an extended light sources packet arrives
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_RegisterLightSrcsInfoExCB(const char *sensId, LightSrcInfoExCB lightSrcInfoExCB);
		
		/*
			Sens_SetLightMode: set sensor's light mode
			in:
			sensId - ID of sensor
			lightMode - light mode
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_SetLightMode(const char *sensId, stLightMode lightMode);
		
		/*
			Sens_ToggleLaserGrid: toggle sensor's laser grid
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_ToggleLaserGrid(const char *sensId);
		
		/*
			Sens_OptCalibrationInit: initialize sensor optical calibration
			in:
			sensId - ID of sensor
			fToggleFunc - User provided callback function:
			              Invoked with SENSOR_CALIBRATION_TOGGLE_ON immediately before the process is enabled
			              Invoked with SENSOR_CALIBRATION_TOGGLE_OFF immediately after the process is disabled
			fStatusFunc - User provided callback function:
			              Invoked with SENSOR_CALIBRATION_STATUS_PHASE_1_STARTED immediately after the process phase #1 starts
			              Invoked with SENSOR_CALIBRATION_STATUS_PHASE_2_STARTED immediately after the process phase #2 starts
			              Invoked with SENSOR_CALIBRATION_STATUS_COMPLETE_SUCCESS immediately after the process completes successfully
			              Invoked with SENSOR_CALIBRATION_STATUS_COMPLETE_FAILURE immediately after the process completes with a failure
			fReportFunc - User provided callback function:
			              Invoked with SENSOR_CALIBRATION_REPORT_COMPLETE_SUCCESS after the process completes successfully
			              Invoked with SENSOR_CALIBRATION_REPORT_COMPLETE_FAILURE after the process completes with a failure
			              Invoked with SENSOR_CALIBRATION_REPORT_ABORT_ON_TIMEOUT after the process is aborted due to timeout
			              Invoked with SENSOR_CALIBRATION_REPORT_ABORT_ON_REQUEST after the process is aborted due to user-request
			fRedrawFunc - User provided callback function:
			              Invoked with a stLightSrcInfoEx structure containing sensor information every time a new sample is received
			fActionFunc - User provided callback function:
			              Invoked with SENSOR_CALIBRATION_ACTION_POINT_TO_SCREEN and the number of milliseconds left for the user to start pointing to the screen
			              Invoked with SENSOR_CALIBRATION_ACTION_SCAN_THE_SCREEN and the number of milliseconds left for the user to start scanning the entire screen
			fRecordFunc - User provided callback function:
			              Invoked with the following values every time a new sample is received, and once again after all samples for a given gain/threshold combination have been reecived:
			              - gain           - current gain
			              - threshold      - current threshold
			              - falsePositives - number of redundant light sources counted
			              - falseNegatives - number of missing light sources counted
			              - rectangleGrade - assessment of the light-source bounding rectangles
			              - grade          - overall grade as a function of falsePositives, falseNegatives and rectangleGrade
			              - done           - indicates whether or not all samples for the current gain/threshold combination have been reecived
			              - completed      - process completion percentage
			remarks:
			When the process completes successfully or with a failure, the difference between fStatusFunc and fReportFunc is minor:
			- fStatusFunc is invoked before the process is disabled (and before fToggleFunc invoked)
			- fReportFunc is invoked after the process is disabled (and after fToggleFunc invoked)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_OptCalibrationInit(const char *sensId,
		                                  SensorCalibrationToggleFunc fToggleFunc,
		                                  SensorCalibrationStatusFunc fStatusFunc,
		                                  SensorCalibrationReportFunc fReportFunc,
		                                  SensorCalibrationRedrawFunc fRedrawFunc,
		                                  SensorCalibrationActionFunc fActionFunc,
		                                  SensorCalibrationRecordFunc fRecordFunc);
		
		/*
			Sens_OptCalibrationStart: start sensor optical calibration
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_OptCalibrationStart(const char *sensId);
		
		/*
			Sens_AbortCalibration: abort sensor calibration
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_AbortCalibration(const char *sensId);
		
		/*
			Sens_AbortCalibrationCurrentPhase: abort sensor calibration's current phase
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_AbortCalibrationCurrentPhase(const char *sensId);

		/*
			Sens_CalibTouchPadsAllThi: calibrate touchpads on all connected Thimbles
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_CalibTouchPadsAllThi(const char *sensId);

		/*
			Sens_PortNameToSensId: return a sensor name that matches 
			the given serial id
			in:
			serialId - ID of serial port
			out:
			sensId - buffer for ID of serial port
			buff_size - size of buffer
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_PortNameToSensId(const char *serialId, char *sensId, unsigned int buff_size);

		/*
			Sens_SetGestureSensitivityAllThi: Set gesture sensotivity to all thimbles
			in:
			sensId - ID of sensor
			gestId - id of gesture (#follow GestureId)
			sens - sensitivity value (0 - 99)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_SetGestureSensitivityAllThi(const char *sensId, const GestureId gestId, const int sens);

		/*
			Sens_SetOFNOptionsAllThi: Set OFN Options to all thimbles
			in:
			sensId - ID of sensor
			bEnableOFNDblClick - Enable OFN Dbl Clicks
			bEnableOFNTapAndHold - Enable OFN TapAndHold
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_SetOFNOptionsAllThi(const char *sensId, bool bEnableOFNDblClick, bool bEnableOFNTapAndHold);

		/*
			Sens_SetGestureSensitivityAllThi: Set gesture availability to all thimbles
			in:
			sensId - ID of sensor
			gestId - id of gesture (#follow GestureId)
			bChecked - enabled/disabled
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_SetGestureEnabledAllThi(const char *sensId, const GestureId gestId, bool bChecked);

		/*
			Sens_SetMousePadSensitivityAllThi: Set mousepad sensitivity to all thimbles
			in:
			sensId - ID of sensor
			sens - sensitivity value (0 - 99)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_SetMousePadSensitivityAllThi(const char *sensId, const int sens);

		/*
			Sens_SetScrollSensitivityAllThi: Set scroll sensitivity to all thimbles
			in:
			sensId - ID of sensor
			sens - sensitivity value (0 - 99)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_SetScrollSensitivityAllThi(const char *sensId, const int sens);

		/*
			Sens_SetChannel: Set the channel of the sensor and all thimbles paired to it.
			in:
			sensId - ID of sensor
			channel - channel number (11 - 26)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_SetChannel(const char *sensId, int channel);

		/*
			Sens_TestAndSetChannel: strat channels scan test and switch channel at the end
			in:
			sensId	- ID of the sensor
			thiId	- ID of the thimble
			channelStartId - start channel number (11 - 26)
			out:
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_TestAndSetChannel(const char *sensId, const char *thiId, int channelStartId, int channelEndId, tSetChannelCB cb);
		int	API_DECL Sens_TestAndSetChannelAbort(const char *sensId, const char *thiId);
		/*
		Thi_Remove: remove the specified thimble
		in:
		thiId - ID of thimble
		out:
		returns:
		0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_Remove(const char *thiId);

		/*
			Thi_GetChannel: Get the channel of the thible.
			in:
			thiId - ID of thimble
			out:
			channel - channel number (11 - 26)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetChannel(const char *thiId, int* channel);

		/*
			Sens_GetChannel: Get the channel of the thible.
			in:
			sensId - ID of sensor
			out:
			channel - channel number (11 - 26)
			returns:
			0 - OK; -1 - an error has occured
		*/

		int	API_DECL Sens_GetChannel(const char *sensId, int* channel);

		/*
			Thi_SetThimbleLowPower: Set thimble to "sleepable" or not "sleepable" state.
			in:
			thiId - ID of thimble
			enabled - enables/disable sleepable mode
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetThimbleLowPower(const char *thiId, bool enabled);

		/*
			Sens_RegisterOnThiAddedCB: register a callback to recieve 'new thimble
			added' events.
			in:
			sensId - ID of sensor
			CBFunc - callback to receive events
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_RegisterOnThiAddedCB(const char *sensId, tOnThiAddedCB CBFunc);

		/*
			Sens_UnpairThimble: unpair a thimble
			in:
			sensId - ID of sensor
			thiId - ID of Thimble (hex format)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Sens_UnpairThimble(const char *sensId, const char *thiId);

		/*
			Sens_BlindSpotsAcquire: Start/stop Acquiring Blind Spots
			in:
			sensId - ID of sensor
			bOn	- Start/stop Acquiring
			returns:
			0 - OK; -1 - an error has occurred
		*/
		int	API_DECL Sens_BlindSpotsAcquire(const char *sensId, bool bOn);

		/*
			Sens_BlindSpotsClear: clear Acquired Blind Spots
			in:
			sensId - ID of sensor
			returns:
			0 - OK; -1 - an error has occurred
		*/
		int	API_DECL Sens_BlindSpotsClear(const char *sensId);

		/*
			Sens_GetCalibrationViewport: get the calibration viewport - the original points which 
			used for the calibration process, in sensor coords.
			in:
			sensId - ID of sensor
			out:
			X0, Y0, X1, Y1, X2, Y2, X3, Y3: original calibration points, from top-left, clockwise
			returns:
			0 - OK; -1 - an error has occurred
		*/
		int	API_DECL Sens_GetCalibrationViewport(const char *sensId, int &X0, int &Y0, int &X1, int &Y1, int &X2, int &Y2, int &X3, int &Y3);

		/*
			Sens_BlockThimbleMessages: block all thimble messages
			in:
			bOn - block all thimble messages
			returns:
			0 - OK; -1 - an error has occurred
		*/
		int	API_DECL Sens_BlockThimbleMessages(const char *sensId, bool bOn);

		/******************************************************************************
			API Module:	Thi_*
			Desc:		Thimble related actions and data
		*******************************************************************************/

		/*
			Thi_Get: get Thimble's configuration
			in:
			thiId - ID of Thimble (hex format)
			out:
			thimbleConfig - Thimble configuration struct to be filled
			returns:
			0 - OK; -1 - an error has occured
		*/
///		int	API_DECL Thi_Get(const char *thiId, stThimbleConfig &thimbleConfig);
		
		/*
			Thi_GetPktStat: get Thimble's packet statistics
			in:
			thiId - ID of Thimble (hex format)
			pktStatCB - a callback that is called asynch'ly when a new Thimble packet arrives
			out:
			pPktStat - packet statistics struct ptr to be filled synch'ly
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetPktStat(const char *thiId, stPktStat *pPktStat, pktStatCB pktStatCB = nullptr);
		
		/*
			Thi_SetAlias: set Thimble's alias
			in:
			thiId - ID of Thimble (hex format)
			alias - Thimble's alias
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetAlias(const char *thiId, const char *alias);
		
		/*
			Thi_GetAlias: get Thimble's alias
			in:
			thiId - ID of Thimble (hex format)
			out:
			alias - Thimble's alias buffer
			alias_buff_size - Thimble's alias buffer size
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetAlias(const char *thiId, char *alias, int alias_buff_size);
		
		/*
			Thi_GetActive: get Thimble's activation status
			in:
			thiId - ID of Thimble (hex format)
			out:
			bActive - Thimble's activation status
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetActive(const char *thiId, bool &bActive);
		
		/*
			Thi_SetActive: set Thimble's activation status
			in:
			thiId - ID of Thimble (hex format)
			bActive - Thimble's activation status
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetActive(const char *thiId, const bool bActive);
		
		/*
		Thi_SetExclusiveMode: set Thimble's activation status
		in:
		thiId - ID of exlusive Thimble (hex format) If empty string is passed exlusive mode is canceled
		returns:
		true - sacceed; false - action is failed
		*/
		bool	API_DECL Thi_SetExclusiveMode(const char *thiId);

		/*
		Thi_GetExclusive: Returns excusive thimble ID 
		in:
		thiId - ID of exclusive Thimble  or empty string
		returns:
		0 - OK; -1 - an error has occured
		*/
		int API_DECL Thi_GetExclusive(string& thiId);

		/*
			Thi_Start: start/stop the Thimble object message processing etc
			in:
			thiId - ID of Thimble (hex format)
			bOn - start/stop the Thimble object 
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_Start(const char *thiId, const bool bOn);

		/*
			Thi_GetConnStatus: get Thimble's connection status
			in:
			thiId - ID of Thimble (hex format)
			out:
			bConnected - Thimble's connection status
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetConnStatus(const char *thiId, BirdConnectionState &bConnectionState, bool& wasStarted);
		
	
		/*
			Thi_GetOperationMode: get Thimble's operation mode
			in:
			thiId - ID of Thimble (hex format)
			out:
			mode - Thimble's operation mode
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetOperationMode(const char *thiId, UIMode &mode);
		
		/*
			Thi_SetOperationMode: set Thimble's operation mode
			in:
			thiId - ID of Thimble (hex format)
			mode - Thimble's operation mode
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetOperationMode(const char *thiId, const UIMode mode);
		
		/*
		Thi_SetMultitouchMode: switches on and off multitouch mode in all thimbles
		in:
		enamble - enable/disable the mode
		returns:
		0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetMultitouchMode(bool enable);
		/*
			Thi_GetBatteryLevel: get Thimble's battery Level
			in:
			thiId - ID of Thimble (hex format)
			battLvl - Thimble's battery level
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetBatteryLevel(const char *thiId, int &battLvl);

		/*
		Thi_GetBatteryLevel: get Thimble's real status
		in:
		thiId - ID of Thimble (hex format)
		battLvl - Thimble's battery level
		mode    - Thimble's operational mode
		gesturesOn - Is teamble in gestures mode
		returns:
		0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetThimbleInfo(const char *thiId, stThimbleInfo& info);
		/*
			Thi_SetOperationModeAllThi: set operation mode for all paired thimbles
			in:
			mode - mode of operation
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetOperationModeAllThi(UIMode mode);

		/*
			Thi_RetrieveCompassAdjustmentValues: retrieve the compass adjustment values of the given thimble
			in:
			thiId - ID of Thimble (hex format)
			bOn - IMU data start/stop
			remarks:
			This function typically needs to be invoked only once
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RetrieveCompassAdjustmentValues(const char *thiId);

		/*
			Thi_SetIMUData: ask Thimble to start/stop sending IMU data
			in:
			thiId - ID of Thimble (hex format)
			bOn - IMU data start/stop
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetIMUData(const char *thiId, bool bOn);

		/*
			Thi_SetGestureRecog: set Thimble's gesture status
			in:
			thiId - ID of Thimble (hex format)
			bGestures - Thimble's gesture status (enable/disable)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetGestureRecog(const char *thiId, const bool bGestures);
		
		/*
			Thi_SetIMUDataAndCompassSrd: set Thimble's IMU data status and compass sample rate divider
			in:
			thiId - ID of Thimble (hex format)
			bOn - Thimble's IMU status (enable/disable)
			compassSrd - Thimble's compass sample rate divider
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetIMUDataAndCompassSrd(const char *thiId, const bool bOn, int compassSrd);
		
		/*
			Thi_Reset: reset Thimble
			in:
			thiId - ID of Thimble (hex format)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_Reset(const char *thiId);

		/*
			Thi_GetMousePadSensitivity: get Thimble's MousePad sensitivity
			in:
			thiId - ID of Thimble (hex format)
			out:
			sens - MousePad sensitivity (0 - 99)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetMousePadSensitivity(const char *thiId, int	&sens);
		
		/*
			Thi_SetMousePadSensitivity: set Thimble's MousePad sensitivity
			in:
			thiId - ID of Thimble (hex format)
			sens - MousePad sensitivity (0 - 99)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetMousePadSensitivity(const char *thiId, const int sens);
		
		/*
			Thi_GetScrollSensitivity: get Thimble's scroll sensitivity
			in:
			thiId - ID of Thimble (hex format)
			out:
			sens - Scroll sensitivity (0 - 99)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetScrollSensitivity(const char *thiId, int	&sens);

		/*
			Thi_SetScrollSensitivity: set Thimble's scroll sensitivity
			in:
			thiId - ID of Thimble (hex format)
			sens - Scroll sensitivity (0 - 99)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetScrollSensitivity(const char *thiId, const int sens);

		/*
			Thi_GetFlySensitivity: get Thimble's Fly sensitivity
			in:
			thiId - ID of Thimble (hex format)
			out:
			sens - sensitivity value (0 - 99)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetFlySensitivity(const char *thiId, int	&sens);
		
		/*
			Thi_SetFlySensitivity: set Thimble's Fly sensitivity
			in:
			thiId - ID of Thimble (hex format)
			sens - sensitivity value (0 - 99)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetFlySensitivity(const char *thiId, const int sens);
		
		/*
			Thi_GetGestureSensitivity: get Thimble's gesture sensitivity
			in:
			thiId - ID of Thimble (hex format)
			gestId - id of gesture (#follow GestureId)
			out:
			sens - sensitivity value (0 - 99)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetGestureSensitivity(const char *thiId, const GestureId gestId, int	&sens);
		
		/*
			Thi_SetGestureSensitivity: set Thimble's gesture sensitivity
			in:
			thiId - ID of Thimble (hex format)
			gestId - id of gesture (#follow GestureId)
			sens - sensitivity value (0 - 99)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetGestureSensitivity(const char *thiId, const GestureId gestId, const int sens);
		
		/*
			Thi_SetGestureSensitivityAll: set Thimble's gesture sensitivity for all gestures
			in:
			thiId - ID of Thimble (hex format)
			sens - sensitivity value (0 - 99)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetGestureSensitivityAll(const char *thiId, const int sens);
		
		/*
			Thi_GetGestureEnabled: get Thimble's gesture state
			in:
			thiId - ID of Thimble (hex format)
			gestId - Thimble's gesture id
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetGestureEnabled(const char *thiId, const GestureId gestId, bool	&bEnabled);

		/*
			Thi_SetGestureEnabled: enable/disable a Thimble's gesture 
			in:
			thiId - ID of Thimble (hex format)
			gestId - a Thimble's gesture to be set
			bEnabled - enable/disable a gesture 
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetGestureEnabled(const char *thiId, const GestureId gestId, const bool bEnabled);

		/*
			Thi_UpdateFirmware: update Thimble's firmware
			in:
			thiId - ID of Thimble (hex format)
			fileName - file to update
			FwUpgradeCallbacks - callbacks
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_UpdateFirmware(const char *thiId, const char *fileName, API::tFwUpgradeCallback callback);

		/*
			Thi_WakeUp: wake thimble up
			in:
			thiId - ID of Thimble (hex format)
			duration - Number of seconds (between 0 and 3600) before going back to low power 
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_WakeUp(const char *thiId, int duration);
		
		/*
			Thi_RequestVersion: Request thimble's SW/LEDs/HW version
			in:
			thiId - Thimble ID
			sw - Send SW/LEDs-Version request
			hw - Send HW-Version request
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RequestVersion(const char *thiId, bool sw, bool hw);
		
		/*
			Thi_RefreshVersion: Refresh thimble's SW/LEDs/HW version
			in:
			thiId - Thimble ID
			sw - Send SW/LEDs-Version request and wait for response
			hw - Send HW-Version request and wait for response
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RefreshVersion(const char *thiId, bool sw, bool hw);
		
		/*
			Thi_RetrieveVersion: Retrieve thimble's SW/LEDs/HW version
			in:
			thiId - Thimble ID
			out:
			version - Thimble's SW/LEDs/HW version
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RetrieveVersion(const char *thiId, stThiVersion &version);

		/*
			Thi_RefreshGyroOffset: Refresh thimble's Gyro Offset
			in:
			thiId - Thimble ID
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RefreshGyroOffset(const char *thiId);

		/*
			Thi_SetHwVersion: Set Thimble's HW version
			in:
			thiId - Thimble ID
			version - Thimble's HW version
			returns:
			0 - OK; -1 - an error has occured

		*/
		int	API_DECL Thi_SetHwVersion(const char *thiId, const stCoreVersion &version);

		/*
			RegisterOnStatusCB: register a callback for status events (called
			every time a new status packet arrives)
			in:
			thiId - ID of Thimble (hex format)
			CBFunc - callback function address
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RegisterOnStatusCB(const char *thiId, tOnThiPktCB CBFunc);

		/*
			Thi_RegisterOnGestureCB: register a callback for gesture events (called
			every time a new gesture is detected or ended)
			in:
			thiId - ID of Thimble (hex format)
			CBFunc - callback function address
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RegisterOnGestureCB(const char *thiId, tOnGestureCB CBFunc);
		
		/*
			Thi_RegisterOnOrientPRYCB: register a callback for orientation values events (called
			every time a new orientation value is calc'd, typically when IMU data arrives)
			Values are in PRY (Pitch/Roll/Yaw) format.
			in:
			thiId - ID of Thimble (hex format)
			CBFunc - callback function address
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RegisterOnOrientPRYCB(const char *thiId, tOnOrientPRYCB CBFunc);
		
		/*
			Thi_GetOrientPRY: get orientation values 
			in:
			thiId - ID of Thimble (hex format)
			orient - orientation values 
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetOrientPRY(const char *thiId, API::tOrientPRY& orient);

		/*
			Thi_RegisterOnOrientQCB: register a callback for orientation values events (called
			every time a new orientation value is calc'd, typically when IMU data arrives)
			Values are in quaternion format.
			in:
			thiId - ID of Thimble (hex format)
			CBFunc - callback function address
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RegisterOnOrientQCB(const char *thiId, tOnOrientQCB CBFunc);

		/*
			Thi_GetOrientQ: get orientation values. Values are in quaternion format.
			in:
			thiId - ID of Thimble (hex format)
			orient - orientation values
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetOrientQ(const char *thiId, API::tOrientQ& orient);

		/*
			Thi_RegisterOnBtnsCB: register a callback for buttons values events (called
			every time a new buttons data arrives)
			in:
			thiId - ID of Thimble (hex format)
			CBFunc - callback function address
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RegisterOnBtnsCB(const char *thiId, tOnBtnsCB CBFunc);

		/*
			Thi_RegisterOnBtnsExtendedCB: register a callback for buttons values events (called
			every time a new buttons data arrives)
			in:
			thiId - ID of Thimble (hex format)
			CBFunc - callback function address
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RegisterOnBtnsExtendedCB(const char *thiId, tOnBtnsExtCB CBFunc);

		/*
		Thi_RegisterOnBtnsCB: register a callback for buttons values events (called
		every time a new buttons data arrives)
		in:
		thiId - ID of Thimble (hex format)
		CBFunc - callback function address. if CBFunc==nullptr it will be ignored
		returns:
		0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetBtnsExtendedMode(const char *thiId, bool bOn, tOnBtnsExtCB CBFunc);
		/*
			Thi_GetBtns: get current state of buttons 
			in:
			thiId - ID of Thimble (hex format)
			btns - current state of buttons 
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetBtns(const char *thiId, tBtnsStateFlags &btns);

		/*
			Thi_RegisterOnBtnsClkCB: register a callback for button 
			click events
			in:
			thiId - ID of Thimble (hex format)
			CBFunc - callback function address
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RegisterOnBtnsClkCB(const char *thiId, tOnBtnsCB CBFunc);
		
		/*
			Thi_RegisterOnBtnsDblClkCB: register a callback for button double
			click events
			in:
			thiId - ID of Thimble (hex format)
			CBFunc - callback function address
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RegisterOnBtnsDblClkCB(const char *thiId, tOnBtnsCB CBFunc);
		
		/*
			Thi_RegisterOnBtnsClkNotDblCB: register a callback for button click (but not double
			click) events
			in:
			thiId - ID of Thimble (hex format)
			CBFunc - callback function address
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RegisterOnBtnsClkNotDblCB(const char *thiId, tOnBtnsCB CBFunc);

		/*
			Thi_RegisterOnOFNCB: register a callback for OFN values events (called
			every time a new OFN data arrives)
			in:
			thiId - ID of Thimble (hex format)
			CBFunc - callback function address
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RegisterOnOFNCB(const char *thiId, tOnOFNCB CBFunc);
		
		/*
			Thi_RegisterOnOFNEventsCB: register a callback for OFN high-level events 
			in:
			thiId - ID of Thimble (hex format)
			SwipeCBFunc - Swipe events callback function address
			ClicksCBFunc - click events callback function address
			ScrollCBFunc - scroll events callback function address
			ScrollStartCBFunc - scroll start events callback function address
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RegisterOnOFNEventsCB(const char *thiId, tOnOFNSwipeCB SwipeCBFunc, tOnOFNClicksCB ClicksCBFunc, tOnOFNScrollCB ScrollCBFunc, tOnOFNScrollCB ScrollStartCBFunc);

		/*
		Thi_RegisterOnSwitchActiveThimbleCB: register a callback that is called in multithimble environment when active thible is changed
		in:
		thiId - ID of Thimble (hex format)
		cb - a callback that is called when active thible is changed
		returns:
		0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RegisterOnSwitchActiveThimbleCB(const char* thimbleId, tOnSwitchActiveThimbleCB cb);

		/*
		Thi_RegisterOnUIInfoCB: register a callback that is called when infrastructure level needs information from UI level
		in:
		thiId - ID of Thimble (hex format)
		cb - a callback that is called when active thible is changed
		returns:
		0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_RegisterOnUIInfoCB(const char* thimbleId, tOnUIInfoCB cb);

		/*
			Thi_GetOFN: get OFN values 
			in:
			thiId - ID of Thimble (hex format)
			x, y : movements coors
			flags : button flags
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_GetOFN(const char *thiId, int &x, int &y, tOFNFlags  &flags);

		/*
			Thi_SetOperational: switch the Thimble from testing to operational mode
			in:
			thiId - ID of Thimble (hex format)
			OperMode - operational/debug (1/0)
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetOperational(const char *thiId, const int OperMode);

		/*
			Thi_SetPostureHandshake: set the posture to handshake 
			- this causes all the orientation calculation to regard handshake 
			psoture at 0,0,0
			- calling this requires also IMU re-calibration
			in:
			thiId - ID of Thimble (hex format)
			bOn - set the posture 
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetPostureHandshake(const char *thiId, bool bOn);

		/*
			Thi_InitIMUCalib: calibrate IMU gyro
			- user should hold the thimble steady for 4 secs
			- we can dispense with this, if we have Magneto readins
			in:
			thiId - ID of Thimble (hex format)
			CBFunc	- called when calibration is done
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_InitIMUCalib(const char *thiId, tActivityCB CBFunc);

		/*
			Thi_IsIMUCalibValid: is thimble's IMU calibration valid?
			in:
			thiId - ID of Thimble (hex format)
			returns: true/false of calibration validity
		*/
		bool	API_DECL Thi_IsIMUCalibValid(const char *thiId);

		/*
			Thi_InitCompassCalib: calibrate compass
			- user should rotate thimble 365 degs
			in:
			thiId - ID of Thimble (hex format)
			CBFunc	- called when calibration is done
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_InitCompassCalib(const char *thiId, tActivityCB CBFunc);

		/*
			switch the Thimble's light src on/off
			Thi_SetRedLaser
			Thi_SetFrontLaser
			Thi_SetPorchIRLaser
			Thi_SetWideLED
			in:
			thiId - ID of Thimble (hex format)
			bOn - light src on/off
			returns:
			0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetRedLaser(const char *thiId, const bool bOn);
		int	API_DECL Thi_SetFrontLaser(const char *thiId, const bool bOn);
		int	API_DECL Thi_SetWideLED(const char *thiId, const bool bOn);
		int	API_DECL Thi_SetPorchIRLaser(const char *thiId, const bool bOn);

		/*
		Thi_SetPeriphRegisterValue - sets the value to the specified address of specified peripheral thimble chip
		in:
		thiId - ID of Thimble (hex format)
		perType - type of the chip
		registerAddr - address of the target register (has to be byte)
		val - target value (has to be byte)
		returns:
		0 - OK; -1 - an error has occured
		*/
		int API_DECL Thi_SetPeriphRegisterValue(const char *thiId, ThimbleChipType chipType, int registerAddr, int val);
		/*
		Thi_GetHardwareStatus - get statuses of thimble peripheral components
		in:
		thiId - ID of Thimble (hex format)
		out:
		hardwareStatus - bitmap containing the components statuses flags
		returns:
		0 - OK; -1 - an error has occured
		*/
		int API_DECL Thi_GetHardwareStatus(const char* thiId, int* hardwareStatus);
		/*
		Thi_SetCapacitiveConfig - sets chargeCurrent and chargeTime registers to the thimble (convenient envelope of Thi_SetPeriphRegisterValue)
		in:
		thiId - ID of Thimble (hex format)
		chargeCurrent - charge current value(has to be byte)
		chargeTime - charge time value (has to be byte)
		returns:
		0 - OK; -1 - an error has occured
		*/
		int API_DECL Thi_SetCapacitiveConfig(const char *thiId, int chargeCurrent, int chargeTime);

		/*
		Thi_GetCapacitanceValues - sets chargeCurrent and chargeTime registers to the thimble (convenient envelope of Thi_SetPeriphRegisterValue)
		in:
		thiId - ID of Thimble (hex format)
		out:
		chargeCurrent - charge current value(has to be byte)
		chargeTime - charge time value (has to be byte)
		returns:
		0 - OK; -1 - an error has occured
		*/
		int API_DECL Thi_GetCapacitiveConfig(const char *thiId, int& chargeCurrent, int& chargeTime);

		/*
		Thi_SetCapacitanceValues - sets chargeCurrent and chargeTime registers to the thimble (convenient envelope of Thi_SetPeriphRegisterValue)
		in:
		thiId - ID of Thimble (hex format)
		out:
		pinchVal - pinch capacitance value(has to be SHORT)
		thresholdVal - pinch capacitance threshold value (has to be SHORT)
		returns:
		0 - OK; -1 - an error has occured
		*/

		int API_DECL Thi_SetCapacitanceValues(const char *thiId, int pinchVal, int thresholdVal);

		/*
		Thi_SetCapacitiveValues - sets chargeCurrent and chargeTime registers to the thimble (convenient envelope of Thi_SetPeriphRegisterValue)
		in:
		thiId - ID of Thimble (hex format)
		out:
		pinchVal - pinch capacitance value saved in the thimble
		thresholdVal - pinch capacitance threshold value saved in the thimble
		airVal - air pinch value saved in the thimble
		returns:
		0 - OK; -1 - an error has occured
		*/
		int API_DECL Thi_GetCapacitanceValues(const char *thiId, int& pinchVal, int& thresholdVal, int& airVal);


		/*
		switch the Thimble's light src on/off
		in:
		thiId - ID of Thimble (hex format)
		frontIR, sideIr, IRLed, redLaser - flags for related IR lights. 0 - switch off, 1 - switch on, -1 - leave as is
		returns:
		0 - OK; -1 - an error has occured
		*/
		int API_DECL Thi_SetLights(const char *thiId, int frontIR, int redLaser, int IRLed, int sideIr);

		int	API_DECL Thi_GetInternalParams(const char *thiId, stThiInternParams &params);
		/*
		SetThimbleMessageTries: Sets number of tries when sending message from thimble to sensor
		in:
		thiId - ID of Thimble (hex format)
		msgType - number of tries on sending packets from thimble to sensor. 0 means default
		numOfTries	- new number of tries
		returns:
		0 - OK; -1 - an error has occured
		*/
		int	API_DECL Thi_SetThimbleMessageTries(const char *thiId, uint msgType, uint numOfTries);
		/*
		Cli_LOG: Prints out the message to standard client log
		in:
		msg - messaged for print out
		returns:
		0 - OK; -1 - an error has occured
		*/
		int	API_DECL Cli_LOG(const char*  msg);

		//The following methods are for testing tool usage only. 
		//SetTestingToolMode - Allows to change client behavior in testing mode
		int	 API_DECL SetTestingToolMode(bool bOn);
		//IsTestingToolMode - returns boolean flag whether we in the testing mode or not

		bool API_DECL IsTestingToolMode();
		/*TestSensorToThimbleConnection sends spoecified number of packets to thimble and returns number of retrieved packets
		in:
		thiId - ID of Thimble (hex format)
		numOfTries - number of tries on sending packets from sensor to thimble
		numPktsToSend - number of packets for connectivity testing
		out:
		recievedPktsNum - number of packets that were recieved by thimble
		returns:
		0 - OK; -1 - an error has occured
		*/
		int API_DECL TestSensorToThimbleConnection(const char *thiId, int numOfTries, int numPktsToSend, int& recievedPktsNum, int sleepDurationBetweenMessagesMiliSec);

		/*TestThimbleToSensorConnection sends spoecified number of packets to thimble and returns number of retrieved packets
		in:
		thiId - ID of Thimble (hex format)
		sizeOfPacket - the size of test packet
		numOfPackets - number of packets for connectivity testing
		numOfRetries - number of tries on sending packets from thimble to sensor
		out:
		none.
		returns:
		0 - OK; -1 - an error has occured
		*/
		int API_DECL TestThimbleToSensorConnection(const char *thiId, int sizeOfPacket, int numOfPackets, int numOfRetries);

	//	int API_DECL isThimbleToSensorTestRunning(string& out_thiId);

		int API_DECL isThimbleToSensorTestRunning();

		int API_DECL GetThimbleToSensorTestReults(const char* thiId, int& out_result);

		int	UnitTest(const char * comPortName);
#ifdef API_EXTERN_C
	}
#endif
}

#endif	//	__BIRDAPI_H__


