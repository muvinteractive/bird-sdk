/******************************************************************************
	Module:	MUV CLient
	File:	STThimbleConfig.h
	Desc:	Thimble configuration data struct
	Author:	#gr
	Creatd:	9-2014
	Remarks:

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

#ifndef __STTHIMBLECONFIG_H__
#define __STTHIMBLECONFIG_H__
#include <string>
#include "VersionInfo.h"

/******************************************************************************
	DEFINES
*******************************************************************************/

#define	STTHIMBLE_MAX_STR_LEN	(32)

#define	THICONFIG_MODE_FLY		1
#define	THICONFIG_MODE_MOUSEPAD 2

#define	THICONFIG_SENS_MIN		0
#define	THICONFIG_SENS_MAX		99

/******************************************************************************
	CLASS DECL
*******************************************************************************/

// modes of the UI
enum class UIMode {
	invalid = 0,
	multicursor,			// act as touch device 
	standard,		// act as fly
	airmouse,		// act as a mousepad + touch
	__num
};

// id's of gestures
enum GestureId {
	gestId_invalid = 0,
	gestId_left,
	gestId_right,
	gestId_up,
	gestId_down,
	gestId_come,
	gestId_go,
	gestId_prayAndPush,
	gestId_pullAnArrow,
	gestId_pullBowBack,
	gestId_reloadAGun,
	gestId___num
};

enum class BirdConnectionState{
    NOT_CONNECTED,
	DISCONNECTED,
    CONNECTED,
    FINGER_PRESENT,
    IN_THE_MOVEMENT
};

inline std::string GestureIdToString(GestureId gId)
{
	switch (gId)
	{
	case gestId_left:  return "left";
	case gestId_right: return "right";
	case gestId_up:	   return "up";
	case gestId_down:  return "down";
	}
	return "undefined";
}

#define	THICONFIG_FIRST_GEST	(gestId_invalid + 1)
#define	THICONFIG_LAST_GEST		(gestId_come - 1)
#define	THICONFIG_GEST_NUM		(gestId_come - 1)

struct stThimbleConfig {
	char m_Id[STTHIMBLE_MAX_STR_LEN];		// Unique identifier of this thimble (per sensor), no other sensor should have this identifier
	char m_alias[STTHIMBLE_MAX_STR_LEN];	// Friendly name of this thimble
	bool m_bActive;				// Indicates if this thimble is active or not
	UIMode	 _place_holder; // mode is set seperately /// UIMode	 m_mode;			// The operation mode for this thimble.
	bool m_bGestures;			// Indicates if this thimble supports gestures 
	int	 m_mousepadSens;		// The sensitivity for the Mouse Pad mode for this thimble. val: 0-99
	int	 m_scrollSens;			// The sensitivity for the scroll for this thimble. val: 0-99
	int	 __unused2; // don't remove - for binary compat
	int	 m_flySens;				// The sensitivity of the Fly mode for this thimble. val: 0 - 99
	int	 m_gestureSens[THICONFIG_GEST_NUM];		// The sensitivity of the gesture mode for this thimble. val: 0 - 99
	bool m_gestureEnabled[THICONFIG_GEST_NUM];		// The enable state of the gesture mode for this thimble. val: true/false
	char m_firmware[STTHIMBLE_MAX_STR_LEN];			// The firmware version of this sensor
};

struct stThimbleInfo{
	bool			m_bFingerPresent{}; //finger is inside thible
	stThiVersion	m_tver{};
	bool			m_bActive{};				// Indicates if this thimble is active or not
	BirdConnectionState	m_connectionState;			// Indicates if this thimble got at least one thimble originated packet during last 30 sec
	int				m_battLvl{};				// The battery level of this thimble
	UIMode			m_mode{};			// The operation mode for this thimble.
	bool			m_bGestures{};			// Indicates if this thimble supports gestures 
	unsigned int	m_elapsedSinceLastPkt{}; //Time in miliseconds elapsed since the last non-void packet from thimble
	int				m_index{-1};			// index of this thimble; >= 0 if valid
	bool			m_bMultiactive;			//Active in multithimble meaninig
	int				m_rfLevel;				//Average RF level (0-6) for some period of time (based on lost packets count)
	int				m_rfPerc;				//The same as m_rfLevel but in % measure
};


#endif // __STTHIMBLECONFIG_H__

