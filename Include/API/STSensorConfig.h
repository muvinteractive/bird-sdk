/******************************************************************************
	Module:	MUV CLient
	File:	STSensorConfig.h
	Desc:	Sensor configuration data
	Author:	#gr
	Creatd:	9-2014
	Remarks:

	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.

*******************************************************************************/

#ifndef __STSENSORCONFIG_H__
#define __STSENSORCONFIG_H__

#pragma warning(disable :4351)

/******************************************************************************
	DEFINES
*******************************************************************************/

#define	STSENSOR_MAX_STR_LEN		(32)
#define	STSENSOR_MAX_CALIB_PROFS	(5)

/******************************************************************************
	CLASS DECL
*******************************************************************************/
struct stSensorConfig {
	stSensorConfig() : m_Id{}, m_alias{}, m_comPort{}, m_firmware{}, m_firmwareDateTime{} {}
	char m_Id[STSENSOR_MAX_STR_LEN];	// Unique identifier of this sensor, no other sensor should have this identifier
	char m_alias[STSENSOR_MAX_STR_LEN];	// Friendly name of this sensor
	bool m_bActive{};					// Indicates if this sensor is active or not
	bool m_bMaster{};					// Indicates if this sensor is the master one.A master sensor means it�s the only one that needs to compute the Radio Messages
	int	 m_displayExtension{};			// The display extension this sensor is looking at
	char m_comPort[STSENSOR_MAX_STR_LEN];		// The COM port this sensor is connected
	/// from warper --- float m_warpMatrix[4][2];			 //The warp matrix for this sensor and the designated display extension
	float m_dummy_warpMatrix[STSENSOR_MAX_CALIB_PROFS][16];		 //The warp matrix for this sensor and the designated display extension
	int	m_curCalibProfInx{};			// what profile of calib are we now on; 0 to STSENSOR_MAX_CALIB_PROFS-1
	int	m_calibrationState{ -1 };		// The current state for the calibration procedure of this sensor: -1 - No Calibration; 0 - Step 1; 1 - Step 2; 2 - Step 3; 3 - Step 4; 	4 - Step 5; 5 - Done	
	char m_firmware[STSENSOR_MAX_STR_LEN];			// The firmware version of this sensor
	char m_firmwareDateTime[STSENSOR_MAX_STR_LEN];	// The date and time of the last firmware version update for this sensor
	int	m_dummy_cmos_gain{};			// CMOS gain
	int	m_dummy_cmos_threshold{};		// CMOS threshold
	int	m_dummy_cmos_min_spot_size{};	// CMOS minimum spot size
	int	m_dummy_cmos_max_spot_size{};	// CMOS maximum spot size 
	int	m_dummy_calibRefPoints[STSENSOR_MAX_CALIB_PROFS][8];		 // original calib ref points
};


#endif // __STSENSORCONFIG_H__

