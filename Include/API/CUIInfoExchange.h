/******************************************************************************
Module:	MUV CLient API
File:	stPktStat.h
Desc:	Classes that provide data containment for passing from UI level to infrastructure level
Author:	#hk
Creatd:	08-2016
Remarks: 
-----------------------------
Copyright (C) MUV Interactive
All rights reserved.

*******************************************************************************/

#ifndef __CUIINFOEXCHANGE_H__
#define __CUIINFOEXCHANGE_H__

#include "STThimbleConfig.h"
/******************************************************************************
DEFINES
*******************************************************************************/
#define UIINFO_PTR(ref, class_name) dynamic_cast<class_name*>(&ref)

/******************************************************************************
STRUCT DECL
*******************************************************************************/

namespace API
{

	class CUIInfoExchange {
	public:
		virtual ~CUIInfoExchange() {}
	};
	class CUIInfoSystemConfigOngoing : public CUIInfoExchange
	{
	public:
		void SetOngoingFlag(bool isOngoing) { m_isOngoing = isOngoing; }
		bool IsSystemConfigOngoing() const { return m_isOngoing; }

	protected:
		bool m_isOngoing{ false };
	};

	class CUIInfoSwitchMultiactivity : public CUIInfoSystemConfigOngoing
	{
	public:
		void SetUiParams(UIMode mode, bool gestures) { m_uiMode = mode; m_gestures = gestures; }
		UIMode GetUIMode() { return m_uiMode; }
		bool   IsGestureOn() { return m_gestures; }

	protected:
		UIMode	m_uiMode{ UIMode ::airmouse};
		bool	m_gestures{};
	};
}

#endif // __STPKTSTAT_H__

