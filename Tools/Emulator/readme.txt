﻿//////////////////////////////////////////////////////////////////////////////

	Bird SDK 1.2 - Tools/Emulator/readme.txt
	May-2016
	-----------------------------
	Copyright (C) MUV Interactive
	All rights reserved.
	

//////////////////////////////////////////////////////////////////////////////

1. Setting up the Bird Emulator:
- Start the BirdEmulator.exe program
- make sure the status line shows "ready. port=2727"
- Start the Bird App and close it. In the running directory you should find a 
file named MUVClient.ini; open it for editing.
- Find and edit the following property __sensName=127.0.0.1:2727
- Save the INI file and start Bird App. It should now connect to the Bird 
Emulator; the status line should reflect that.
- When using the Bird DLL in any other application, the steps above are the 
same; the file MUVClient.ini has the same format.

2. Using the Bird Emulator:
- to start emulating any kind of input from the Bird, click on the appropriate 
checkbox: IMU data for inertial information (the angle of the Bird is controlled 
by the Pitch/Roll sliders); Home/Pinch/Touch buttons; On-Finger status.
